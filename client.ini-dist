#################################################
# Global configuration
# https://pastedeploy.readthedocs.io/en/latest/#global-configuration
#################################################
[DEFAULT]
obsportal.environment = client
obsportal.variant = local

obsportal.paths.data = /data/cache
obsportal.paths.logs = /logs
obsportal.paths.tmp = /tmp

obsportal.locks.synchronize = %(obsportal.paths.tmp)s/synchronize.lock

#################################################
# Application configuration
# https://docs.pylonsproject.org/projects/pyramid/en/latest/narr/environment.html
#################################################
[app:main]
use = egg:obsportal

#######################
## Database parameters
#######################

# Using PostgreSQL (pg_sphere required)
# See https://docs.sqlalchemy.org/en/latest/core/engines.html#postgresql
sqlalchemy.url = postgresql://user:password@host/dbname?client_encoding=utf8

# Connection Pool settings:
sqlalchemy.pool_size = 5
sqlalchemy.max_overflow = 5
# recycle opened connection after 1h idle:
sqlalchemy.pool_recycle = 3600

#########################
# Pyramid configuration
#########################
pyramid.reload_templates = false
pyramid.debug_authorization = false
pyramid.debug_notfound = false
pyramid.debug_routematch = false

pyramid.default_locale_name = en

# See https://docs.pylonsproject.org/projects/pyramid-retry/en/latest/
retry.attempts = 3

pyramid.includes =

########################
## Jinja2 custom filters
########################
jinja2.filters =
    datetime = obsportal.filters:filter_datetime


#################################################
## Alembic configuration
#################################################
[alembic]
# path to migration scripts
script_location = src/obsportal/alembic
file_template = %%(year)d%%(month).2d%%(day).2d_%%(rev)s
# file_template = %%(rev)s_%%(slug)s

#################################################
## Framework shell parameters
#################################################
[pshell]
setup = obsportal.pshell.setup

#################################################
## WSGI server configuration
#################################################
[server:main]
use = egg:waitress#main
listen =

#################################################
## Logging configuration
##https://docs.pylonsproject.org/projects/pyramid/en/latest/narr/logging.html
#################################################
[loggers]
keys = root, obsportal, obsportal_query_cache, waitress, sqlalchemy, sqlalchemy_pool, alembic, urllib

[handlers]
keys = console, file

[formatters]
keys = generic

[logger_root]
level = INFO
handlers = console, file

[logger_obsportal]
level = INFO
handlers =
qualname = obsportal

[logger_obsportal_query_cache]
level = INFO
handlers =
qualname = obsportal.model.utils

[logger_sqlalchemy]
level = WARN
handlers = 
qualname = sqlalchemy.engine
# "level = INFO" logs SQL queries.
# "level = DEBUG" logs SQL queries and results.
# "level = WARN" logs neither.  (Recommended for production systems.)

[logger_sqlalchemy_pool]
level = WARN
handlers = 
qualname = sqlalchemy.pool

[logger_alembic]
level = INFO
handlers =
qualname = alembic

[logger_waitress]
level = INFO
handlers = 
qualname = waitress

[logger_urllib]
level = INFO
handlers = 
qualname = urllib3

[handler_console]
class = StreamHandler
args = (sys.stdout,)
level = NOTSET
formatter = generic

[handler_file]
class = FileHandler
args=(__import__("datetime").datetime.now().strftime('%(obsportal.paths.logs)s/%(obsportal.environment)s_%%Y-%%m-%%d_%%H-%%M-%%S.log'), 'a')
level = NOTSET
formatter = generic

[formatter_generic]
format = %(asctime)s %(levelname)-5.5s [%(name)s:%(lineno)s][%(threadName)s] %(message)s

