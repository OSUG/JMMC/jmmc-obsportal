FROM python:3.11.11-slim

###################################
#
# Usual maintenance
#

# ALWAYS RUN apt-get update && apt-get install -y --no-install-recommends

# Install debian packages:
RUN set -eux ; \
	apt-get update && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends vim python3-pip python3-dev postgresql-common postgresql-client libpq-dev git && \
    apt-get clean && \
    true


# Pre-install important pip packages (for speedup during the application setup)
RUN set -eux ; \
    pip install --upgrade pip setuptools wheel && \
    pip install lxml cryptography click pyramid pyramid_jinja2 wtforms alembic waitress SQLAlchemy transaction psycopg2_binary numpy astropy astroquery pyvo && \
    true


# see .dockerignore (.git is skipped)
ADD . /app
WORKDIR /app


# Define the image tag before setup.py (version):
ARG IMAGE_TAG=NOT_SET
ENV IMAGE_TAG=$IMAGE_TAG

ARG APPCONFFILE=production.ini-dist
ADD $APPCONFFILE /app/docker.ini

RUN set -eux ; \
# Install application:
    echo "install Obsportal with dependencies ..." && \
    mkdir -p /data /logs && \
    pip install .

RUN set -eux ; \
# Install dev and testing for DEV versions:
    echo "$IMAGE_TAG" | grep -qi DEV  && \
    echo "install develop and testing material Obsportal with dependencies ..." && \
    pip install -e ".[develop]" && \
    pip install -e ".[testing]" || \
    echo "$IMAGE_TAG is not a development version (DEV must be present in IMAGE_TAG)"

ARG APPCONFFILE=production.ini-dist
ADD $APPCONFFILE /app/docker.ini

ARG CLICONFFILE=client.ini-dist
ADD $CLICONFFILE /app/client.ini


CMD ["sh", "run-obsportal.sh", "docker.ini" ]

EXPOSE 6543

