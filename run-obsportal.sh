#!/bin/bash


SETTINGSFILE=$1
test -z "$SETTINGSFILE"   && echo "Missing argument, please provide a settings file."            && exit 1
test ! -r "$SETTINGSFILE" && echo "Can't read '$SETTINGSFILE', please provide a settings file."  && exit 1

#echo "Run alembic upgrade head for $SETTINGSFILE"
#alembic -c $SETTINGSFILE upgrade head

processId="pserve"
logFile=/logs/production.log
vmFile=/logs/vm.log
fsTestFile=/logs/test-fs-production.log


# Check if service is running:
PID=`ps -ef | grep "${processId}" | grep -v "grep" | awk '{print $2}'`
#echo "PID: '${PID}'"

N_PID=`echo "${PID}" | wc -w`

if [ "${N_PID}" -ne "0" ]
then
    echo "Process '${processId}' (PID = ${PID}) is already running ..."
    exit 1
fi


# Move log before starting new process
mv ${logFile} ${logFile}.$(date +"%F_%T")
mv ${vmFile} ${vmFile}.$(date +"%F_%T")


# Test FS (write) before starting web server:
set -eux
echo "TEST production" > ${fsTestFile}
rm ${fsTestFile}


echo "Start vmstat in background..."
vmstat -t 60 2>&1 > ${vmFile} &

# Upgrade the database structure
CMD="obsportal-cli database upgrade --settings=$SETTINGSFILE"
echo "Running: $CMD"
$CMD


# Launch the server
echo "Run 'pserve' $SETTINGSFILE"
exec pserve $SETTINGSFILE

