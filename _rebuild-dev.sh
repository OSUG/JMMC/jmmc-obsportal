#!/bin/bash

obsportal-cli database run-sql-script db/_clear_db.sql --settings=dev-local.ini

obsportal-cli database create-structure --skip_exist --debug --settings=dev-local.ini
obsportal-cli database init --debug --settings=dev-local.ini

echo "That's all folks !!!"

