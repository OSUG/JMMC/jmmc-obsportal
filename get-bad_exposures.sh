#!/bin/bash
#!/bin/bash

export LANG=C

# production
#HOST="http://obs.jmmc.fr"
# pre-production
#HOST="http://obs-preprod.jmmc.fr"
# dev
HOST="http://localhost:6543"

echo "HOST: $HOST"

URL="${HOST}/search.votable?valid_level=0"

wget -O bad_exp.votable.gz ${URL}

