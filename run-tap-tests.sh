#!/bin/bash

export LANG=C

DIR="tests"

mkdir $DIR

obsportal-cli eso synchronize --instrument MIDI --settings dev-local.ini --log &> $DIR/tap_midi.log

obsportal-cli eso synchronize --instrument AMBER --settings dev-local.ini --log &> $DIR/tap_amber.log

obsportal-cli eso synchronize --instrument PIONIER --settings dev-local.ini --log &> $DIR/tap_pionier.log

obsportal-cli eso synchronize --instrument GRAVITY --settings dev-local.ini --log &> $DIR/tap_gravity.log

obsportal-cli eso synchronize --instrument MATISSE --settings dev-local.ini --log &> $DIR/tap_matisse.log

