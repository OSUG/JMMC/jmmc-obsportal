#!/bin/bash

N=1 # max 5 threads in dev


# TRAP: Do not leave children jobs running if the shell has been cancelled
cleanup_trap() {
    CHILDREN_PIDS=$(jobs -p)
    if [ -n "$CHILDREN_PIDS" ]
    then
        trap - EXIT
        echo -e "SHELL cancelled, stopping $CHILDREN_PIDS"
        # we may try to send only TERM before a pause and a last loop with KILL signal ?
        kill $CHILDREN_PIDS

        echo -e "SHELL cancelled, waiting on $CHILDREN_PIDS"
        # wait for all pids
        for pid in $CHILDREN_PIDS; do
            wait $pid
        done

        CHILDREN_PIDS=$(jobs -p)
        if [ -n "$CHILDREN_PIDS" ]
        then
            echo -e "SHELL cancelled, killing $CHILDREN_PIDS"
            kill -9 $CHILDREN_PIDS
        fi
  fi
}
trap cleanup_trap EXIT



CMD="./get-loop-votable-queries.sh"

DIR="./tests"

mkdir $DIR

logFile="$DIR/get_votable_"
logExt=txt

echo "Started @ `date`"

for i in $(seq $N)
do
    TH=$((i-1))
    LOG=${logFile}${i}.${logExt}
    echo "Spawn process ${i}), logs in $LOG"
    nohup $CMD $TH < /dev/null > ${logFile}${i}.${logExt} 2>&1 &
done

echo "Waiting ..."

# Wait for runner to end properly:
PID=`ps -ef | grep "$CMD" | grep -v "grep" | grep -v "-bench_votable-queries.sh" | awk '{print $2}'`
N_PID=`echo "${PID}" | wc -w`

while [ "${N_PID}" -ne "0" ]; do
    sleep 1
    PID=`ps -ef | grep "$CMD" | grep -v "grep" | grep -v "-bench_votable-queries.sh" | awk '{print $2}'`
    N_PID=`echo "${PID}" | wc -w`
done

echo "All Processes terminated."

echo "Stopped @ `date`"

