-- defines ObsDB group with users :

CREATE ROLE obsdb_group WITH NOSUPERUSER NOCREATEDB NOCREATEROLE NOLOGIN;
ALTER ROLE obsdb_group SET client_encoding = 'UTF8';

CREATE ROLE obsdb_admin WITH LOGIN PASSWORD 'admin' INHERIT IN ROLE obsdb_group;


-- creates ObsDB database owned by osug_doi_super :

CREATE DATABASE obsdb WITH OWNER = obsdb_admin;

