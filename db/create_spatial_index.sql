-- defines PgSphere spatial index on target table :
CREATE INDEX ix_target_ra_dec_spatial ON obsportal.target USING GIST(spoint(radians(ra), radians(dec)));

