-- obsolete script (2020.02.17):

DROP INDEX obsportal.obs_target_spatial;

-- drop table created by sql alchemy:
DROP TABLE obsportal.obs_target_uniq_obs;

DROP MATERIALIZED VIEW obsportal.obs_target_uniq_obs;
DROP MATERIALIZED VIEW obsportal.obs_target_uniq;
DROP MATERIALIZED VIEW obsportal.obs_target_bad_matches;
DROP MATERIALIZED VIEW obsportal.obs_target_all_matches;
DROP MATERIALIZED VIEW obsportal.obs_target;


-- VIEW obs_target
CREATE MATERIALIZED VIEW obsportal.obs_target AS SELECT DISTINCT target_name, target_ra, target_dec, DENSE_RANK() OVER(ORDER BY target_name, target_dec, target_ra) AS row_number FROM obsportal.observation ORDER BY target_name, row_number;

CREATE INDEX obs_target_spatial ON obsportal.obs_target USING GIST(spoint(radians(target_ra), radians(target_dec)));

-- 10 arcsec = 10 / 3600 = 0.002777777777777778


-- VIEW obs_target_all_matches
-- exclude same row (only extra matches on the right side):
CREATE MATERIALIZED VIEW obsportal.obs_target_all_matches AS SELECT 
    degrees( spoint(radians(o1.target_ra), radians(o1.target_dec)) <-> spoint(radians(o2.target_ra), radians(o2.target_dec)) ) * 3600.0 AS dist_as, 
    o1.row_number as rowNum1, o1.target_name as name1, o1.target_ra as ra1, o1.target_dec as dec1, 
    o2.row_number as rowNum2, o2.target_name as name2, o2.target_ra as ra2, o2.target_dec as dec2 
FROM obsportal.obs_target o1, obsportal.obs_target o2 
WHERE
spoint(radians(o2.target_ra), radians(o2.target_dec)) @ scircle(spoint(radians(o1.target_ra), radians(o1.target_dec)), radians(0.002777777777777778)) 
AND (o2.row_number > o1.row_number) 
ORDER BY o1.row_number, dist_as, o2.row_number;


-- VIEW obs_target_bad_matches
CREATE MATERIALIZED VIEW obsportal.obs_target_bad_matches AS SELECT DISTINCT(rowNum2) as bad_num
FROM obsportal.obs_target_all_matches order by rowNum2;


-- Uniq ids not in obs_target_bad_matches:
CREATE MATERIALIZED VIEW obsportal.obs_target_uniq AS SELECT * 
FROM obsportal.obs_target ot
WHERE ot.row_number NOT IN (SELECT bad_num FROM obsportal.obs_target_bad_matches) ORDER BY ot.target_name, ot.row_number;


CREATE MATERIALIZED VIEW obsportal.obs_target_uniq_obs AS SELECT ot.*,
    (SELECT COUNT(*) FROM obsportal.observation o WHERE (spoint(radians(o.target_ra), radians(o.target_dec)) @ scircle(spoint(radians(ot.target_ra), radians(ot.target_dec)), radians(0.002777777777777778)) ) OR o.target_name = ot.target_name ) as obs_count
FROM obsportal.obs_target_uniq ot;

-- slower:
-- add  OR ot.target_name = o.target_name

