#!/bin/bash

HOSTS="https://obs.jmmc.fr https://obs-preprod.jmmc.fr"
HOSTS="$HOSTS https://obs-test.jmmc.fr"

test -d tests/ || mkdir -v tests

for i in {1..1000}; do
    for HOST in $HOSTS
    do
        URL="${HOST}/health"
        wget -q $URL -O tests/health.html

        # Check Variant variable in prod:    
        VARIANT=`grep -A1 "Variant" tests/health.html | tail -1 `

        if [ ! -z "$VARIANT" ]; then
          echo "$(date) : Variant $VARIANT detected on $HOST"
        else
          echo "$(date) : No variant detected on $HOST"
        fi
            sleep 1
    done
done

