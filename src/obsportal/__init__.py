# coding: utf-8
import logging
import os
from datetime import datetime
from pyramid.config import Configurator
from obsportal.tools.settings import update_settings_with_envvars


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    app_settings = global_config.copy()
    app_settings.update(settings)
    app_settings = update_settings_with_envvars(app_settings, 'obsportal')

    with Configurator(settings=app_settings) as config:
        config.include('.models')
        config.include('pyramid_jinja2')
        config.include('.routes')
        config.scan()

        # Inject root path
        config.registry.settings['obsportal.paths.root'] = os.path.abspath(os.path.dirname(__file__))

        # Inject application version
        app_tag = os.environ.get('IMAGE_TAG')
        if not app_tag:
            app_tag = "UNKNOWN"
        config.registry.settings['obsportal.version.app'] = app_tag

        # Startup timestamp
        date_now = str(datetime.now())
        config.registry.settings['obsportal.start'] = date_now

        logging.getLogger('obsportal').info(f"ObsPortal Tag: '{app_tag}'")
        logging.getLogger('obsportal').info(f"ObsPortal Configurator started at {date_now}")

    return config.make_wsgi_app()
