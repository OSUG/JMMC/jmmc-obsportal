"""empty message

Revision ID: b27b5a751c73
Revises: 826d0b9dcafa
Create Date: 2020-06-04 14:53:36.754556

"""
import sqlalchemy as sa
from alembic import op
# revision identifiers, used by Alembic.
from sqlalchemy_utc import UtcDateTime
from sqlalchemy_utils import UUIDType, ChoiceType
from sqlalchemy.sql import text

from obsportal.models.enums.program import EnumProgramType

revision = 'b27b5a751c73'
down_revision = '826d0b9dcafa'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('database_tag',
                    sa.Column('id', sa.Unicode(), nullable=False),
                    sa.Column('value', sa.Unicode(), nullable=True),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_database_tag')),
                    schema='obsportal'
                    )
    op.create_index(op.f('ix_obsportal_database_tag_value'), 'database_tag', ['value'], unique=False,
                    schema='obsportal')

    op.add_column('exposure', sa.Column('date_release', UtcDateTime(timezone=True), nullable=True), schema='obsportal')

    op.drop_table('program_run', schema='obsportal')
    op.drop_table('program', schema='obsportal')

    op.create_table('program',
                    sa.Column('id', sa.Unicode(), nullable=False),
                    sa.Column('title', sa.Unicode(), nullable=True),
                    sa.Column('type', ChoiceType(EnumProgramType, impl=sa.Integer()), nullable=False),
                    sa.Column('pi_coi', sa.Unicode(), nullable=True),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_program')),
                    schema='obsportal'
                    )

    op.execute(text(
        'INSERT INTO obsportal.program SELECT distinct(o.program_id) as id, null, 0, null FROM obsportal.observation o'))

    op.create_foreign_key(
        constraint_name="fk_observation_program_id_program",
        source_table="observation",
        referent_table="program",
        local_cols=["program_id"],
        remote_cols=["id"], onupdate='CASCADE', ondelete='RESTRICT',
        source_schema='obsportal',
        referent_schema='obsportal'
    )
    # ### end Alembic commands ###


def downgrade():
    op.drop_index(op.f('ix_obsportal_database_tag_value'), table_name='database_tag', schema='obsportal')
    op.drop_table('database_tag', schema='obsportal')

    op.drop_column('exposure', 'date_release', schema='obsportal')

    op.drop_constraint("fk_observation_program_id_program", "observation", schema='obsportal')

    op.drop_table('program', schema='obsportal')
    op.create_table('program',
                    sa.Column('id', UUIDType(), nullable=False),
                    sa.Column('identifier', sa.Unicode(), nullable=True),
                    sa.Column('alternative_identifier', sa.Unicode(), nullable=True),
                    sa.Column('period', sa.Integer(), nullable=True),
                    sa.Column('name', sa.Text(), nullable=True),
                    sa.Column('type', ChoiceType(EnumProgramType), nullable=False),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_program')),
                    schema='obsportal'
                    )

    op.create_table('program_run',
                    sa.Column('id', UUIDType(), nullable=False),
                    sa.Column('program_id', UUIDType(), nullable=True),
                    sa.Column('identifier', sa.Unicode(), nullable=True),
                    sa.Column('alternative_identifier', sa.Unicode(), nullable=True),
                    sa.ForeignKeyConstraint(['program_id'], ['obsportal.program.id'],
                                            name=op.f('fk_program_run_program_id_program'), onupdate='CASCADE',
                                            ondelete='RESTRICT'),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_program_run')),
                    schema='obsportal'
                    )
    # ### end Alembic commands ###
