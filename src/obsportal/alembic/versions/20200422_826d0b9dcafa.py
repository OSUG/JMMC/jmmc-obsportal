"""Release (ObsPortal2020_04_22)

Revision ID: 826d0b9dcafa
Revises: 5a7cecf8ad60
Create Date: 2020-04-22 14:26:12.263211

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '826d0b9dcafa'
down_revision = '5a7cecf8ad60'
branch_labels = None
depends_on = None


def upgrade():
    op.create_index(op.f('ix_obsportal_header_date_updated'), 'header', ['date_updated'], unique=False,
                    schema='obsportal')
    # ### end Alembic commands ###


def downgrade():
    op.drop_index(op.f('ix_obsportal_header_date_updated'), table_name='header', schema='obsportal')
    # ### end Alembic commands ###
