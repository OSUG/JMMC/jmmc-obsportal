def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')

    config.add_route('health', '/health')

    config.add_route('search_votable', '/search.votable')
    config.add_route('search', '/search')

    config.add_route('list_votable', '/list/{type}.votable')
    config.add_route('list', '/list/{type}')

    config.add_route('detail_votable', '/detail/{type}/{id}.votable')
    config.add_route('detail_raw', '/detail/{type}/{id}.raw')
    config.add_route('detail', '/detail/{type}/{id}')
