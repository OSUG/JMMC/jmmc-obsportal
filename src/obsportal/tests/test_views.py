# coding=utf-8
import os
from unittest import TestCase
from pyramid import testing
from obsportal.tools.settings import load_settings


class TestViews(TestCase):

    def setUp(self):
        from webtest import TestApp
        from obsportal import main

        config_uri = os.path.abspath(os.path.join(os.path.basename(__file__), '..', 'testing.ini'))
        settings = load_settings(config_uri)

        app = main({}, **settings)
        self.testapp = TestApp(app)

    def tearDown(self):
        testing.tearDown()

    # HOME #############################################################################################################

    def test_home(self):
        res = self.testapp.get('/', status=200)
        self.assertTrue(b'ObsPortal' in res.body)

    # HEALTH ###########################################################################################################

    def test_health(self):
        res = self.testapp.get('/health', status=200)
        self.assertTrue(b'Health' in res.body)

    # SEARCH ###########################################################################################################

    def test_search_get_empty(self):
        res = self.testapp.get('/search', status=200)
        self.assertTrue(b'Search' in res.body)

    def test_search_post_empty(self):
        res = self.testapp.post('/search', status=200)
        self.assertTrue(b'Search' in res.body)

    def test_search_votable_get_empty(self):
        res = self.testapp.get('/search.votable', status=200)
        self.assertTrue(b'VOTABLE' in res.body)

    def test_search_votable_post_empty(self):
        res = self.testapp.post('/search.votable', status=200)
        self.assertTrue(b'VOTABLE' in res.body)

    # INTERFEROMETER ###################################################################################################

    def test_list_interferometer(self):
        res = self.testapp.get('/list/interferometer', status=200)
        self.assertTrue(b'interferometers' in res.body)

    def test_detail_interferometer_vlti(self):
        res = self.testapp.get('/detail/interferometer/VLTI', status=200)
        self.assertTrue(b'VLTI' in res.body)

    # INSTRUMENT #######################################################################################################

    def test_list_instrument(self):
        res = self.testapp.get('/list/instrument', status=200)
        self.assertTrue(b'instruments' in res.body)

    def test_detail_instrument_amber(self):
        res = self.testapp.get('/detail/instrument/AMBER', status=200)
        self.assertTrue(b'AMBER' in res.body)

    def test_detail_instrument_midi(self):
        res = self.testapp.get('/detail/instrument/MIDI', status=200)
        self.assertTrue(b'MIDI' in res.body)

    def test_detail_instrument_pionier(self):
        res = self.testapp.get('/detail/instrument/PIONIER', status=200)
        self.assertTrue(b'PIONIER' in res.body)

    def test_detail_instrument_gravity(self):
        res = self.testapp.get('/detail/instrument/GRAVITY', status=200)
        self.assertTrue(b'GRAVITY' in res.body)

    def test_detail_instrument_gravity_ft(self):
        res = self.testapp.get('/detail/instrument/GRAVITY_FT', status=200)
        self.assertTrue(b'GRAVITY_FT' in res.body)

    def test_detail_instrument_matisse(self):
        res = self.testapp.get('/detail/instrument/MATISSE', status=200)
        self.assertTrue(b'MATISSE' in res.body)

    # TARGET ###########################################################################################################

    def test_list_target(self):
        res = self.testapp.get('/list/target', status=200)
        self.assertTrue(b'Targets' in res.body)

    def test_detail_target(self):
        res = self.testapp.get('/detail/target/1', status=200)
        self.assertTrue(b'1' in res.body)

    # OBSERVATION ######################################################################################################

    def test_list_observation(self):
        res = self.testapp.get('/list/observation', status=200)
        self.assertTrue(b'Observations' in res.body)

    def test_detail_observation(self):
        res = self.testapp.get('/detail/observation/1', status=200)
        self.assertTrue(b'1' in res.body)

    # HEADER ###########################################################################################################

    def test_list_header(self):
        res = self.testapp.get('/list/header', status=200)
        self.assertTrue(b'Headers' in res.body)

    def test_detail_header(self):
        res = self.testapp.get('/detail/header/AMBER.2004-05-31T06:02:46.487', status=200)
        self.assertTrue(b'AMBER.2004-05-31T06:02:46.487' in res.body)

    # EXPOSURE #########################################################################################################

    def test_list_exposure(self):
        res = self.testapp.get('/list/exposure', status=200)
        self.assertTrue(b'Exposures' in res.body)

    def test_detail_exposure(self):
        res = self.testapp.get('/detail/exposure/AMBER.2004-05-31T06:02:46.487_1', status=200)
        self.assertTrue(b'AMBER.2004-05-31T06:02:46.487_1' in res.body)

