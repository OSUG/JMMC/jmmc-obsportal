# coding=utf-8
from wtforms.form import Form
from wtforms.validators import optional
from wtforms.fields import SelectField


class FormInstrumentNameList(Form):
    def __init__(self, formdata=None, obj=None, prefix='', data=None, meta=None, instruments=[], interferometers=[]):
        super().__init__(formdata, obj, prefix, data, meta)
        self.instrument.choices = [('', "---")] + [(instrument.id, instrument.name) for instrument in instruments]
        self.interferometer.choices = [('', "---")] + [(interferometer.id, interferometer.name) for interferometer in interferometers]

    instrument = SelectField('Instrument', validators=[optional()], default='')
    interferometer = SelectField('Interferometer', validators=[optional()], default='')
