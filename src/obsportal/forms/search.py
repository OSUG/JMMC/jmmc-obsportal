# coding=utf-8
from wtforms.validators import optional
from wtforms.fields import FloatField, SelectField, StringField
from .extended_selectfield import ExtendedSelectField
from .instrument import FormInstrumentNameList


class FormSearch(FormInstrumentNameList):
    def __init__(self, formdata=None, obj=None, prefix='', data=None, meta=None, instruments=[], interferometers=[]):
        super().__init__(formdata, obj, prefix, data, meta, instruments,interferometers)
        self.instrument_mode.choices = [("", "---")] + [(instrument.name, [(instrument_mode.id, instrument_mode.name) for instrument_mode in instrument.modes]) for instrument in instruments]

    instrument_mode = ExtendedSelectField('Instrument mode', validators=[optional()], coerce=int)

    ra = FloatField('RA', validators=[optional()])
    dec = FloatField('DEC', validators=[optional()])
    radius = FloatField('Radius', default=10.0, validators=[optional()])

    target_name = StringField('Name', validators=[optional()])
    program_id = StringField('Program ID', validators=[optional()])

    mjd_from = FloatField('MJD From', validators=[optional()])
    mjd_to = FloatField('MJD To', validators=[optional()])

    format = SelectField('Format', validators=[optional()], choices=[('html', 'HTML'), ('votable', 'VOTable')], default='html')

    valid_level = SelectField('Validation level', validators=[optional()], choices=[('', 'All exposures'), ('1', 'Only valid exposures'), ('0', 'Only invalid exposures')], default='')
