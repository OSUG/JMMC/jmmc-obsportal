# coding: utf-8
import click
import os
import logging
import progressbar
from pyramid.paster import setup_logging

logger = logging.getLogger('obsportal.cli')


def before_command(config_uri, log=None, debug=None):
    # Initialize ProgressBar with logging:
    progressbar.streams.wrap_stderr()

    if log or debug:
        setup_logging(config_uri)

    logger.info("===========================")
    logger.info("| ObsPortal-Client: start |")
    logger.info(f"| config_uri: {config_uri} |")
    logger.info("===========================")


def after_command():
    logger.info("==========================")
    logger.info("| ObsPortal-Client: exit |")
    logger.info("==========================")


def get_config_uri(settings=None):
    config_uri = None

    # Get environment filename
    if settings is not None:
        config_uri = click.format_filename(settings)
    elif os.environ.get('OBSPORTAL_SETTINGS', None) and os.path.exists(os.environ.get('OBSPORTAL_SETTINGS')):
        config_uri = os.environ.get('OBSPORTAL_SETTINGS')

    if config_uri is None:
        if os.path.exists('development.ini'):
            config_uri = 'development.ini'
        elif os.path.exists('production.ini'):
            config_uri = 'production.ini'
        else:
            click.echo(
                "Environment files 'development.ini' or 'production.ini' not found in the current dir. Please use the 'settings' argument.")
            config_uri = None

    # End
    return config_uri


def get_root_path():
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
