# coding: utf-8
import warnings

import click
import os

plugin_folder = os.path.join(os.path.dirname(__file__), 'commands')


class ObsPortalCli(click.MultiCommand):

    def list_commands(self, ctx):
        rv = []
        for filename in os.listdir(plugin_folder):
            if filename.endswith('.py') and not filename.startswith('__'):
                rv.append(filename[:-3])
        rv.sort()
        return rv

    def get_command(self, ctx, name):
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", message="^.*(@font-face|Cairo)*$")

            ns = {}
            try:
                fn = os.path.join(plugin_folder, name + '.py')
                with open(fn) as f:
                    code = compile(f.read(), fn, 'exec')
                    eval(code, ns, ns)
                return ns['cli']
            except IOError:
                pass


cli = ObsPortalCli(help='Welcome to ObsPortal command-line interface. Please use one of the following commands.')

if __name__ == '__main__':
    cli()
