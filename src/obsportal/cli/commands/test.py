# coding: utf-8
import os
import click
import logging
from obsportal.cli.tools import get_root_path

logger = logging.getLogger(__name__)


@click.command(help='Run tests')
def cli():
    config_uri = os.path.abspath(os.path.join(get_root_path(), '..', 'testing.ini'))
    if os.path.exists(config_uri):
        pytest_args = [
            f'-c{config_uri}'
        ]
        import pytest
        pytest.main(pytest_args)
    else:
        click.echo("Please configure the 'testing.ini' file.")
