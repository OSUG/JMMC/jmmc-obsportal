# coding: utf-8
import click
import logging
import traceback
from pyramid.paster import bootstrap
from obsportal.cli.tools import before_command, after_command, get_config_uri
from obsportal.views.exposure import ViewExposureSearchVotable

logger = logging.getLogger('obsportal.cli.profile')


@click.group(help='Profiling')
def cli():
    pass


# DOWNLOAD #############################################################################################################

@cli.command()
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def votable_exposure_search(settings, log, debug):
    config_uri = get_config_uri(settings)
    if config_uri is not None:

        # Init
        before_command(config_uri, log, debug)
        logger.info("get votable exposure search")
        env = bootstrap(config_uri)

        #cp = cProfile.Profile()

        # Run
        try:
            with env['request'].tm:
                view = ViewExposureSearchVotable(env['request'], env['request'].dbsession)
                render = view()
                print(render)

                #cp.enable()
                #for i in range(0, 100):
                #    render = view()
                #cp.disable()
            #cp.print_stats()

            click.echo("Done.")
        except Exception as e:
            logger.warning(f"votable exposure search failure:", e)
            if debug:
                traceback.print_exc()
            else:
                click.echo(e)
        finally:
            after_command()
