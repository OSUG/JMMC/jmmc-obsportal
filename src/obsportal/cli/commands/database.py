# coding: utf-8
from pkgutil import iter_modules
import logging
import os
import click
import traceback
import alembic.config, alembic.command

from sqlalchemy import engine_from_config, event, text, MetaData
from sqlalchemy.orm import class_mapper
from sqlalchemy.exc import OperationalError
from sqlalchemy.schema import CreateSchema, DropSchema
from sqlalchemy_utils.functions import database_exists, create_database, drop_database

from pyramid.paster import bootstrap
from pyramid.config import Configurator

from lxml import etree

from obsportal.models.meta import Base, SCHEMAS
from obsportal.cli.tools import before_command, after_command, get_config_uri, get_root_path
from obsportal.tools.settings import load_settings

logger = logging.getLogger('obsportal.cli.database')


@click.group(help='Database management')
def cli():
    pass


@cli.command()
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def init(settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("init database ...")

        # Get environment
        env = bootstrap(config_uri)

        # Import model
        from obsportal.models.interferometer import Interferometer
        from obsportal.models.instrument import Instrument, InstrumentMode

        try:
            with env['request'].tm:
                db_session = env['request'].dbsession

                # load CHARA conf
                loadConfXml(db_session, 'chara.xml')

                # load VLTI conf:
                loadConfXml(db_session, 'eso_vlti.xml')

        except Exception as e:
            if debug:
                traceback.print_exc()
            else:
                click.echo(e)
        after_command()


def loadConfXml(db_session, conf_file):
    # Import model
    from obsportal.models.interferometer import Interferometer
    from obsportal.models.instrument import Instrument, InstrumentMode

    # load VLTI conf:
    xml_tree = etree.parse(os.path.join(get_root_path(), 'config', 'instruments', conf_file))
    xml_root = xml_tree.getroot()

    for xml_interferometer in xml_root.iterchildren('interferometer'):
        interferometer_name = xml_interferometer.find('name').text

        interferometer = db_session.query(Interferometer).filter_by(name=interferometer_name).one_or_none()
        if interferometer is None:
            interferometer_lon = xml_interferometer.find('lon').text
            interferometer_lat = xml_interferometer.find('lat').text
            interferometer_alt = xml_interferometer.find('alt').text

            interferometer = Interferometer()
            interferometer.id = interferometer_name
            interferometer.name = interferometer_name
            interferometer.longitude = interferometer_lon
            interferometer.latitude = interferometer_lat
            interferometer.altitude = interferometer_alt
            db_session.add(interferometer)

        for xml_instrument in xml_interferometer.iterchildren('instrument'):
            instrument_name = xml_instrument.find('name').text

            instrument = db_session.query(Instrument).filter_by(name=instrument_name).one_or_none()
            if instrument is None:
                instrument = Instrument()
                instrument.interferometer = interferometer
                instrument.id = instrument_name
                instrument.name = instrument_name
                db_session.add(instrument)

            for xml_instrument_mode in xml_instrument.iterchildren('mode'):
                instrument_mode_name = xml_instrument_mode.find('name').text

                instrument_mode = db_session.query(InstrumentMode).filter_by(
                    name=instrument_mode_name).one_or_none()

                # TODO: update wavelength ranges (if changed ?)
                if instrument_mode is None:
                    instrument_mode_wmin = xml_instrument_mode.find('wave_min').text
                    instrument_mode_wmax = xml_instrument_mode.find('wave_max').text
                    instrument_mode_specres = xml_instrument_mode.find('spec_res').text

                    instrument_mode = InstrumentMode()
                    instrument_mode.instrument = instrument
                    instrument_mode.name = instrument_mode_name
                    instrument_mode.wavelength_min = instrument_mode_wmin
                    instrument_mode.wavelength_max = instrument_mode_wmax
                    instrument_mode.spectral_resolution = instrument_mode_specres
                    db_session.add(instrument_mode)


@cli.command()
@click.option('--encoding', default='utf8')
@click.option('--template', default='template0')
@click.option('--structure/--no-structure', default=True, help='create structure')
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
@click.option('--skip_exist', is_flag=False, help='Skip database_exists() check')
def create(encoding, template, structure, settings, log, debug, skip_exist):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)
        logger.info("create database ...")

        app_settings = load_settings(config_uri)
        engine = engine_from_config(app_settings, 'sqlalchemy.')
        config = Configurator(settings=app_settings)

        # CHECK IF DB EXISTS
        logger.info(f"db url = '{engine.url}'")

        if not skip_exist and database_exists(engine.url):
            click.echo("ERROR: The database already exists")
        else:
            # CREATE THE DB
            create_database(engine.url, encoding=encoding, template=template)

            # CREATE EXTENSIONS
            with engine.connect() as connection:
                connection.execute(text('CREATE EXTENSION IF NOT EXISTS pg_sphere;'))

            # CREATE DATABASE STRUCTURE
            if structure:
                # CREATE SCHEMAS
                for schema in SCHEMAS:
                    event.listen(Base.metadata, 'before_create', CreateSchema(schema))

                try:
                    # CREATE TABLES
                    config.scan('obsportal.models')
                    Base.metadata.create_all(engine)
                    click.echo("DONE: Database created.")

                    # CREATE SPATIAL INDEXES
                    with engine.connect() as connection:
                        connection.execute(text(
                            'CREATE INDEX ix_target_ra_dec_spatial ON obsportal.target USING GIST(spoint(radians(ra), radians(dec)));'))

                    # INITIALIZE ALEMBIC
                    alembic_cfg = alembic.config.Config(config_uri)
                    alembic.command.stamp(alembic_cfg, "head")
                except Exception as e:
                    logger.error("creation failed", e)
                    click.echo("database.create() failed")
                    raise
        after_command()


@cli.command()
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
@click.option('--skip_exist', is_flag=True, help='Skip database_exists() check')
def create_structure(settings, log, debug, skip_exist):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("create database structure ...")

        app_settings = load_settings(config_uri)
        engine = engine_from_config(app_settings, 'sqlalchemy.')
        config = Configurator(settings=app_settings)

        # CHECK DATABASE
        if not skip_exist:
            try:
                with engine.connect() as connection:
                    connection.execute(text('SELECT 1'))
            except OperationalError:
                click.echo("ERROR: The database doesn't exist")
                raise

        # CREATE SCHEMAS
        for schema in SCHEMAS:
            event.listen(Base.metadata, 'before_create', CreateSchema(schema))


        try:
            # CREATE TABLES
            config.scan('obsportal.models')
            Base.metadata.create_all(engine)
            click.echo("DONE: Database created.")

            # CREATE SPATIAL INDEXES
            with engine.connect() as connection:
                connection.execute(text(
                    'CREATE INDEX ix_target_ra_dec_spatial ON obsportal.target USING GIST(spoint(radians(ra), radians(dec)));'))

            # INITIALIZE ALEMBIC
            alembic_cfg = alembic.config.Config(config_uri)
            alembic.command.stamp(alembic_cfg, "head")
        except Exception as e:
            logger.error("create structure failed", e)
            click.echo("database.create_structure() failed")
            raise
        after_command()


@cli.command()
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def drop(settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("drop database")

        app_settings = load_settings(config_uri)
        engine = engine_from_config(app_settings, 'sqlalchemy.')

        # CHECK IF DB EXISTS
        if not database_exists(engine.url):
            click.echo("ERROR: The database doesn't exist")
        else:
            # DROP THE DB
            drop_database(engine.url)
            click.echo("DONE: Database dropped.")
        after_command()


@cli.command()
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def drop_structure(settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("drop database structure ...")

        app_settings = load_settings(config_uri)
        engine = engine_from_config(app_settings, 'sqlalchemy.')

        try:
            with engine.connect() as connection:
                for schema in SCHEMAS:
                    connection.execute(DropSchema(schema, cascade=True))
                click.echo("DONE: Database structure dropped.")

        except OperationalError:
            click.echo("ERROR: The database doesn't exist")
            raise

        after_command()


@cli.command()
@click.option('--version', default='head')
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def upgrade(version, settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("upgrade database ...")

        # Configure Alembic
        alembic_cfg = alembic.config.Config(config_uri)

        # Upgrade DB through Alembic
        try:
            alembic.command.upgrade(alembic_cfg, version)
            click.echo("DONE: Database upgraded.")
        except Exception as e:
            logger.error("database upgrade failed", e)
            click.echo("database.upgrade() failed")
        after_command()


@cli.command()
@click.argument('version')
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def downgrade(version, settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("database downgrade ...")

        # Configure Alembic
        alembic_cfg = alembic.config.Config(config_uri)

        # Upgrade DB through Alembic
        alembic.command.downgrade(alembic_cfg, version)
        click.echo("DONE: Database downgraded.")
    after_command()


@cli.command()
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def current(settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("database current ...")

        # Configure Alembic
        alembic_cfg = alembic.config.Config(config_uri)

        # Get current DB version
        alembic.command.current(alembic_cfg)
        after_command()


@cli.command()
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def history(settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("database history ...")

        # Configure Alembic
        alembic_cfg = alembic.config.Config(config_uri)

        # Get DB history
        alembic.command.history(alembic_cfg, indicate_current=True)
        after_command()


@cli.command()
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def revision(settings, log, debug):
    autogenerate = True

    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("database revision ...")

        # Configure Alembic
        alembic_cfg = alembic.config.Config(config_uri)

        # Create new revision
        alembic.command.revision(alembic_cfg, autogenerate=autogenerate)
        click.echo("DONE: New revision created.")
        after_command()


@cli.command()
@click.argument('version')
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def stamp(version, settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("database stamp ...")

        # Configure Alembic
        alembic_cfg = alembic.config.Config(config_uri)

        # Upgrade DB through Alembic
        alembic.command.stamp(alembic_cfg, version)
        click.echo("DONE: Database stamped to version '%s'." % version)
        after_command()


@cli.command()
@click.argument('output_file')
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def diagram_er(output_file, settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("database diagram_er ...")

        if 'sqlalchemy_schemadisplay' in (name for loader, name, ispkg in iter_modules()):
            from sqlalchemy_schemadisplay import create_schema_graph
            click.echo("Rendering diagram from database...")

            app_settings = load_settings(config_uri)
            engine = engine_from_config(app_settings, 'sqlalchemy.')

            # See https://github.com/sqlalchemy/sqlalchemy/wiki/SchemaDisplay
            # create the pydot graph object by autoloading all tables via a bound metadata object
            graph = create_schema_graph(metadata=MetaData(str(engine.url), schema='obsportal'),
                                        show_datatypes=False,  # The image would get nasty big if we'd show the datatypes
                                        show_indexes=False,  # ditto for indexes
                                        rankdir='LR',  # From left to right (instead of top to bottom)
                                        concentrate=False  # Don't try to join the relation lines together
                                        )
            graph.write_png(output_file)  # write out the file

        else:
            click.echo("Please install the Python module 'sqlalchemy_schemadisplay' to use this feature")

        after_command()


@cli.command()
@click.argument('output_file')
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def diagram_uml(output_file, settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info("database diagram_uml ...")

        if 'sqlalchemy_schemadisplay' in (name for loader, name, ispkg in iter_modules()):
            from sqlalchemy_schemadisplay import create_uml_graph
            click.echo("Rendering diagram from database...")

            from obsportal.models.database_tag import DatabaseTag
            from obsportal.models.interferometer import Interferometer
            from obsportal.models.instrument import Instrument, InstrumentMode
            from obsportal.models.program import Program
            from obsportal.models.header import Header, EsoHeader, CharaHeader
            from obsportal.models.observation import Observation
            from obsportal.models.exposure import Exposure
            from obsportal.models.target import Target

            mappers = []
            mappers.append(class_mapper(DatabaseTag))
            mappers.append(class_mapper(Interferometer))
            mappers.append(class_mapper(Instrument))
            mappers.append(class_mapper(InstrumentMode))
            mappers.append(class_mapper(Header))
            mappers.append(class_mapper(EsoHeader))
            mappers.append(class_mapper(CharaHeader))
            mappers.append(class_mapper(Observation))
            mappers.append(class_mapper(Exposure))
            mappers.append(class_mapper(Target))
            mappers.append(class_mapper(Program))

            graph = create_uml_graph(mappers,
                                     show_operations=False,  # not necessary in this case
                                     show_multiplicity_one=False  # some people like to see the ones, some don't
                                     )
            graph.write_png(output_file)  # write out the file

        else:
            click.echo("Please install the Python module 'sqlalchemy_schemadisplay' to use this feature")

        after_command()


@cli.command()
@click.argument('sql_file')
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
def run_sql_script(sql_file, settings, log, debug):
    # Get config_uri
    config_uri = get_config_uri(settings)
    if config_uri is not None:
        before_command(config_uri, log, debug)

        logger.info(f"database run_sql_script : '{sql_file}'")

        # Get environment
        env = bootstrap(config_uri)

        session_factory = env['registry']['dbsession_factory']

        # Open the .sql file
        sql_file = open(sql_file, 'r')

        # Create an empty command string
        sql_command = ''

        # Iterate over all lines in the sql file
        for line in sql_file:
            # Ignore commented lines
            if not line.startswith('--') and line.strip('\n'):
                # Append line to the command string
                sql_command += line.strip('\n') + ' '

                # If the command string ends with ';', it is a full statement
                if sql_command.endswith('; '):
                    # Try to execute statement and commit it
                    db_session = session_factory()
                    try:
                        logger.info(f"Execute SQL: '{sql_command}'")
                        db_session.execute(text(sql_command))
                        db_session.commit()
                    except Exception as e:
                        db_session.rollback()
                        if debug:
                            traceback.print_exc()
                        else:
                            click.echo(e)
                    finally:
                        db_session.close()
                        sql_command = ''
        after_command()
