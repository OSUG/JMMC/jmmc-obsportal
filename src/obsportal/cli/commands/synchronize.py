# coding: utf-8
import os
import click
import logging
import traceback
from datetime import datetime
from pyramid.paster import bootstrap
from obsportal.cli.tools import before_command, after_command, get_config_uri
from obsportal.services.eso.service_eso import ServiceEso
from obsportal.services.chara.service_chara import ServiceChara

logger = logging.getLogger('obsportal.cli.synchronize')


@click.command(help='ObsPortal content synchronization')
@click.option('--limit', default=None, help='maximum number of records to harvest per instrument', type=int)
@click.option('--force-update', is_flag=True, help='force update')
@click.option('--services', default=None, help='list of services to query comma separated', type=str)
@click.option('--settings', default=None, help='settings file', type=click.Path(exists=True))
@click.option('--log/--no-log', default=True, help='activate logs')
@click.option('--debug', is_flag=True, help='debug mode')
@click.option('--check-lockfile', 'check_lockfile', is_flag=True, help='check the lock file')
def cli(limit, force_update, services, settings, log, debug, check_lockfile):
    config_uri = get_config_uri(settings)
    if config_uri is not None:

        # Init
        before_command(config_uri, log, debug)
        logger.info("init")
        env = bootstrap(config_uri)

        # Lock file
        app_settings = env['registry'].settings
        lock_filepath = app_settings.get('obsportal.locks.synchronize') or '/tmp/synchronize.lock'

        if check_lockfile and os.path.exists(lock_filepath):
            logger.warning('ObsPortal content is already under synchronization. Please wait!')
            after_command()
            return None

        # Run
        try:
            # Create lockfile
            logger.info(f"Creating lockfile '{lock_filepath}'")
            with open(lock_filepath, 'w') as lock_file:
                lock_file.write(f"Started at: {str(datetime.now())}")

            # Run ESO synchronization
            if not services or 'eso' in services.lower():
                service_eso = ServiceEso(env['request'], env['registry']['dbsession_factory'])
                service_eso.synchronize( limit=limit, debug=debug, force_update=force_update)

            # Run CHARA synchronization
            if not services or 'chara' in services.lower():
                service_chara = ServiceChara(env['request'], env['registry']['dbsession_factory'])
                service_chara.synchronize( limit=limit, debug=debug, force_update=force_update)

            click.echo("Done. ObsPortal content synchronized")

        except (KeyboardInterrupt, SystemExit) as x:
            logger.warning("Interrupted")
        except Exception as e:
            logger.warning("failure:", exc_info=e)
            if debug:
                traceback.print_exc()
            else:
                click.echo(e)
        finally:
            # Remove lockfile
            if os.path.exists(lock_filepath):
                logger.info(f"Removing lockfile '{lock_filepath}'")
                os.remove(lock_filepath)

            after_command()
