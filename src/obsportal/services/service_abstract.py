# coding: utf-8
import signal

import arrow
import logging
import re
from pyvo.dal.tap import TAPService
from dateutil.parser import parse
from datetime import datetime

from obsportal.models.header import Header
from obsportal.models.instrument import Instrument, InstrumentMode
from obsportal.models.interferometer import Interferometer
from obsportal.models.observation import Observation
from obsportal.models.target import Target

from timeit import default_timer as timer

from ..models.database_tag import DatabaseTag
from ..models.utils.memcache import MemCache

# TODO CHARA: simplify and test regexps:
regex_header_id = re.compile('^([A-Z]+)\.(\d{4})-(\d{2})-(\d{2})T\d{2}:\d{2}:\d{2}(?:\.\d*)?$')
regex_header_id_from_file = re.compile(
    '^((?:[A-Z]+)\.(?:\d{4}-\d{2}-\d{2})T\d{2}:\d{2}:\d{2}(?:\.\d*)?)(?:\.txt|log|hdr|fits)?$')
regex_date_from_header_id = re.compile('^(?:[A-Z]+)\.(\d{4}-\d{2}-\d{2})T\d{2}:\d{2}:\d{2}(?:\.\d*)?$')

logger = logging.getLogger(__name__)
memcache = MemCache()

class ServiceAbstract:

    def __init__(self, request, session_factory, interferometer_id):
        """
        Handle synchronization for a given interferometer id.
        Loop over its associated instruments (retrieved from db) and try to synchronize for each of them.
        At present time, ESO and CHARA services extend this class and specialize the instrument process.

        :param request: magic proxy
        :param session_factory: session_factory to play with the db
        :param interferometer_id: facility id
        """
        self.session_factory = session_factory
        self.settings = request.registry.settings
        # register signal handlers
        self.flag_interrupted = False
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)
        # acts as a singleton:
        memcache.setEnabled(True)
        self.interferometer_id=interferometer_id
        # next lines may change in the future
        from obsportal.services.eso.factory import EsoHeaderFactory, EsoWrapperFactory
        self.wrapper_factory = EsoWrapperFactory(self.settings)
        self.header_factory =  EsoHeaderFactory(self.settings)

    def exit_gracefully(self, signum, frame):
        logger.warning(f"ServiceAbstract: Captured Signal [{signum}]: exiting ...")
        self.flag_interrupted = True
        raise SystemExit

    # DB Session handling ##############################################################################################

    def get_session(self):
        db_session = self.session_factory()
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"db_session: {db_session}")
        return db_session

    @staticmethod
    def begin(db_session):
        # clear memory cache anyway:
        memcache.clear()

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"begin {db_session}")
        # implicit tx begin in sql alchemy

    @staticmethod
    def commit(db_session):
        # dump memory cache:
        memcache.dump()

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"commit {db_session}")
        db_session.commit()

    @staticmethod
    def rollback(db_session):
        # dump memory cache:
        memcache.dump()

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"rollback {db_session}")
        db_session.rollback()

    @staticmethod
    def close(db_session):
        # dump memory cache:
        memcache.dump()
        # clear memory cache anyway:
        memcache.clear()

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"close {db_session}")
        db_session.close()

    # SYNCHRONIZE ######################################################################################################

    def synchronize(self, limit=None, debug=False,  force_update = False):
        """
        Synchronize new/updated headers with associated service

        :param limit: The maximum number of results
        :param debug: Activating the debug mode
        :return: None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"interferometer_id = {self.interferometer_id}  limit = {limit}, debug = {debug}")

        facility_name=self.interferometer_id

        # List all instruments of VLTI but CHARA may be added here or a refactoring may store this in eso/chara codes
        excluded_instruments = ['GRAVITY_FT']
        instruments = []

        # TEST ONLY:
        # if False:
        #   excluded_instruments = ['GRAVITY_FT', 'AMBER', 'MIDI', 'PIONIER']
        #   limit = 5000

        # Get all instruments from service to synchronize:
        # update database tags:
        db_session = self.get_session()
        try:
            # check database version:
            if not DatabaseTag.is_current_db_version(db_session):
                force_update = True

            logger.info(f"force_update = {force_update}")

            # query instruments with service's facility:
            facility = Interferometer.get(db_session, facility_name)
            if facility:
                for instrument in facility.instruments:
                    if instrument.id not in excluded_instruments:
                        instruments.append(instrument.id)
            else:
                logger.warning(f"Unable to find the '{facility_name}' interferometer in database.")

            # TODO CHARA: update database tags ONCE (not twice: eso then chara) => move in the pre/post synchronisation
            # Start transaction
            ServiceAbstract.begin(db_session)

            DatabaseTag.set_synchronize_start_now(db_session)

            # Commit transaction
            ServiceAbstract.commit(db_session)

        except BaseException as e:
            ServiceAbstract.rollback(db_session)
            logger.warning("failure",e)
            raise e
        finally:
            ServiceAbstract.close(db_session)

        # Synchronize headers for each instrument
        for instrument in instruments:
            self._synchronize_instrument(instrument, limit=limit, force_update=force_update, debug=debug)

        # TODO CHARA: update stats ONCE (not twice: eso then chara) => move in the pre/post synchronisation
        # Compute stats:
        db_session = self.get_session()
        try:
            # Start transaction
            ServiceAbstract.begin(db_session)

            self._update_stats(db_session)

            # Commit transaction
            ServiceAbstract.commit(db_session)

        except BaseException as e:
            ServiceAbstract.rollback(db_session)
            logger.warning("failure",e)
            raise e
        finally:
            ServiceAbstract.close(db_session)

        # TODO CHARA: update database tags ONCE (not twice: eso then chara) => move in the pre/post synchronisation
        # finally update database tags:
        db_session = self.get_session()
        try:
            # Start transaction
            ServiceAbstract.begin(db_session)

            if force_update:
                # Update database version to current
                DatabaseTag.set_current_db_version(db_session)

            DatabaseTag.set_synchronize_end_now(db_session)

            # Commit transaction
            ServiceAbstract.commit(db_session)

        except BaseException as e:
            ServiceAbstract.rollback(db_session)
            logger.warning("failure",e)
            raise e
        finally:
            ServiceAbstract.close(db_session)

    def _synchronize_header(self, db_session, header_id, date_updated, attributes, force_update=False):
        """
        Synchronize an header

        :param db_session: The database session
        :param header_id: The header ID
        :param date_updated: The last modification date of the header
        :param attributes: other attributes (TAP ESO)
        :param force_update: Enforce the update of existing headers
        :return: boolean|None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_id {header_id} date_updated {date_updated}")

        # Try to load an existing header from database for this header_id
        header = self.header_factory.from_database(db_session, header_id)

        # Case 1: Header in DB, and up-to-date
        if header and not force_update and header.date_updated >= date_updated:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"header '{header_id}' up-to-date in DB")
            # TODO: Check cache file ?
            return header.is_observation

        # Cases 2 and 3: Header in DB but obsolete, or missing in DB
        else:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"header '{header_id}' obsolete or missing in DB")

            # Try to load a cache file
            wrapper = self.wrapper_factory.from_cache(header_id, date_updated)

            # Else, load from ESO
            if not wrapper:
                wrapper = self.wrapper_factory.from_eso(header_id, date_updated)

            # Create/Update an header from the wrapper
            header = self.header_factory.from_wrapper(wrapper, date_updated, header)

            # Save in database
            return self._save_header(db_session, header, attributes, wrapper)

    @staticmethod
    def _save_header(db_session, header, attributes, wrapper):
        """
        Save an header in database

        :param db_session: The database session
        :param header: The Header object
        :param attributes: other attributes (TAP ESO)
        :param wrapper: The Wrapper object
        :return: True if the header is an observation
        :rtype: bool
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"save_header: header {wrapper.header_id} is new")

        # Save observation/exposure if needed
        header.is_observation = wrapper.is_observation()
        if header.is_observation:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"save_header: header {wrapper.header_id} is an observation")
            wrapper.save(db_session, header, attributes)
        else:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"save_header: header {wrapper.header_id} is NOT an observation")

        # Save header validation logs/dictionary
        for message in wrapper._all_validation_messages:
            header.add_message(message)

            if message.startswith("Invalid 'DPR"):
                if logger.isEnabledFor(logging.DEBUG):
                    logger.debug(f"[{wrapper.header_id}] {message}")
            else:
                logger.info(f"[{wrapper.header_id}] {message}")

        for key in wrapper.extracted_content:
            header.add_content(key, wrapper.extracted_content[key])

        # Mark the object to be added to DB
        db_session.add(header)

        return header.is_observation

    @staticmethod
    def _update_stats(db_session):
        """
        Update stats in DB
        :param db_session: The database session
        :return: None
        """
        logger.info("start")
        Observation.update_stats(db_session)
        Target.update_stats(db_session)
        InstrumentMode.update_stats(db_session)
        Instrument.update_stats(db_session)
        Interferometer.update_stats(db_session)
        logger.info("end")


