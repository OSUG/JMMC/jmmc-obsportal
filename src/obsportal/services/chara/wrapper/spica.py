# coding: utf-8
import logging

from .chara_wrapper_abstract import CharaWrapperAbstract
from ...eso.wrapper.enums.keywords import EnumCharaWrapperKeyword as Keyword

logger = logging.getLogger(__name__)


class CharaWrapperSpica(CharaWrapperAbstract):

    def __init__(self, header_dict, header_id=None, header_hash=None, last_updated=None):
        super(CharaWrapperSpica, self).__init__(header_dict, header_id, header_hash, last_updated, 'SPICA')

    # VALIDATE #########################################################################################################
    def is_observation(self):
        """
        Check if the current wrapper can be used to create an observation

        :return: bool
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_file {self.header_id}")

        #if not super().is_observation():
        #    return False

        dp_type = self.dp_type
        if self.dp_cat == 'SCIENCE':
            if dp_type not in ['OBJECT']:
                self._context_validation_messages.append(f"Invalid 'DPR TYPE' = '{dp_type}' (science)")
                return False
        else:
            # CALIB:
            if dp_type not in ['STD']:
                if dp_type not in ["DARK", "LAMP", "NOFRINGE", "SOURCE", "SOURCE,FLUX", "SOURCE,NOFRINGE"]:
                    self._context_validation_messages.append(f"Invalid 'DPR TYPE' = '{dp_type}' (calib)")
                return False

        return True

    # DATE #############################################################################################################
    @property
    def exposure_duration(self):
        """
        Extract value for exposure duration

        :return: float|None
        """
        dit = self.get(Keyword.DET_DIT)
        ndit = self.get(Keyword.DET_NDIT)
        if dit is not None and ndit is not None:
            return dit * ndit  # seconds
        else:
            return self.get(Keyword.EXPTIME)

    # CUSTOM PROPERTIES ################################################################################################
    @property
    def instrument_mode_id(self):
        """
        Extract value for instrument mode (key)

        :return: str|None
        """

        res = self.get(Keyword.INS_RESOL)
        cwl = self.getFloat(Keyword.INS_CENTRAL_WL)

        if res.startswith("L"):
            return "LR"
        elif res.startswith("H"):
            return "HR"
        elif res.startswith("M"):
            return "MR"+ str(int(cwl//1))
        else:
            return None

    @property
    def target_ra(self):
        """
        Extract value for target RA

        :return: float
        """
        return self.getFloat(Keyword.RA)

    @property
    def target_dec(self):
        """
        Extract value for target RA

        :return: float
        """
        return self.getFloat(Keyword.DEC)

    # INSTRUMENT #######################################################################################################
    @property
    def program_id(self):
        """
        Extract value for Program ID

        :return: str|None
        """
        return self.get(Keyword.OBS_PROG_ID)

    @property
    def stations(self):
        """
        Extract stations' list as string

        :return: str|None
        """
        return self.get(Keyword.BASELINE)


    # We have to overide this property since Keyword.ISS_CONF_NTET differs between parent wrapper (EnumESOWrapperKeyword)
    # and current's EnumCharaWrapperKeyword
    @property
    def _stations_as_dict(self):
        """
         Extract stations' list as dictionary

         :return: dict
         """
        stations = {}

        stations_number = self.get(Keyword.ISS_CONF_NTEL)
        if stations_number is not None:
             for i in range(1, stations_number + 1):
                 stations[i] = self.get(
                     Keyword.ISS_CONF_STATION(i), default='')

        return stations

    @property
    def avoid_projected_baselines(self):
        """
        Extract projected baselines' list as dictionary

        :return: dict
        """
        # TODO ask for addition of
        # Keywords ISS_PBL_START/END, ISS_PBLA_START/END
        # and remove this property or implement CHARA/SPICA specific rules
        return {}

    @property
    def projected_baselines(self):
        """
        Extract projected baselines' list as dictionary

        :return: dict
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"is_observation: header_file {self.header_id}")

        projected_baselines = {}

        stations = self._stations_as_dict
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"stations_as_dict: {self.stations}")

        stations_number = self.get(Keyword.ISS_CONF_NTEL)
        if stations_number is not None and stations is not None:

            for i in range(1, stations_number):
                for j in range(i + 1, stations_number + 1):
                    projected_baselines['%s-%s' % (stations[i], stations[j])] = {
                        'length': {
                            'start': self.get(Keyword.ISS_PBL_START(i, j), format='%.4f'),
                            'end': self.get(Keyword.ISS_PBL_END(i, j), format='%.4f'),
                        },
                        'angle': {
                            'start': self.get(Keyword.ISS_PBLA_START(i, j), format='%.4f'),
                            'end': self.get(Keyword.ISS_PBLA_END(i, j), format='%.4f'),
                        }
                    }
        return projected_baselines


    # WEATHER CONDITIONS ###############################################################################################
    @property
    def tau0(self):
        return self.get(Keyword.TAU0)

    @property
    def temperature(self):
        #return self.get(Keyword.ISS_AMBI_TEMP)
        return -1.234

    @property
    def seeing(self):
        seeing_as = self.get(Keyword.SEEING)
        return seeing_as
