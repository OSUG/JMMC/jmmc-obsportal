# coding: utf-8
import logging

from obsportal.services.eso.wrapper.eso_wrapper_abstract import EsoWrapperAbstract
from obsportal.models.header import CharaHeader

logger = logging.getLogger(__name__)


class CharaWrapperAbstract(EsoWrapperAbstract):

    def __init__(self, header_dict, header_id=None, header_hash=None, last_updated=None, instrument_id=None, context=None):
        super(CharaWrapperAbstract, self).__init__(header_dict, header_id, header_hash, last_updated, instrument_id, context)
        self.interferometer_id="CHARA"

    @property
    def header_relative_path(self):
        d = self.header_date
        return "-".join([str(d.year), f"{d.month:02d}", f"{d.day:02d}"])

    def get_new_header(self):
        return CharaHeader()
