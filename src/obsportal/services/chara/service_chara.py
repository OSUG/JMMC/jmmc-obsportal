# coding: utf-8

import logging
from obsportal.services.eso.factory import EsoFactoryError
import os
import re

from obsportal.services.service_abstract import ServiceAbstract

from obsportal.models.enums.tapfields import EnumEsoTapField


logger = logging.getLogger(__name__)

class ServiceChara(ServiceAbstract):

    def __init__(self, request, session_factory):
        ServiceAbstract.__init__(self, request, session_factory, "CHARA")
        self._chara = None

    # SYNCHRONIZE ######################################################################################################



    def _synchronize_instrument(self, instrument, limit=None, force_update=False, debug=False):
        """
        Synchronize new/updated headers for the instrument

        :param instrument: The instrument name
        :param limit: The maximum number of results
        :param force_update: Enforce the update of existing headers
        :param debug: Activating the debug mode
        :return: None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"instrument '{instrument}', limit '{limit}', force_update '{force_update}', debug '{debug}'")

        directory = os.path.join(self.settings.get('obsportal.paths.data'), 'chara', instrument)
        logger.info(f"looking into '{directory}':")
        header_files = self._scan_path(directory)
        if header_files:
            logger.info(f"{len(header_files)} files to import from folder {directory}.")
            count = 0
            try:
                for header_file in header_files:
                    try:
                        # create a new DB session each time:
                        if self.import_from_file(header_file, force_update=force_update):
                            count = count + 1
                            logger.debug(f"header_file import done with success\n\n\n")
                    except Exception as e:
                        logger.warning("failure: %s", e, exc_info=1)
                        logger.debug(f"header_file import aborted\n\n\n")
            finally:
                logger.info(f"{count} / {len(header_files)} files imported.")
        else:
            # No results
            logger.debug('no headers found!')

    def import_from_file(self, header_file, force_update):
        db_session = self.get_session()
        try:
            result = self._import_from_file(db_session, header_file, force_update=force_update)
            self.commit(db_session)
            return result
        except Exception as e:
            self.rollback(db_session)
            raise e
        finally:
            self.close(db_session)

    def _import_from_file(self, db_session, header_file, force_update):
        # TODO CHARA: refactor/merge with parent synchronize_header
        logger.debug(f"import_from_file: header_file {header_file}")
        try:
            # Try to extract header ID from filename
            header_id = self._extract_header_id_from_path(header_file)

            # If header_id is extracted from filename
            if header_id is not None:
                logger.debug(f"import_from_file: extracted header_id {header_id} from header_file {header_file}")

                # Try to load an existing header from database for this header_id
                header = self.header_factory.from_database(db_session, header_id)

                logger.debug("header "+"not "*(not header) + "found in database")

                # If the header does not exist, initialize it from the wrapper
                if header is None or force_update:
                    # Load file content into wrapper
                    wrapper = self.wrapper_factory.from_file(header_file, header_id)

                    # Create/Update an header from the wrapper
                    header = self.header_factory.from_wrapper(wrapper, header=header)

                    # Retrieve program from specific keyword
                    # TODO make it more generic when other CHARA header could come ?
                    attributes={}
                    attributes[EnumEsoTapField.PROG_ID] = wrapper.get("HIERARCH OBS PROG ID")
                    attributes[EnumEsoTapField.PI_COI] = wrapper.get("PI-COI")
                    attributes[EnumEsoTapField.PROG_TITLE]="TBD"
                    attributes[EnumEsoTapField.PROG_TYPE]=None

                    return self._save_header(db_session, header, attributes, wrapper)

            # Else, If header_id can't be extracted from filename
            else:
                # TODO CHARA: !
                logger.error("FIX THIS CODE")

            # Else, already analyzed header
            logger.debug(f"import_from_file: header {header_id} is already analyzed")

        except EsoFactoryError as e:
            logger.error(f"import_from_file: {e}")

        # TOOLS ############################################################################################################

    @staticmethod
    def _extract_header_id_from_path(file_path):
        """
        Specific to CHARA/ SPICA at present time. May be generalized later...
        """
        file_name = os.path.basename(file_path)

        m=re.match("(.*?)\.(fits|txt$)", file_name) # all before .fits/txt extension
        if m is not None:
            header_id = m.group(1)
            # /data/chara/SPICA/2024-09-30/SPICA.2024-09-30T08-46-58.txt
            # => SPICA.2024-09-30T08-46-58
            # logger.debug(f"file_path {file_path} => header_id {header_id}")
            return header_id

    @staticmethod
    def _scan_path(header_path):
        logger.debug(f"scan_path: header_path {header_path}")

        header_files = []
        for root, dirs, files in os.walk(header_path):
            dirs.sort()  # traverse sub directories in alphabetical order
            for file in files:
                header_file = os.path.join(root, file)
                if file.lower().endswith((".txt", ".headers")):
                    logger.debug(f"scan_path: adding '{header_file}'")
                    header_files.append(header_file)
                else:
                    logger.debug(f"scan_path: skipping '{header_file}'")
        return header_files

