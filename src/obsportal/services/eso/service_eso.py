# coding: utf-8
import signal

import arrow
import logging
import re
from pyvo.dal.tap import TAPService
from dateutil.parser import parse
from datetime import datetime

from obsportal.models.header import Header
from obsportal.models.instrument import Instrument, InstrumentMode
from obsportal.models.interferometer import Interferometer
from obsportal.models.observation import Observation
from obsportal.models.target import Target
from .factory import EsoWrapperFactory, EsoHeaderFactory, EsoFactoryError, requests_session
from obsportal.models.enums.tapfields import EnumEsoTapField
from obsportal.services.service_abstract import ServiceAbstract
from timeit import default_timer as timer

from ...models.database_tag import DatabaseTag
from ...models.utils.memcache import MemCache

regex_header_id = re.compile('^([A-Z]+)\.(\d{4})-(\d{2})-(\d{2})T\d{2}:\d{2}:\d{2}(?:\.\d*)?$')
regex_header_id_from_file = re.compile(
    '^((?:[A-Z]+)\.(?:\d{4}-\d{2}-\d{2})T\d{2}:\d{2}:\d{2}(?:\.\d*)?)(?:\.txt|log|hdr|fits)?$')
regex_date_from_header_id = re.compile('^(?:[A-Z]+)\.(\d{4}-\d{2}-\d{2})T\d{2}:\d{2}:\d{2}(?:\.\d*)?$')

logger = logging.getLogger(__name__)

def grouper(iterable, n):
    iterable = iter(iterable)
    count = 0
    group = []
    while True:
        try:
            group.append(next(iterable))
            count += 1
            if count % n == 0:
                yield group
                group = []
        except StopIteration:
            if len(group) > 0:
                yield group
            break

class ServiceEso(ServiceAbstract):

    def __init__(self, request, session_factory):
        ServiceAbstract.__init__(self, request, session_factory, "VLTI")
        self._eso = None

    # ARCHIVE QUERY ####################################################################################################

    def _run_eso_query(self, query):
        """
        Run an ADQL query on the ESO TAP service

        :param query: The query string
        :return: A table of results
        """
        # Init ESO service
        if not self._eso:
            # use requests.Session with Retry (see factory):
            self._eso = TAPService('http://archive.eso.org/tap_obs', session=requests_session)

        # Run query
        start_time = timer()

        # timeout = 600s by default (wait)
        result = self._eso.run_async(query, maxrec=15000000)

        end_time = timer()
        logger.info(f"Time (s): { end_time - start_time }")

        return result

    @staticmethod
    def _format_query_date(query_date):
        """
        Format a datetime to the accepted format of ADQL queries
        :param query_date: The datetime to format
        :return: The formatted datetime string
        :rtype: str
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query_date '{query_date}'")

        if query_date:
            # It's necessary to reset the timezone (to UTC)...
            utc_date = arrow.get(query_date).to('UTC')
            # ... and to rewrite the datetime into the only format that seems to be accepted by TAP
            formatted_date = utc_date.format('YYYY-MM-DDTHH:mm:ss')

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"formatted query_date '{formatted_date}'")
            return formatted_date

    def _query_update(self, instrument=None, use_last_mod_date=True, limit=None):
        """
        Build an ADQL query

        :param instrument: The instrument name
        :param use_last_mod_date: Flag activating the last_mod_date parameter
        :param limit: The maximum number of results
        :return: The query string
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"instrument '{instrument}', limit '{limit}'")

        # Build the TAP query
        query = f"""
                SELECT
                """

        if limit:
            query += f"""TOP {limit} """

        query += f"""dp_id, last_mod_date, {EnumEsoTapField.all_columns()}
                FROM dbo.raw
                WHERE dp_tech LIKE 'INTERFEROMETRY%'
                """

        # TODO: make instrument a required argument
        if instrument:
            query += f""" AND instrument = '{instrument}'"""

        if use_last_mod_date:
            # Get the datetime of the last updated header
            db_session = self.get_session()
            try:
                last_mod_date = Header.last_modification_date(db_session, instrument)
            except Exception as e:
                logger.warning("failure")
                raise e
            finally:
                ServiceEso.close(db_session)

            if last_mod_date:
                query += f""" AND last_mod_date >= '{self._format_query_date(last_mod_date)}' """

        #  note: lots of headers have last_mod_date=2010-11-30 09:20:26+00:00
        # to have a stable sort, sort by date_obs too

        query += """ ORDER BY last_mod_date ASC, dp_id ASC """

        logger.info(f"query: {query}")
        return query

    # SYNCHRONIZE ######################################################################################################

    def _synchronize_instrument(self, instrument, limit=None, force_update=False, debug=False):
        """
        Synchronize new/updated headers for the instrument

        :param instrument: The instrument name
        :param limit: The maximum number of results
        :param force_update: Enforce the update of existing headers
        :param debug: Activating the debug mode
        :return: None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"instrument '{instrument}', limit '{limit}', force_update '{force_update}', debug '{debug}'")


        # Always # Run the query if limit is greater than 0 :
        if True: # limit and limit > 0:
            # Get an run the query
            query = self._query_update(instrument, not force_update, limit)
            result_table = self._run_eso_query(query)
        else:
            result_table = None

        # Analyze the results
        if result_table:
            logger.info(f'{len(result_table)} headers found!')

            chunk_size = 500  # or max(100, len(result_table) / 100)

            # Save results to file (debug mode)
            if debug:
                result_table.to_table().write(f'synchronize_{instrument}_{datetime.now().isoformat()}.votable', format='votable')

            attributes = {}

            # Process chunks in same transaction
            ti = len(result_table)
            ni = 0
            for chunk in grouper(result_table, chunk_size):
                logger.info(f"Processing [{ni} / {ti}] ...")

                db_session = self.get_session()
                current_row = None
                try:
                    # Start transaction
                    ServiceEso.begin(db_session)

                    for result_row in chunk:
                        current_row = result_row
                        ni += 1

                        header_id = str(result_row['dp_id'])
                        header_mod_date = parse(str(result_row['last_mod_date']))

                        # get extra attributes:
                        attributes.clear()
                        # only parse valid date:
                        row_rel_date = str(result_row[EnumEsoTapField.RELEASE_DATE.value])
                        attributes[EnumEsoTapField.RELEASE_DATE] = parse(row_rel_date) if row_rel_date else None

                        attributes[EnumEsoTapField.PROG_ID] = str(result_row[EnumEsoTapField.PROG_ID.value])
                        attributes[EnumEsoTapField.PROG_TITLE] = str(result_row[EnumEsoTapField.PROG_TITLE.value])
                        attributes[EnumEsoTapField.PROG_TYPE] = result_row[EnumEsoTapField.PROG_TYPE.value]  # int
                        attributes[EnumEsoTapField.PI_COI] = str(result_row[EnumEsoTapField.PI_COI.value])

                        if logger.isEnabledFor(logging.DEBUG):
                            logger.debug(f"Checking header '{header_id}' (updated at '{header_mod_date}') attributes: {attributes}")

                        # Analyze the header
                        self._synchronize_header(db_session, header_id, header_mod_date, attributes, force_update)

                        # Fast signal check:
                        if self.flag_interrupted:
                            raise SystemExit

                    # Commit transaction
                    ServiceEso.commit(db_session)

                except BaseException as e:
                    ServiceEso.rollback(db_session)
                    logger.warning(f"failure on row:\n{current_row}")
                    raise e
                finally:
                    ServiceEso.close(db_session)

            logger.info(f"Processing [{ni} / {ti}] done")

        else:
            # No results
            logger.debug('no header found!')
