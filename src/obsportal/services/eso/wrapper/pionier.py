# coding: utf-8
import logging
from datetime import date
from .eso_wrapper_abstract import EsoWrapperAbstract
from .enums.keywords import EnumEsoWrapperKeywordPionier as Keyword

logger = logging.getLogger(__name__)


class EsoWrapperPionier(EsoWrapperAbstract):

    def __init__(self, header_dict, header_id=None, header_hash=None, last_updated=None):
        super(EsoWrapperPionier, self).__init__(header_dict, header_id, header_hash, last_updated, 'PIONIER')

    # VALIDATE #########################################################################################################
    def is_observation(self):
        """
        Check if the current wrapper can be used to create an observation

        :return: bool
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"is_observation: header_file {self.header_id}")

        if not super().is_observation():
            return False

        dp_type = self.dp_type

        if dp_type not in ['FRINGE,OBJECT', 'OBJECT']:
            self._context_validation_messages.append(f"Invalid 'DPR TYPE' = '{dp_type}'")
            return False

        return True

    # CUSTOM PROPERTIES ################################################################################################
    @property
    def instrument_mode_id(self):
        """
        Extract value for instrument mode (key)

        :return: str|None
        """
        spec = None
        band = 'H'
        if self.header_date < date.fromisoformat('2015-08-29'):
            spec = self.get(Keyword.INS_OPTI3_ID, default=self.get(Keyword.INS_OPTI3_NAME))
            band = self.get(Keyword.INS_OPTI2_ID, default=self.get(Keyword.INS_OPTI2_NAME))
        else:
            spec = self.get(Keyword.INS_OPTI2_ID, default=self.get(Keyword.INS_OPTI2_NAME))
            band = self.get(Keyword.INS_FILT1_NAME)
        # Fix GRISM:
        if spec == 'GRI+WOL':
            spec = 'GRISM'
        elif spec == 'WOLL':
            spec = 'FREE'
        return '%s-%s' % (spec, band)

