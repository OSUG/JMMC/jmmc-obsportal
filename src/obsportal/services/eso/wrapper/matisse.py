# coding: utf-8
import logging
from .eso_wrapper_abstract import EsoWrapperAbstract
from .enums.keywords import EnumEsoWrapperKeyword as Keyword
from .enums.keywords import EnumEsoWrapperKeywordMatisse as KeywordMatisse

logger = logging.getLogger(__name__)


class EsoWrapperMatisse(EsoWrapperAbstract):

    def __init__(self, header_dict, header_id=None, header_hash=None, last_updated=None):
        super(EsoWrapperMatisse, self).__init__(header_dict, header_id, header_hash, last_updated, 'MATISSE')

    # VALIDATE #########################################################################################################
    def is_observation(self):
        """
        Check if the current wrapper can be used to create an observation

        :return: bool
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"is_observation: header_file {self.header_id}")

        if not super().is_observation():
            return False

        dp_cat = self.dp_cat
        dp_type = self.dp_type

        if dp_cat == 'SCIENCE':
            if dp_type not in ['OBJECT']:
                self._context_validation_messages.append(f"Invalid 'DPR TYPE' = '{dp_type}' (science)")
                return False
        else:
            # CALIB:
            if dp_type not in ['STD']:
                self._context_validation_messages.append(f"Invalid 'DPR TYPE' = '{dp_type}' (calib)")
                return False

        return True

    # DATE #############################################################################################################
    @property
    def exposure_duration(self):
        """
        Extract value for exposure duration

        :return: float|None
        """
        exp_time = self.get(Keyword.EXPTIME)
        period = self.get(KeywordMatisse.DET_SEQ1_PERIOD)  # period = dit + overheads
        ndit = self.get(Keyword.DET_NDIT)
        if period is not None and ndit is not None:
            exp_time = period * ndit  # seconds
        return exp_time

    # CUSTOM PROPERTIES ################################################################################################
    @property
    def detector(self):
        """
        Extract value for instrument detector

        :return: str|None
        """
        return self.get(KeywordMatisse.DET_NAME)

    @property
    def instrument_submode(self):
        """
        Extract value for instrument submode

        :return: str|None
        """
        return self.get(KeywordMatisse.INS_MODE)

    @property
    def instrument_mode_id(self):
        """
        Extract value for instrument mode (key)

        :return: str|None
        """
        if self.detector == 'MATISSE-LM':
            fil = self.get(KeywordMatisse.INS_FIL_ID, default=self.get(KeywordMatisse.INS_FIL_NAME))
            dil = self.get(KeywordMatisse.INS_DIL_ID, default=self.get(KeywordMatisse.INS_DIL_NAME))
            # Fix MED to MEDIUM:
            if dil == 'MED':
                dil = 'MEDIUM'
            # TODO: fix 'LM_HIGH' into 'L_HIGH' or 'M_HIGH'
            mode = 'SI_PHOT_%s_%s' % (fil, dil)
        elif self.detector == 'MATISSE-N':
            din = self.get(KeywordMatisse.INS_DIN_ID, default=self.get(KeywordMatisse.INS_DIN_NAME))
            mode = 'HIGH_SENS_N_%s' % din
        else:
            mode = None
        return mode
