# coding: utf-8
import re
import logging

from .eso_wrapper_abstract import EsoWrapperAbstract
from .enums.keywords import EnumEsoWrapperKeyword as Keyword
from .enums.keywords import EnumEsoWrapperKeywordGravity as KeywordGravity

from astropy.coordinates import Angle
import astropy.units as u

logger = logging.getLogger(__name__)

regex_radec_ft = re.compile('^([\-\+]?\d{2})(\d{2})(\d{2}\.\d*)$')


class EsoWrapperGravity(EsoWrapperAbstract):

    def __init__(self, header_dict, header_id=None, header_hash=None, last_updated=None):
        super(EsoWrapperGravity, self).__init__(header_dict, header_id, header_hash, last_updated, 'GRAVITY')
        self.kw = Keyword

    # VALIDATE #########################################################################################################
    def is_observation(self):
        """
        Check if the current wrapper can be used to create an observation

        :return: bool
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"is_observation: header_file {self.header_id}")

        if not super().is_observation():
            return False

        dp_cat = self.dp_cat
        dp_type = self.dp_type

        if dp_cat == 'SCIENCE':
            if dp_type not in ['OBJECT,SINGLE', 'OBJECT,DUAL']:
                self._context_validation_messages.append(f"Invalid 'DPR TYPE' = '{dp_type}' (science)")
                return False
        else:
            # CALIB:
            if dp_type not in ['STD,SINGLE', 'STD,DUAL']:
                self._context_validation_messages.append(f"Invalid 'DPR TYPE' = '{dp_type}' (calib)")
                return False

        return True

    # DATE #############################################################################################################
    @property
    def exposure_duration(self):
        """
        Extract value for exposure duration

        :return: float|None
        """
        # HIERARCH ESO DET2 CHIP ID = 'ESO-Hawaii2RG' / Detector chip identification
        # HIERARCH ESO DET2 NAME = 'SCIENCECAM' / Name of detector system
        # HIERARCH ESO DET2 NDIT = 64 / Number of Sub-Integrations
        # HIERARCH ESO DET2 SEQ1 DIT = 1.0000000 / [s] Integration time

        # HIERARCH ESO DET3 CHIP ID = 'ESO-selex' / Detector chip identification
        # HIERARCH ESO DET3 NAME = 'FRINGETRACKER' / Name of detector system
        # HIERARCH ESO DET3 SEQ1 DIT = 0.0008500 / [s] Integration time

        exp_time = self.get(self.kw.EXPTIME)
        period = self.get(KeywordGravity.DET2_SEQ1_DIT)
        ndit = self.get(KeywordGravity.DET2_NDIT)
        if period is not None and ndit is not None:
            exp_time = period * ndit  # seconds
        return exp_time

    # CUSTOM PROPERTIES ################################################################################################
    @property
    def instrument_mode_id(self):
        """
        Extract value for instrument mode (key)

        :return: str|None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"instrument_mode: context {self._current_context_key}")

        res = self.get(KeywordGravity.INS_SPEC_RES)
        pola = self.get(KeywordGravity.INS_POLA_MODE)
        if res is not None and pola is not None:
            return '%s-%s' % (res, pola)

    @property
    def instrument_submode(self):
        """
        Extract value for instrument submode

        :return: str|None
        """

        dp_type = self.dp_type

        if dp_type.endswith('SINGLE'):
            return 'SINGLE'
        if dp_type.endswith('DUAL'):
            return 'DUAL'

        return dp_type

    # SAVE #############################################################################################################
    def save(self, db_session, header, attributes):
        """
        Save the current wrapper by creating Observation and Exposure objects

        :param db_session:
        :param header: Header
        :param attributes: other attributes (TAP ESO)
        :return: None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"save: header_file {self.header_id}")

        self.switch_context('science')
        self._save_science(db_session, header, attributes)

        if self.instrument_submode == 'DUAL':
            self.switch_context('ft')
            self._save_fringetracker(db_session, header, attributes)

    def _save_science(self, db_session, header, attributes):
        """
        Save the current wrapper by creating Observation and Exposure objects for the SCIENCE target

        :param db_session:
        :param header: Header
        :param attributes: other attributes (TAP ESO)
        :return: None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_file {self.header_id}")

        # TARGET
        target_name = self.get(KeywordGravity.INS_SOBJ_NAME, default=self.target_name)
        target = self.target(db_session, target_name=target_name)
        target = self.validate_target(target)

        # OBSERVATION
        observation = super().build_observation(db_session, attributes, target=target)
        observation = self.validate_observation(observation)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"observation {observation}")

        # EXPOSURE
        exposure = self.build_exposure(db_session, attributes, observation)
        if exposure:
            exposure = self.validate_exposure(exposure)

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"exposure {exposure}")

            header.exposures.append(exposure)

    def _save_fringetracker(self, db_session, header, attributes):
        """
        Save the current wrapper by creating Observation and Exposure objects for the FRINGE TRACKER target

        :param db_session:
        :param header: Header
        :param attributes: other attributes (TAP ESO)
        :return: None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_file {self.header_id}")

        # TARGET
        target_name = self.get(KeywordGravity.FT_ROBJ_NAME)
        target_ra = self._ft_hms_to_deg(self.get(KeywordGravity.FT_ROBJ_ALPHA), u.hour, default=KeywordGravity.FT_ROBJ_ALPHA.default)
        target_dec = self._ft_hms_to_deg(self.get(KeywordGravity.FT_ROBJ_DELTA), u.deg, default=KeywordGravity.FT_ROBJ_DELTA.default)
        target = self.target(db_session, target_name=target_name, target_ra=target_ra, target_dec=target_dec)
        target = self.validate_target(target)

        # INSTRUMENT
        instrument = self.instrument(db_session, 'GRAVITY_FT')
        instrument_mode = self.instrument_mode(db_session, 'GRAVITY_FT', 'LOW')

        # OBSERVATION
        observation = super().build_observation(db_session, attributes, target=target, instrument=instrument, instrument_mode=instrument_mode)
        observation = self.validate_observation(observation)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"observation {observation}")

        # EXPOSURE
        exposure = self.build_exposure(db_session, attributes, observation, number=2)
        if exposure:
            exposure = self.validate_exposure(exposure)

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"exposure {exposure}")

            header.exposures.append(exposure)

    @staticmethod
    def _ft_hms_to_deg(value, unit, default=None):
        """
        Convert value expressed in hms without colon into a float value

        :param value: The value to convert
        :param unit: The output unit (astropy.units)
        :return: float|None
        """
        if value is None:
            return default
        elif isinstance(value, (float, int)):
            value = str(value)

        m = regex_radec_ft.match(value)
        if m is not None:
            value_corrected = ':'.join([m.group(1), m.group(2), m.group(3)])
            angle = Angle(value_corrected, unit=unit)
            return str(angle.deg)

        return default
