# coding: utf-8
import logging
from .eso_wrapper_abstract import EsoWrapperAbstract
from .enums.keywords import EnumEsoWrapperKeywordMidi as Keyword

logger = logging.getLogger(__name__)


class EsoWrapperMidi(EsoWrapperAbstract):

    def __init__(self, header_dict, header_id=None, header_hash=None, last_updated=None):
        super(EsoWrapperMidi, self).__init__(header_dict, header_id, header_hash, last_updated, 'MIDI')

    # VALIDATE #########################################################################################################
    def is_observation(self):
        """
        Check if the current wrapper can be used to create an observation

        :return: bool
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"is_observation: header_file {self.header_id}")

        if not super().is_observation():
            return False

        dp_type = self.dp_type

        # TODO: check DPR TYPE values:
        if dp_type not in ['TRACK,OBJECT,DISPERSED,SCIPHOT', 'TRACK,OBJECT,DISPERSED', 'SEARCH,OBJECT,DISPERSED',
                           'FRINGE_SEARCH,OBJECT,FOURIER']:
            self._context_validation_messages.append(f"Invalid 'DPR TYPE' = '{dp_type}' (science)")
            return False

        return True

    # CUSTOM PROPERTIES ################################################################################################
    @property
    def instrument_mode_id(self):
        """
        Extract value for instrument mode (key)

        :return: str|None
        """
        sensitivity = self.get(Keyword.INS_OPT1_ID)
        disperser = self.get(Keyword.INS_GRIS_ID)
        if sensitivity is not None and disperser is not None:
            return '%s+%s' % (sensitivity, disperser)
        return None
