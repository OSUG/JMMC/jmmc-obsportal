# coding: utf-8
from enum import Enum


class EnumEsoWrapperField(Enum):

    def __init__(self, key, required=False):
        self.key = key
        self.required = required

    def __str__(self):
        return self.key

    def __repr__(self):
        return self.key

    DP_CAT = 'dp_cat'
    DP_TYPE = 'dp_type'
    DP_TECH = 'dp_tech'

    TARGET = 'target'  # TODO
    TARGET_NAME = 'target_name'
    TARGET_RA = 'target_ra', True
    TARGET_DEC = 'target_dec', True

    MJD_START = 'mjd_start', True
    MJD_END = 'mjd_end', True

    PROGRAM_ID = 'program_id'

    INSTRUMENT = 'instrument', True

    INSTRUMENT_MODE = 'instrument_mode', True
    INSTRUMENT_SUBMODE = 'instrument_submode'

    STATIONS = 'stations'
    PROJECTED_BASELINES = 'projected_baselines'

    TAU0 = 'tau0'
    TEMPERATURE = 'temperature'
    SEEING = 'seeing'
