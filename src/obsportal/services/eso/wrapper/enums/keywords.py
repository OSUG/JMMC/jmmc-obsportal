# coding: utf-8
from .fields import EnumEsoWrapperField as Field


class EsoWrapperKeyword(object):

    def __init__(self, key, fields=None, default=None):
        self.key = key
        self.fields = fields
        self.default = default

    def __str__(self):
        return self.key

    def __repr__(self):
        return self.key

    # TODO CHARA: fix/improve or remove next hack
    # TODO CHARA: rename method to get_key(interferometer)
    def mykey(self, interferometer):
        if "CHARA" in interferometer:
            k = self.key.replace(" ESO ", " ")
            # print(k)
            return k
        else:
            return self.key

    def __repr__(self):
        return self.key


class EnumEsoWrapperKeyword(EsoWrapperKeyword):

    ARCFILE = EsoWrapperKeyword('ARCFILE')  # FIXME
    INSTRUME = EsoWrapperKeyword('INSTRUME', Field.INSTRUMENT)

    EXPTIME = EsoWrapperKeyword('EXPTIME', Field.MJD_END)
    MJD_OBS = EsoWrapperKeyword('MJD-OBS', (Field.MJD_START, Field.MJD_END))

    RA = EsoWrapperKeyword('RA', Field.TARGET_RA, 0.0)
    DEC = EsoWrapperKeyword('DEC', Field.TARGET_DEC, 0.0)

    DPR_CATG = EsoWrapperKeyword('HIERARCH ESO DPR CATG', Field.DP_CAT)
    DPR_TECH = EsoWrapperKeyword('HIERARCH ESO DPR TECH', Field.DP_TECH)
    DPR_TYPE = EsoWrapperKeyword('HIERARCH ESO DPR TYPE', Field.DP_TYPE)

    DET_DIT = EsoWrapperKeyword('HIERARCH ESO DET DIT', Field.MJD_END)
    DET_NDIT = EsoWrapperKeyword('HIERARCH ESO DET NDIT', Field.MJD_END)

    OBS_TARG_NAME = EsoWrapperKeyword('HIERARCH ESO OBS TARG NAME', Field.TARGET_NAME)
    OBS_PROG_ID = EsoWrapperKeyword('HIERARCH ESO OBS PROG ID', Field.PROGRAM_ID)

    ISS_CONF_NTEL = EsoWrapperKeyword('HIERARCH ESO ISS CONF NTEL', (Field.STATIONS, Field.PROJECTED_BASELINES))

    @classmethod
    def ISS_CONF_STATION(cls, i):
        return EsoWrapperKeyword('HIERARCH ESO ISS CONF STATION%i' % i, Field.PROJECTED_BASELINES)

    @classmethod
    def ISS_PBL_START(cls, i, j):
        return EsoWrapperKeyword('HIERARCH ESO ISS PBL%i%i START' % (i, j), Field.PROJECTED_BASELINES)

    @classmethod
    def ISS_PBL_END(cls, i, j):
        return EsoWrapperKeyword('HIERARCH ESO ISS PBL%i%i END' % (i, j), Field.PROJECTED_BASELINES)

    @classmethod
    def ISS_PBLA_START(cls, i, j):
        return EsoWrapperKeyword('HIERARCH ESO ISS PBLA%i%i START' % (i, j), Field.PROJECTED_BASELINES)

    @classmethod
    def ISS_PBLA_END(cls, i, j):
        return EsoWrapperKeyword('HIERARCH ESO ISS PBLA%i%i END' % (i, j), Field.PROJECTED_BASELINES)

    ISS_AMBI_TAU0_START = EsoWrapperKeyword('HIERARCH ESO ISS AMBI TAU0 START', Field.TAU0)
    ISS_AMBI_TAU0_END = EsoWrapperKeyword('HIERARCH ESO ISS AMBI TAU0 END', Field.TAU0)

    ISS_AMBI_TEMP = EsoWrapperKeyword('HIERARCH ESO ISS AMBI TEMP', Field.TEMPERATURE)

    ISS_AMBI_FWHM_START = EsoWrapperKeyword('HIERARCH ESO ISS AMBI FWHM START', Field.SEEING)
    ISS_AMBI_FWHM_END = EsoWrapperKeyword('HIERARCH ESO ISS AMBI FWHM END', Field.SEEING)


class EnumEsoWrapperKeywordAmber(EsoWrapperKeyword):
    OCS_OBS_SPECCONF = EsoWrapperKeyword('HIERARCH ESO OCS OBS SPECCONF', Field.INSTRUMENT_MODE)


class EnumEsoWrapperKeywordGravity(EsoWrapperKeyword):
    DET2_SEQ1_DIT = EsoWrapperKeyword('HIERARCH ESO DET2 SEQ1 DIT', Field.MJD_END)
    DET2_NDIT = EsoWrapperKeyword('HIERARCH ESO DET2 NDIT', Field.MJD_END)
    INS_SPEC_RES = EsoWrapperKeyword('HIERARCH ESO INS SPEC RES', Field.INSTRUMENT_MODE)
    INS_POLA_MODE = EsoWrapperKeyword('HIERARCH ESO INS POLA MODE', Field.INSTRUMENT_MODE)
    INS_SOBJ_NAME = EsoWrapperKeyword('HIERARCH ESO INS SOBJ NAME', Field.TARGET_NAME)
    FT_ROBJ_NAME = EsoWrapperKeyword('HIERARCH ESO FT ROBJ NAME', Field.TARGET_NAME)
    FT_ROBJ_ALPHA = EsoWrapperKeyword('HIERARCH ESO FT ROBJ ALPHA', Field.TARGET_RA, 0.0)
    FT_ROBJ_DELTA = EsoWrapperKeyword('HIERARCH ESO FT ROBJ DELTA', Field.TARGET_DEC, 0.0)


class EnumEsoWrapperKeywordMatisse(EsoWrapperKeyword):
    DET_SEQ1_PERIOD = EsoWrapperKeyword('HIERARCH ESO DET SEQ1 PERIOD', Field.MJD_END)
    DET_NAME = EsoWrapperKeyword('HIERARCH ESO DET NAME', Field.INSTRUMENT_MODE)
    INS_MODE = EsoWrapperKeyword('HIERARCH ESO INS MODE', Field.INSTRUMENT_SUBMODE)
    INS_FIL_ID = EsoWrapperKeyword('HIERARCH ESO INS FIL ID', Field.INSTRUMENT_MODE)
    INS_FIL_NAME = EsoWrapperKeyword('HIERARCH ESO INS FIL NAME', Field.INSTRUMENT_MODE)
    INS_DIL_ID = EsoWrapperKeyword('HIERARCH ESO INS DIL ID', Field.INSTRUMENT_MODE)
    INS_DIL_NAME = EsoWrapperKeyword('HIERARCH ESO INS DIL NAME', Field.INSTRUMENT_MODE)
    INS_DIN_ID = EsoWrapperKeyword('HIERARCH ESO INS DIN ID', Field.INSTRUMENT_MODE)
    INS_DIN_NAME = EsoWrapperKeyword('HIERARCH ESO INS DIN NAME', Field.INSTRUMENT_MODE)

class EnumEsoWrapperKeywordMidi(EsoWrapperKeyword):
    INS_OPT1_ID = EsoWrapperKeyword('HIERARCH ESO INS OPT1 ID', Field.INSTRUMENT_MODE)
    INS_GRIS_ID = EsoWrapperKeyword('HIERARCH ESO INS GRIS ID', Field.INSTRUMENT_MODE)


class EnumEsoWrapperKeywordPionier(EsoWrapperKeyword):
    INS_OPTI2_ID = EsoWrapperKeyword('HIERARCH ESO INS OPTI2 ID', Field.INSTRUMENT_MODE)
    INS_OPTI2_NAME = EsoWrapperKeyword('HIERARCH ESO INS OPTI2 NAME', Field.INSTRUMENT_MODE)
    INS_OPTI3_ID = EsoWrapperKeyword('HIERARCH ESO INS OPTI3 ID', Field.INSTRUMENT_MODE)
    INS_OPTI3_NAME = EsoWrapperKeyword('HIERARCH ESO INS OPTI3 NAME', Field.INSTRUMENT_MODE)
    INS_FILT1_NAME = EsoWrapperKeyword('HIERARCH ESO INS FILT1 NAME', Field.INSTRUMENT_MODE)


class EnumEsoWrapperKeywordVinci(EsoWrapperKeyword):
    OCS_OBS_SPECCONF = EsoWrapperKeyword('HIERARCH ESO OCS OBS SPECCONF', Field.INSTRUMENT_MODE)


# TODO move code up and out of eso module package (same for eso content ?)
class EnumCharaWrapperKeyword(EnumEsoWrapperKeyword):
    OBS_PROG_ID = EsoWrapperKeyword('HIERARCH OBS PROG ID', Field.PROGRAM_ID, "ENG")

    OCS_OBS_SPECCONF = EsoWrapperKeyword('HIERARCH OBS SPECCONF', Field.INSTRUMENT_MODE)

    ISS_CONF_NTEL = EsoWrapperKeyword('HIERARCH CHARA NB TEL', (Field.STATIONS, Field.PROJECTED_BASELINES))

    @classmethod
    def ISS_CONF_STATION(cls, i):
        return EsoWrapperKeyword('HIERARCH CHARA T%iNAME' % i, Field.PROJECTED_BASELINES)

    BASELINE = EsoWrapperKeyword('HIERARCH CHARA BASELINE', Field.STATIONS)


    # prefere overiding so it does not report bad keywords
    # We should inherith from a common Keyword between CHARA and ESO
    RA = EsoWrapperKeyword('HIERARCH OBS RA', Field.TARGET_RA, 0.0)
    DEC = EsoWrapperKeyword('HIERARCH OBS DEC', Field.TARGET_DEC, 0.0)

    EXPTIME = EsoWrapperKeyword('HIERARCH DET EXPTIME', Field.MJD_END)

    TAU0 = EsoWrapperKeyword('HIERARCH CHARA TAU0', Field.TAU0)
    SEEING = EsoWrapperKeyword('HIERARCH CHARA SEEING', Field.SEEING)

    INS_RESOL = EsoWrapperKeyword('HIERARCH INS RESOL', Field.INSTRUMENT_MODE)
    INS_CENTRAL_WL = EsoWrapperKeyword('HIERARCH INS CENTRAL WL', Field.INSTRUMENT_MODE)


