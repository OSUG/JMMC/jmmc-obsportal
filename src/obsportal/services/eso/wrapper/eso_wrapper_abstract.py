# coding: utf-8
import logging
import os
import re
from sqlalchemy_utc import utc
from datetime import date, datetime
from dateutil.parser import parse
from obsportal.models.enums.observation import EnumObservationCategory
from obsportal.models.instrument import Instrument, InstrumentMode
from obsportal.models.observation import Observation
from obsportal.models.header import EsoHeader
from obsportal.models.target import Target
from obsportal.models.interferometer import Interferometer
from obsportal.models.exposure import Exposure
from .enums.fields import EnumEsoWrapperField as Field
from .enums.keywords import EnumEsoWrapperKeyword as Keyword
from .enums.keywords import EsoWrapperKeyword
from obsportal.models.enums.tapfields import EnumEsoTapField

regex_header_id_from_arcfile = re.compile(
    '^((?:[A-Z]+)\.(?:\d{4}-\d{2}-\d{2})T\d{2}:\d{2}:\d{2}(?:\.\d*)?)(?:\.fits)?$')
regex_date_from_header_id = re.compile('^(?:[A-Z]+)\.(\d{4}-\d{2}-\d{2})T(\d{2}:\d{2}:\d{2})(?:\.\d*)?$')

logger = logging.getLogger(__name__)


class EsoWrapperAbstract(object):

    def __init__(self, header_dict, header_id=None, header_hash=None, last_updated=None, instrument_id=None, context=None):
        self._header_dict = header_dict
        self._header_hash = header_hash
        self._instrument_id = instrument_id
        self._contexts = {}
        self._current_context_key = context or 'main'

        if header_id:
            self._header_id = header_id
        elif 'ARCFILE' in header_dict:
            m = regex_header_id_from_arcfile.match(header_dict.get('ARCFILE'))
            if m is not None:
                self._header_id = m.group(1)

        if not self._header_id:
            raise Exception('Header ID is not defined and cannot be retrieved from content!')

        if last_updated:
            if isinstance(last_updated, str):
                self._header_last_updated = parse(last_updated)
            else:
                self._header_last_updated = last_updated

            if self._header_last_updated.tzinfo is None:
                self._header_last_updated = self._header_last_updated.replace(tzinfo=utc)
        else:
            self._header_last_updated=None

        # set default value. May be given as a new constructor param by factory...
        # don't forget to overwrite on CHARA side
        self.interferometer_id="VLTI"

    # CONTEXT MANAGEMENT ###############################################################################################

    def switch_context(self, key):
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f'switch_context: key: {key}')
        self._current_context_key = key

    @property
    def _current_context(self):
        if self._current_context_key not in self._contexts:
            self._contexts[self._current_context_key] = {
                'used_keywords': [],
                'invalid_fields': {},
                'validation_messages': [],
            }
        return self._contexts[self._current_context_key]

    @_current_context.setter
    def _current_context(self, value):
        self._contexts[self._current_context_key] = value

    @property
    def _context_invalid_fields(self):
        return self._current_context['invalid_fields']

    @_context_invalid_fields.setter
    def _context_invalid_fields(self, value):
        self._current_context['invalid_fields'] = value

    @property
    def _context_used_keywords(self):
        return self._current_context['used_keywords']

    @_context_used_keywords.setter
    def _context_used_keywords(self, value):
        self._current_context['used_keywords'] = value

    @property
    def _context_validation_messages(self):
        return self._current_context['validation_messages']

    @_context_validation_messages.setter
    def _context_validation_messages(self, value):
        self._current_context['validation_messages'] = value

    @property
    def _all_used_keywords(self):
        used_keywords = []
        for ctx in self._contexts.values():
            for kw in ctx.get('used_keywords', []):
                if kw not in used_keywords:
                    used_keywords.append(kw)
        return used_keywords

    @property
    def _all_validation_messages(self):
        validation_messages = []
        for ctx in self._contexts.values():
            for msg in ctx.get('validation_messages', []):
                if msg not in validation_messages:
                    validation_messages.append(msg)
        return validation_messages

    @property
    def _all_invalid_fields(self):
        invalid_fields = {}
        for ctx in self._contexts.values():
            for f_k, f_v in ctx.get('invalid_fields', {}):
                if f_k not in invalid_fields:
                    invalid_fields[f_k] = f_v
        return invalid_fields

    @property
    def _merged_invalid_fields(self):
        if self._current_context_key == 'main':
            return self._context_invalid_fields
        else:
            invalid_fields = {}
            invalid_fields.update(self._contexts.get('main', {}).get('invalid_fields', {}))
            invalid_fields.update(self._context_invalid_fields)
            return invalid_fields

    # KEYWORD ACCESS ###################################################################################################
    def getFloat(self, kw, default=None, format=None, fields=None):
        """
        Extract a float typed value from the header content for the requested key

        :param kw: The requested keyword (str|EnumEsoWrapperKeyword)
        :param default: The default value
        :param format: The output format mask to apply on value
        :param fields: The field(s) to mark as invalid
        :return:
        """
        value = self.get(kw,default, format, fields)
        if value:
            return float(value)
        if default:
            return default


    def get(self, kw, default=None, format=None, fields=None):
        """
        Extract a value from the header content for the requested key

        :param kw: The requested keyword (str|EnumEsoWrapperKeyword)
        :param default: The default value unless kw has a default value
        :param format: The output format mask to apply on value
        :param fields: The field(s) to mark as invalid
        :return:
        """

        # Set default properties
        message = None
        key = kw
        value = default

        if kw is None:
            # TODO raise exception
            return value
        elif isinstance(kw, EsoWrapperKeyword):
            # special hack to be compatible with some ESO HIERARCH kw
            key = kw.mykey(self.interferometer_id)
            fields = fields or kw.fields
            default = default or kw.default
            value = default

        # Get the value
        if key not in self._header_dict:
            message = f"Missing '{key}'"
        else:
            value = self._header_dict.get(key)
            if value in (None, '', ' '):
                message = f"No value for '{key}'"
                value = default
            elif value == 0.0:
                # FIXME: Quick and dirty RA/DEC check
                message = f"Invalid value for '{key}' : '{self._header_dict.get(key)}'"

        # Mark as used keyword
        if key not in self._context_used_keywords:
            self._context_used_keywords.append(key)

            if message and message not in self._context_validation_messages:
                self._context_validation_messages.append(message)

                # Mark as invalid fields
                if isinstance(fields, (list, tuple)):
                    for field in fields:
                        self._mark_invalid_field(field)
                else:
                    self._mark_invalid_field(fields)

        # Format the value
        if format and value is not None:
            value = format % value

        logger.debug(f"get({key}) = {value} (kw({type(kw)}) = {kw}, header_value = {self._header_dict.get(key)}, default = {default})")
        return value

    def _mark_invalid_field(self, field):
        """
        Mark a field as invalid

        :param field: str|EnumEsoWrapperField
        :return: None
        """
        if isinstance(field, Field):
            f = field.name
        elif isinstance(field, str):
            f = field
        else:
            f = None

        if f and f not in self._context_invalid_fields:
            self._context_invalid_fields[f] = field

    # HEADER PROPERTIES ################################################################################################
    @property
    def header_id(self):
        return self._header_id

    @property
    def header_datetime_updated(self):
        if self._header_last_updated:
            return self._header_last_updated
        else:
            return self.header_datetime

    @property
    def header_datetime(self):
        m = regex_date_from_header_id.match(self.header_id)
        if m is not None:
            iso = m.group(1) + 'T' + m.group(2) + '+00:00'
            return datetime.fromisoformat(iso)

        date_obs=""
        # fall back to date-obs
        if "DATE-OBS" in self._header_dict:
            date_obs=self._header_dict["DATE-OBS"] + '+00:00'
            return datetime.fromisoformat(date_obs)

        raise Exception(f'Header datetime cannot be extracted from ID({self.header_id}) nor DATE-OBS({date_obs})!')

    @property
    def header_date(self):
        m = regex_date_from_header_id.match(self.header_id)
        if m is not None:
            return date.fromisoformat(m.group(1))

        date_obs=""
        # fall back to date-obs
        if "DATE-OBS" in self._header_dict:
            date_obs=self._header_dict["DATE-OBS"][:10]
            return date.fromisoformat(date_obs)

        raise Exception(f'Header date cannot be extracted from ID({self.header_id}) nor DATE-OBS({date_obs})!')

    @property
    def header_relative_path(self):
        d = self.header_date
        return os.path.join(str(d.year), f"{d.month:02d}", f"{d.day:02d}")

    @property
    def header_filename(self):
        return '%s.txt' % self.header_id

    # ESO DPR keywords #################################################################################################

    @property
    def dp_cat(self):
        """
        Extract value for DPR CATG

        :return: str|None
        """
        return self.get(Keyword.DPR_CATG)

    @property
    def dp_type(self):
        """
        Extract value for DPR TYPE

        :return: str|None
        """
        return self.get(Keyword.DPR_TYPE)

    @property
    def category(self):
        """
        Extract category

        :return: EnumObservationCategory|None
        """
        header_category = self.dp_cat
        if header_category == 'SCIENCE':
            return EnumObservationCategory.science
        elif header_category == 'CALIB':
            return EnumObservationCategory.calibrator

    # TARGET ###########################################################################################################
    def target(self, db_session, target_name=None, target_ra=None, target_dec=None):
        """
        Get the target object

        :param db_session:
        :param target_name:
        :param target_ra:
        :param target_dec:
        :return: Target|None
        """
        target_name = target_name or self.target_name
        target_ra = target_ra or self.target_ra
        target_dec = target_dec or self.target_dec

        target = Target.factory(db_session, target_name, target_ra, target_dec)
        if target is None:
            self._context_validation_messages.append('Target not found!')
            self._mark_invalid_field(Field.TARGET)

        return target

    @property
    def target_name(self):
        """
        Extract value for target name

        :return: str|None
        """
        return self.get(Keyword.OBS_TARG_NAME)

    @property
    def target_ra(self):
        """
        Extract value for target RA

        :return: float
        """
        return self.get(Keyword.RA)  # use 0.0 to report invalid coordinates

    @property
    def target_dec(self):
        """
        Extract value for target DEC

        :return: float
        """
        return self.get(Keyword.DEC)  # use 0.0 to report invalid coordinates

    # DATE #############################################################################################################
    @property
    def exposure_duration(self):
        """
        Extract value for exposure duration

        :return: float|None
        """
        dit = self.get(Keyword.DET_DIT)
        ndit = self.get(Keyword.DET_NDIT)
        if dit is not None and ndit is not None:
            return dit * ndit  # seconds
        return self.get(Keyword.EXPTIME)


    @property
    def mjd_start(self):
        """
        Extract value for MJD Start

        :return: float|None
        """
        return self.getFloat(Keyword.MJD_OBS)

    @property
    def mjd_end(self):
        """
        Extract value for MJD End

        :return: float|None
        """
        if self.mjd_start is not None and self.exposure_duration is not None:
            return self.mjd_start + self.exposure_duration / 86400.0  # s to days
        return None

    # INSTRUMENT #######################################################################################################
    @property
    def program_id(self):
        """
        Extract value for Program ID

        :return: str|None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"Keyword.OBS_PROG_ID {Keyword.OBS_PROG_ID} of type '{type(Keyword.OBS_PROG_ID)}'")
        return self.get(Keyword.OBS_PROG_ID)

    def instrument(self, db_session, instrument_id=None):
        """
        Get the instrument object

        :param db_session:
        :param instrument_id:
        :return: Instrument|None
        """
        instrument_id = instrument_id or self.instrument_id

        instrument = Instrument.get(db_session, instrument_id)
        if instrument is None:
            self._context_validation_messages.append(f"Invalid instrument_id = '{instrument_id}'")
            self._mark_invalid_field(Field.INSTRUMENT)

        return instrument

    @property
    def instrument_id(self):
        """
        Extract value for instrument ID

        :return: str|None
        """
        if self._instrument_id:
            return self._instrument_id
        else:
            return self.get(Keyword.INSTRUME)

    def instrument_mode(self, db_session, instrument_id=None, instrument_mode_id=None):
        """
        Get the instrument mode object

        :param db_session:
        :param instrument_id:
        :param instrument_mode_id:
        :return: InstrumentMode|None
        """
        instrument_id = instrument_id or self.instrument_id
        instrument_mode_id = instrument_mode_id or self.instrument_mode_id

        if instrument_mode_id:
            instrument_mode = InstrumentMode.find(db_session, instrument_id, instrument_mode_id)
            if instrument_mode is None:
                self._context_validation_messages.append(f"Invalid instrument_mode = '{instrument_mode_id}' for instrument '{instrument_id}'")
                self._mark_invalid_field(Field.INSTRUMENT_MODE)

            return instrument_mode
        else:
            self._context_validation_messages.append(f"No instrument_mode for instrument '{instrument_id}'")
            self._mark_invalid_field(Field.INSTRUMENT_MODE)

    @property
    def instrument_mode_id(self):
        """
        Extract value for instrument mode (key)
        This method must be specialized in subclasses

        :return: None
        """
        return None

    @property
    def instrument_submode(self):
        """
        Extract value for instrument submode
        This method must be specialized in subclasses

        :return: None
        """
        return None

    @property
    def stations(self):
        """
        Extract stations' list as string

        :return: str|None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_file {self.header_id}")

        stations = self._stations_as_dict
        if stations:
            return ' '.join(stations.values())

    @property
    def _stations_as_dict(self):
        """
        Extract stations' list as dictionary

        :return: dict
        """
        stations = {}

        stations_number = self.get(Keyword.ISS_CONF_NTEL)
        if stations_number is not None:
            for i in range(1, stations_number + 1):
                stations[i] = self.get(Keyword.ISS_CONF_STATION(i), default='')

        return stations

    @property
    def projected_baselines(self):
        """
        Extract projected baselines' list as dictionary

        :return: dict
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_file {self.header_id}")

        projected_baselines = {}

        stations = self._stations_as_dict
        stations_number = self.get(Keyword.ISS_CONF_NTEL)
        if stations_number is not None and stations is not None:

            for i in range(1, stations_number):
                for j in range(i + 1, stations_number + 1):
                    projected_baselines['%s-%s' % (stations[i], stations[j])] = {
                        'length': {
                            'start': self.get(Keyword.ISS_PBL_START(i, j), format='%.4f'),
                            'end': self.get(Keyword.ISS_PBL_END(i, j), format='%.4f'),
                        },
                        'angle': {
                            'start': self.get(Keyword.ISS_PBLA_START(i, j), format='%.4f'),
                            'end': self.get(Keyword.ISS_PBLA_END(i, j), format='%.4f'),
                        }
                    }
        return projected_baselines

    # WEATHER CONDITIONS ###############################################################################################
    @property
    def tau0(self):
        tau0_start = self.get(Keyword.ISS_AMBI_TAU0_START)
        tau0_end = self.get(Keyword.ISS_AMBI_TAU0_END)

        mean_tau = None
        sum_tau = 0.0
        n = 0
        if tau0_start is not None:
            sum_tau += float(tau0_start)
            n = n + 1
        if tau0_end is not None:
            sum_tau += float(tau0_end)
            n = n + 1
        if n > 0:
            mean_tau = sum_tau / n

        return mean_tau

    @property
    def temperature(self):
        return self.get(Keyword.ISS_AMBI_TEMP)

    @property
    def seeing(self):
        seeing_start = self.get(Keyword.ISS_AMBI_FWHM_START)
        seeing_end = self.get(Keyword.ISS_AMBI_FWHM_END)

        mean_seeing = None
        sum_seeing = 0.0
        n = 0
        if seeing_start is not None:
            sum_seeing += float(seeing_start)
            n = n + 1
        if seeing_end is not None:
            sum_seeing += float(seeing_end)
            n = n + 1
        if n > 0:
            mean_seeing = sum_seeing / n

        return mean_seeing

    # EXTRACTED CONTENT ################################################################################################
    @property
    def extracted_content(self):
        """
        Get a dictionary of all used keywords and their values

        :return: dict
        """
        return {
            **{str(keyword): self._header_dict.get(str(keyword)) for keyword in self._all_used_keywords},
        }

    # CHECK ############################################################################################################
    def is_observation(self):
        """
        Check if the current wrapper can be used to create an observation

        :return: bool
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_file {self.header_id}")

        # General rules:
        tech = self.get(Keyword.DPR_TECH, '')
        if not tech.startswith('INTERFEROMETRY'):
            self._context_validation_messages.append(f"Invalid '{Keyword.DPR_TECH}' = '{tech}'")
            return False

        dp_cat = self.dp_cat
        if dp_cat not in ['SCIENCE', 'CALIB']:
            self._context_validation_messages.append(f"Invalid '{Keyword.DPR_CATG}' = '{dp_cat}'")
            return False

        return True

    # SAVE #############################################################################################################
    def save(self, db_session, header, attributes):
        """
        Save the current wrapper by creating Observation and Exposure objects

        :param db_session:
        :param header: Header
        :param attributes: other attributes (TAP ESO)
        :return: None
        """

        # Observation
        observation = self.build_observation(db_session, attributes)
        observation = self.validate_observation(observation)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"observation {observation}")

        # Exposure
        exposure = self.build_exposure(db_session, attributes, observation)
        if exposure:
            exposure = self.validate_exposure(exposure)

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"exposure {exposure}")

            header.exposures.append(exposure)

    def get_new_header(self):
        return EsoHeader()

    def build_header(self, date_updated=None, header=None):
        """
        Build a EsoHeader object from the current wrapper

        :return: EsoHeader
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_id {self.header_id}")
#            logger.debug(f"_header_dict: {self._header_dict}")


        # Update or Create
        header = header or self.get_new_header()

        # Clean existing properties before update
        header.validation_log = None
        header.extracted_content = None
        header.exposures = []

        # HEADER #######################################################################################################
        header.id = self.header_id
        header.date_updated = date_updated or self.header_datetime_updated

        header.program_id = self.program_id

        # ESO DPR keywords #############################################################################################
        header.dp_cat = self.dp_cat
        header.dp_type = self.dp_type

        # INTERFEROMETER ###############################################################################################
        header.interferometer_id = self.interferometer_id

        # INSTRUMENT ###################################################################################################
        header.instrument_id = self.instrument_id

        # ACQUISITION ##################################################################################################
        header.mjd_start = self.mjd_start

        # CONTENT ######################################################################################################
        header.file_path = os.path.join(self.header_relative_path, self.header_filename)
        header.file_hash = self._header_hash

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header:{header}")
        return header

    def build_observation(self, db_session, attributes, target=None, instrument=None, instrument_mode=None):
        """
        Build an Observation object from the current wrapper

        :param db_session:
        :param attributes: other attributes (TAP ESO)
        :return: Observation
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_file {self.header_id}")

        # Get linked objects
        if not target:
            target = self.target(db_session)
            target = self.validate_target(target)

        interferometer = Interferometer.get(db_session, self.interferometer_id)

        instrument = instrument or self.instrument(db_session)
        instrument_mode = instrument_mode or self.instrument_mode(db_session)

        # Get existing (or create) observation object
        observation = Observation.factory(db_session, attributes,
                                          interferometer=interferometer,
                                          instrument=instrument,
                                          instrument_mode=instrument_mode,
                                          instrument_submode=self.instrument_submode,
                                          category=self.category,
                                          program_id=self.program_id,
                                          target=target,
                                          stations=self.stations)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"build_observation: observation:{observation}")
        return observation

    def build_exposure(self, db_session, attributes, observation, number=1):
        """
        Build an Exposure object from the current wrapper

        :param db_session: The database session
        :param attributes: other attributes (TAP ESO)
        :param observation: Observation
        :param number: int
        :return: Exposure
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_file {self.header_id} number {number}")

        # Build exposure ID
        exposure_id = f"{self.header_id}_{number}"

        # Update or Create
        exposure = Exposure.get(db_session, exposure_id) or Exposure()

        # Set properties
        exposure.id = exposure_id

        # HEADER #######################################################################################################
        exposure.header_id = self.header_id
        exposure.date_updated = self.header_datetime_updated

        # OBSERVATION ##################################################################################################
        exposure.observation = observation

        # ACQUISITION ##################################################################################################

        # Time ranges:
        exposure.mjd_start = self.mjd_start
        exposure.mjd_end = self.mjd_end

        # Baselines:
        exposure.projected_baselines = self.projected_baselines

        # WEATHER CONDITIONS ###########################################################################################
        exposure.tau0 = self.tau0
        exposure.temperature = self.temperature
        exposure.seeing = self.seeing

        # Extra ########################################################################################################
        exposure.date_release = get_attribute(attributes,EnumEsoTapField.RELEASE_DATE)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"exposure:{exposure}")
        return exposure

    def validate_exposure(self, exposure):
        """
        Validate the exposure object

        :param exposure: Exposure
        :return: Exposure
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"exposure {exposure} context {self._current_context_key}")

        exposure.validation_level = 1

        for key, field in self._merged_invalid_fields.items():
            exposure.add_invalid_field(key)

            if isinstance(field, Field) and field.required:
                exposure.validation_level = 0

        return exposure

    def validate_observation(self, observation):
        """
        Validate the observation object

        :param observation: Observation
        :return: Observation
        """
        # TODO
        return observation

    def validate_target(self, target):
        """
        Validate the target object

        :param target: Target
        :return: Target
        """
        # TODO
        return target


# TODO CHARA: move me in a helper zone
def get_attribute(attributes, key, default_value=None):
    if key in attributes.keys():
        return attributes[key]
    return default_value
