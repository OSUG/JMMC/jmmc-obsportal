# coding: utf-8
import hashlib
import logging
import os
import re
import time
from datetime import date

import arrow
from bs4 import BeautifulSoup
import requests
from requests.adapters import HTTPAdapter

from urllib3.util.retry import Retry

from obsportal.models.header import Header
from obsportal.tools.misc import is_integer, is_number

from .wrapper.amber import EsoWrapperAmber
from .wrapper.gravity import EsoWrapperGravity
from .wrapper.matisse import EsoWrapperMatisse
from .wrapper.midi import EsoWrapperMidi
from .wrapper.pionier import EsoWrapperPionier
from .wrapper.vinci import EsoWrapperVinci

regex_date_from_header_id = re.compile('^(?:[A-Z]+)\.(\d{4}-\d{2}-\d{2})T\d{2}:\d{2}:\d{2}(?:\.\d*)?$')

# regex_instrument_from_header_id = re.compile('^([A-Z]+)\.\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d*)?$')
# old regex was too complicated here is the new one
regex_instrument_from_header_id = re.compile('(.*?)(\.|_)') # part before first . or _

logger = logging.getLogger(__name__)


#  Retry Handling for requests
# See https://www.peterbe.com/plog/best-practice-with-retries-with-requests
# Or https://findwork.dev/blog/advanced-usage-python-requests-timeouts-retries-hooks/
def requests_retry_session(
        retries=3,
        backoff_factor=1.0,
        status_forcelist=(500, 502, 504)):

    adapter = HTTPAdapter()
    adapter.max_retries = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
        respect_retry_after_header=False
    )
    session = requests.Session()
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


requests_session = requests_retry_session()


def requests_get(url, b_keyword_check):
    timeout = 5  # 5s first, double at each retry
    max_retry = 3
    retry = 0

    while True:
        try:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"url: '{url}'")

            # use session to benefit from connection pool + retries:
            # timeouts = (conn_timeout, read_timeout)
            result = requests_session.get(url, timeout=(10, timeout))

            # check http status code:
            if result.status_code / 100 != 2:
                logger.info("Http status is incorrect: {result.status_code}")

            # validate result is correct ie match keyword_check in response:
            content = result.content
            if content and content.find(b_keyword_check) == -1:
                logger.info(f"Bad result (missing '{b_keyword_check}'):\n{content}")
            else:
                return content

        except requests.exceptions.ConnectionError as ce:
            logger.debug(ce)

        retry += 1
        if retry >= max_retry:
            raise ValueError(f"Bad http response for {url}")
        # Retry http query:
        timeout *= 2
        logger.info(f"Http connection error: retry ({retry} / {max_retry}) timeout = {timeout}s")
        # wait 3s between retries:
        time.sleep(3)


class EsoWrapperFactory(object):

    def __init__(self, settings):
        self.settings = settings

    def build_cache_path(self, header_id, make_dirs=False):
        """
        Build the path of the cache file associated to an header ID

        :param header_id: The header ID
        :param make_dirs: Flag requesting the creation of directories
        :return: string|None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_id '{header_id}'")

        # Extract the date from the header ID
        m = regex_date_from_header_id.match(header_id)
        if m is not None:
            d = date.fromisoformat(m.group(1))

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"extracted date '{d}'")

            cache_relative_path = os.path.join(str(d.year), f"{d.month:02d}", f"{d.day:02d}")
            cache_full_path = os.path.join(self.settings.get('obsportal.paths.data'), 'eso', cache_relative_path)

            if make_dirs:
                os.makedirs(cache_full_path, exist_ok=True)

            cache_filename = '%s.txt' % header_id
            cache_file_path = os.path.join(cache_full_path, cache_filename)

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"cache_file_path '{cache_file_path}'")
            return cache_file_path

        else:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"Unable to extract a date from header_id '{header_id}'")
            return None

    @staticmethod
    def from_dict(header_dict, header_id=None, header_hash=None, date_updated=None):
        """
        Load an ESO Wrapper from a dictionary

        :param header_dict: The content dictionary
        :param header_id: The header ID
        :param header_hash: The hash of the content string
        :param date_updated: The last modification date of the header
        :return: EsoWrapper|None
        """

        # Try to find instrument name inside the header
        instrument = header_dict.get('INSTRUME')

        # Else, try to extract instrument name from header ID
        if instrument is None and header_id is not None:
            m = regex_instrument_from_header_id.match(header_id)
            if m is not None:
                instrument = m.group(1)

        # Finally, init a wrapper object regarding to instrument name
        if instrument is None or instrument == '':
            raise EsoWrapperFactoryError("The instrument cannot be found in the header!")
        elif instrument.startswith('VIN'):
            return EsoWrapperVinci(header_dict, header_id, header_hash, date_updated)
        elif instrument.startswith('AMB'):
            return EsoWrapperAmber(header_dict, header_id, header_hash, date_updated)
        elif instrument.startswith('MID'):
            return EsoWrapperMidi(header_dict, header_id, header_hash, date_updated)
        elif instrument.startswith('PIO'):
            return EsoWrapperPionier(header_dict, header_id, header_hash, date_updated)
        elif instrument.startswith('GRA'):
            return EsoWrapperGravity(header_dict, header_id, header_hash, date_updated)
        elif instrument.startswith('MAT'):
            return EsoWrapperMatisse(header_dict, header_id, header_hash, date_updated)
        elif instrument.startswith('SPICA'):
            # note: import done below to avoid cyclic dependencies
            from obsportal.services.chara.wrapper.spica import CharaWrapperSpica
            return CharaWrapperSpica(header_dict, header_id, header_hash, date_updated)
        else:
            raise EsoWrapperFactoryError("The instrument '%s' is unknown!" % instrument)

    def from_string(self, header_string, header_id=None, date_updated=None):
        """
        Load an ESO Wrapper from a string

        :param header_string: The string content of the header
        :param header_id: The header ID
        :param date_updated: The last modification date of the header
        :return: EsoWrapper|None
        """

        # Build the hash of the content
        header_hash = hashlib.sha1(header_string.encode()).hexdigest()

        # Extract the essential content from the string
        # See: https://raw.githubusercontent.com/astropy/astroquery/master/astroquery/eso/core.py
        header_dict = {}
        for key_value in header_string.split('\n'):
            if "=" in key_value:
                key, value = key_value.split('=', 1)
                if key[0:7] != "COMMENT":  # drop comments
                    key = key.strip()
                    value = value.strip()
                    # Search for string keyword removing quotation marks & stripping value
                    if value[0] == "'":
                        # Search for next single-quote character
                        searchingEndQuote=True
                        nextquotePos=1
                        try:
                            while searchingEndQuote:
                                nextquotePos=value.index("'",nextquotePos)
                                # Ignore and search after two successive quotes
                                if value[nextquotePos+1:nextquotePos+2]=="'":
                                    nextquotePos+=2
                                else:
                                    searchingEndQuote=False
                        except:
                            logger.error(f"error reading key wrong number of quotes ?")
                            pass
                        value = value[1:nextquotePos].strip()
                    else:
                        # Remove comment part and strip value
                        value = value.split('/', 1)[0].strip()

                        # Convert boolean T to True
                        if value == "T":
                            value = True

                        # Convert boolean F to False
                        elif value == "F":
                            value = False

                        # Convert to float
                        elif "." in value and is_number(value):
                            value = float(value)

                        # Convert to integer
                        elif is_integer(value):
                            value = int(value)

                    header_dict[key] = value
                    logger.debug(f"{header_id}.header[{key}]='{value}'")
            elif key_value.startswith("END"):
                break

        return self.from_dict(header_dict, header_id, header_hash, date_updated)

    def from_file(self, header_file, header_id=None, date_updated=None):
        """
        Load a Wrapper from a file

        :param header_file: The file path
        :param header_id: The header ID
        :param date_updated: The last modification date of the header
        :return: EsoWrapper|None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_file '{header_file}', header_id '{header_id}', date_updated '{date_updated}'")

        with open(header_file, 'r', encoding='utf8') as input_file:
            header_string = input_file.read()

        return self.from_string(header_string, header_id, date_updated)

    def from_cache(self, header_id, date_updated=None):
        """
        Load an ESO Wrapper from a cache file

        :param header_id: The header ID
        :param date_updated: The last modification date of the header
        :return: EsoWrapper|None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_id '{header_id}', date_updated '{date_updated}'")

        # Build the cache file path
        cache_file_path = self.build_cache_path(header_id)

        # Case 0: No cache path
        if not cache_file_path:
            logger.debug("No cache file path")
            return None

        # Case 1: Cache file missing
        if not os.path.exists(cache_file_path):
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"cache file '{cache_file_path}' missing")
            return None

        # get file stats:
        file_stats = os.stat(cache_file_path)

        # Case 1b: Cache file is 0-size (empty <=> I/O write failure):
        if file_stats.st_size == 0:
            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"cache file '{cache_file_path}' is empty")
            return None

        if date_updated:
            a_mtime = arrow.get(file_stats.st_mtime).floor('second')
            # It's necessary to reset the timezone (to UTC)...
            a_updated = arrow.get(date_updated).floor('second').to('UTC')

            # Case 2: Cache file present, but obsolete
            if a_mtime < a_updated:
                if logger.isEnabledFor(logging.DEBUG):
                    logger.debug(f"cache file '{cache_file_path}' obsolete")
                return None

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"mtime: {a_mtime} updated: {a_updated}")

            if (a_mtime - a_updated).seconds > 1:
                self.setFileModTime(cache_file_path, a_updated)

        # Case 3: Cache file present, and up-to-date
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"cache file '{cache_file_path}' up-to-date")

        return self.from_file(cache_file_path, header_id, date_updated)

    def from_eso(self, header_id, date_updated=None):
        """
        Load an ESO Wrapper from ESO archive and save cache file

        :param header_id: The header ID
        :param date_updated: The last modification date of the header
        :return: EsoWrapper|None
        """
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"header_id '{header_id}', date_updated '{date_updated}'")

        # Download header content from ESO
        html_content = requests_get(f"http://archive.eso.org/hdr?DpId={header_id}", b'INSTRUME')

        html_content_root = BeautifulSoup(html_content, 'html5lib')
        header_string = html_content_root.select('pre')[0].text

        if len(header_string):
            # Save cache file (not empty <=> I/O write failure):
            cache_file_path = self.build_cache_path(header_id, make_dirs=True)
            logger.info(f"save cache file '{cache_file_path}'")

            with open(cache_file_path, 'w') as output_file:
                output_file.write(header_string)

            if date_updated:
                # It's necessary to reset the timezone (to UTC)...
                a_updated = arrow.get(date_updated).floor('second').to('UTC')
                self.setFileModTime(cache_file_path, a_updated)

        # Build ESO Wrapper from string
        header = self.from_string(header_string, header_id, date_updated)
        return header

    @staticmethod
    def setFileModTime(file_path, a_updated):
        # set file last modification date
        # Use touch -t 200101011200 file to alter file stats
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"setFileModTime({file_path}) at {a_updated}")

        os.utime(file_path, (a_updated.timestamp(), a_updated.timestamp()))


class EsoHeaderFactory(object):

    def __init__(self, settings=None):
        self.settings = settings

    @staticmethod
    def from_database(db_session, header_id):
        """
        Load an Header object from DB

        :param db_session: The database session
        :param header_id: The header ID
        :return: Header|None
        """
        return Header.get(db_session, header_id)

    @staticmethod
    def from_wrapper(wrapper, date_updated=None, header=None):
        """
        Load an Header object from an ESO Wrapper

        :param wrapper: The ESO Wrapper object
        :param date_updated: The last modification date of the header
        :param header: The Header object to update
        :return: Header|None
        """
        return wrapper.build_header(date_updated=date_updated, header=header)


# ERRORS ###############################################################################################################

class EsoFactoryError(Exception):
    pass


class EsoWrapperFactoryError(EsoFactoryError):
    pass


class EsoHeaderFactoryError(EsoFactoryError):
    pass
