# coding=utf-8
# http://docs.pylonsproject.org/projects/pyramid_jinja2/en/latest/#jinja2-filters
# http://jinja.pocoo.org/docs/dev/api/#writing-filters
from pytz import timezone


def filter_datetime(value, tz='UTC'):
    if value is not None:
        local_tz = timezone(tz)
        return value.astimezone(local_tz).strftime('%Y-%m-%d %H:%M:%S %Z%z')
    return None
