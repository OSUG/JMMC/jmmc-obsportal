# coding: utf-8
import logging
import os

from astropy.io.votable.tree import Field
from astropy.time import Time
from sqlalchemy import (
    Column,
    Boolean,
    Unicode,
    String,
    Float,
    ForeignKey,
    func, bindparam,
)
from sqlalchemy.orm import relationship, joinedload
from sqlalchemy_jsonfield import JSONField
from sqlalchemy_utc import UtcDateTime

from .meta import Base
from .utils import QueryUtil

from . import Exposure

logger = logging.getLogger(__name__)


class Header(Base):
    __tablename__ = 'header'
    __table_args__ = (
        {'schema': 'obsportal'}
    )

    # POLYMORPHISM #####################################################################################################
    type = Column(String(5))
    __mapper_args__ = {
        'polymorphic_on': type,
        'polymorphic_identity': "UNDEF"
    }

    # HEADER ###########################################################################################################
    id = Column(Unicode, nullable=False, primary_key=True)
    date_updated = Column(UtcDateTime, index=True)

    program_id = Column(Unicode, index=True)
    # program_id = Column(ForeignKey('obsportal.program.id', onupdate='CASCADE', ondelete='RESTRICT'), nullable=False)
    # program = relationship('Program', back_populates='observations')

    is_observation = Column(Boolean)

    # ESO DPR keywords #################################################################################################
    dp_cat = Column(Unicode)
    dp_type = Column(Unicode)

    # OBSERVATION EXPOSURE #############################################################################################
    exposures = relationship('Exposure', back_populates='header', cascade='all, delete-orphan', passive_deletes=True)

    # INTERFEROMETER ###################################################################################################
    interferometer_id = Column(ForeignKey('obsportal.interferometer.id', onupdate='CASCADE', ondelete='RESTRICT'),
                               nullable=False, index=True)
    interferometer = relationship('Interferometer', back_populates='headers')

    # INSTRUMENT ######################################################################################################
    instrument_id = Column(ForeignKey('obsportal.instrument.id', onupdate='CASCADE', ondelete='RESTRICT'),
                           nullable=False, index=True)
    instrument = relationship('Instrument', back_populates='headers')

    # ACQUISITION ######################################################################################################

    # Time ranges:
    mjd_start = Column(Float, index=True)

    # CONTENT ##########################################################################################################
    file_path = Column(Unicode)
    file_hash = Column(String)
    extracted_content = Column(JSONField)
    validation_log = Column(JSONField)

    # METHODS ##########################################################################################################
    def __repr__(self):
        return f'<Header {self.id}>'

    @property
    def obs_time(self):
        if self.mjd_start is not None:
            observing_time = Time(self.mjd_start, format='mjd')
            return observing_time.iso
        return None

    def is_on_disk(self, data_path):
        return os.path.exists(os.path.join(data_path, self.type, self.file_path))

    def add_content(self, key, value):
        if self.extracted_content is None:
            self.extracted_content = {}
        self.extracted_content[key] = value

    def add_message(self, value):
        if self.validation_log is None:
            self.validation_log = []
        self.validation_log.append(value)

    @property
    def relative_file_path(self):
        return self.file_path

    # VOTABLE EXPORT ###################################################################################################

    @staticmethod
    def votable_fields(votable, all_fields=True):
        return [
            Field(votable, name="hdr_id", datatype="unicodeChar", arraysize="*", ucd="meta.id"),
            Field(votable, name="hdr_program", datatype="unicodeChar", arraysize="*", ucd="meta.id"),
            Field(votable, name="dp_cat", datatype="unicodeChar", arraysize="*"),
            Field(votable, name="dp_type", datatype="unicodeChar", arraysize="*"),

            Field(votable, name="interferometer_name", datatype="unicodeChar", arraysize="*", ucd="meta.id"),
            Field(votable, name="instrument_name", datatype="unicodeChar", arraysize="*", ucd="meta.id;instr"),

            Field(votable, name="hdr_mjd_start", datatype="double", ucd="time.start;obs.exposure"),
            Field(votable, name="hdr_validation_log", datatype="unicodeChar", arraysize="*", ucd="")  # FIXME: UCD
        ]

    def votable_array(self, all_fields=True):
        return (
            self.id or '',
            self.program_id or '',
            self.dp_cat or '',
            self.dp_type or '',

            self.interferometer_id or '',
            self.instrument_id or '',

            self.mjd_start,
            self.validation_log or ''
        )

    @classmethod
    def last_modification_date(cls, db_session, instrument_name=None):
        query = db_session.query(func.max(Header.date_updated))

        if instrument_name:
            query = query.select_from(Header).filter(Header.instrument_id == instrument_name)

        return query.scalar()

    # SEARCH ###########################################################################################################

    @classmethod
    def get(cls, db_session, oid):
        return QueryUtil.queryGet(cls, db_session, oid) # No cache

    @classmethod
    def search(cls, db_session,
               instrument=None,
               program_id=None,
               use_limit=True,
               load_exposure_ids=False):

        # Build query params
        statement_params = {}

        # Prepare query: eagerly load useful relationships to avoid extra sub-queries:
        query = db_session.query(Header)
        if load_exposure_ids:
            query = query.options(
                joinedload(Header.exposures).load_only(Exposure.id)  # do not load header now
            )

        if instrument:
            statement_params['instrument'] = instrument
            query = query.filter(Header.instrument_id == bindparam('instrument'))

        if program_id:
            statement_params['program_id'] = program_id
            query = query.filter(Header.program_id == bindparam('program_id'))

        logger.info(f"statement: {statement_params}")

        # Finalize query
        query = query.order_by(Header.mjd_start.desc())
        # always sort by id to ensure stable sort:
        query = query.order_by(Header.id.asc())

        if use_limit:
            query = query.limit(1000)

        # Execute query
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query: {query}")

        results = query.params(statement_params).all()

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"found {len(results)} results")
        return results


class EsoHeader(Header):
    __mapper_args__ = {
        'polymorphic_identity': "ESO"
    }

    @property
    def relative_file_path(self):
        return os.path.join('eso', self.file_path)

class CharaHeader(Header):
    __mapper_args__ = {
        'polymorphic_identity': "CHARA"
    }

    @property
    def relative_file_path(self):
        return os.path.join('chara', self.instrument_id, self.file_path)