# coding: utf-8
from .abstract_enum import AbstractEnum


class EnumProgramType(AbstractEnum):
    Normal = 0
    GTO = 1
    ToO = 2
    Large = 3
    DDT = 4
    Short = 5
    Calibration = 6
    Monitoring = 7

    def __str__(self):
        return self.name


switcher = {
    0: EnumProgramType.Normal,
    1: EnumProgramType.GTO,
    2: EnumProgramType.ToO,
    3: EnumProgramType.Large,
    4: EnumProgramType.DDT,
    5: EnumProgramType.Short,
    6: EnumProgramType.Calibration,
    7: EnumProgramType.Monitoring
}


def findProgramType(value):
    if value:
        return switcher[value]
    return EnumProgramType.Normal
