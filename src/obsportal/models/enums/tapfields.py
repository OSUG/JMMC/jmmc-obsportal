# coding: utf-8
from enum import Enum


class EnumEsoTapField(Enum):
    RELEASE_DATE = 'release_date'

    PROG_ID = 'prog_id'
    PROG_TITLE = 'prog_title'
    PROG_TYPE = 'prog_type'
    PI_COI = 'pi_coi'

    @staticmethod
    def all_columns():
        values = [el.value for el in EnumEsoTapField]
        return ', '.join(values)


