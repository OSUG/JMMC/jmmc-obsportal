# coding: utf-8
from .abstract_enum import AbstractEnum


class EnumObservationCategory(AbstractEnum):
    science = 'science'
    calibrator = 'calibrator'
