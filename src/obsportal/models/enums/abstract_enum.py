# coding: utf-8
from enum import Enum


# See: https://stackoverflow.com/questions/44078845/using-wtforms-with-enum
class AbstractEnum(Enum):
    __label__ = {}
    __latex__ = {}
    __exclude__ = []

    @classmethod
    def to_label(cls, value):
        return cls.__label__.get(value, value)

    @classmethod
    def to_latex(cls, value):
        return cls.__latex__.get(value, '$%s$' % value)

    @classmethod
    def choices(cls, latex=False, capitalized=False, union_enums=None, use_name=False, prepend=None, empty=None,
                excludes=[]):
        """Get enum choices for ``wtforms.SelectField()`` .

        :param bool latex: Render latex formula as value.
        :param bool capitalized: Capitalize value.
        :param AbstractEnum|list|tuple|dit union_enums: Choice is comprised of enums union.
        :param bool use_name: Use enum field name as key.
        :param list[tuple]|tuple[tuple] prepend: Prepend the choices with the given values.
        :param str empty: Prepend the choices with this label for empty value.
        :return: The list of choices tuples ``(key, value)``.
        :rtype: list[tuple]
        """

        choices = []
        for choice in cls:
            if choice.name not in cls.__exclude__ and choice.name not in excludes:
                if latex:
                    choices.append((choice.name if use_name else choice.value, cls.to_latex(choice.value)))
                elif capitalized:
                    choices.append((choice.name if use_name else choice.value, cls.to_label(choice.value).capitalize()))
                else:
                    choices.append((choice.name if use_name else choice.value, cls.to_label(choice.value)))

        if union_enums is not None:
            if type(union_enums) in (list, tuple, dict):
                for union_enum in union_enums:
                    for enum_choice in union_enum.choices(latex=latex, capitalized=capitalized, use_name=use_name,
                                                          prepend=prepend, empty=empty, excludes=excludes):
                        if enum_choice not in choices:
                            choices.append(enum_choice)
            else:
                for enum_choice in union_enums.choices(latex=latex, capitalized=capitalized, use_name=use_name,
                                                       prepend=prepend, empty=empty, excludes=excludes):
                    if enum_choice not in choices:
                        choices.append(enum_choice)

        if prepend:
            if type(prepend) == tuple:
                choices.insert(0, prepend)
            elif type(prepend) == list:
                prepend.reverse()
                for p in prepend:
                    choices.insert(0, p)

        if empty is not None:
            choices.insert(0, ("", empty))

        return choices

    @classmethod
    def name_choices(cls, latex=False, capitalized=False, union_enums=None, prepend=None, empty=None):
        """Get enum choices for ``wtforms.SelectField()`` using enum field name as key.

        :param bool latex: Render latex formula as value.
        :param bool capitalized: Capitalize value.
        :param AbstractEnum|list|tuple|dit union_enums: Choice is comprised of enums union.
        :param list[tuple]|tuple[tuple] prepend: Prepend the choices with the given values.
        :param str empty: Prepend the choices with this label for empty value.
        :return: The list of choices tuples ``(key, value)``.
        :rtype: list[tuple]
        """
        use_name = True
        return cls.choices(latex=latex, capitalized=capitalized, use_name=use_name, union_enums=union_enums,
                           prepend=prepend, empty=empty)

    @classmethod
    def coerce(cls, item):
        item = cls(item) if not isinstance(item, cls) else item  # a ValueError thrown if item is not defined in cls.
        return item.value

    @classmethod
    def coerce_or_empty(cls, item):
        if item is None or item == "":
            return None
        else:
            return cls.coerce(item)

    @classmethod
    def name_coerce(cls, item):
        item = getattr(cls, item) if not isinstance(item,
                                                    cls) else item  # a ValueError thrown if item is not defined in cls.
        return item.name

    @classmethod
    def name_coerce_or_empty(cls, item):
        if item is None or item == "":
            return None
        else:
            return cls.name_coerce(item)

    def __str__(self):
        return self.value

    def __eq__(self, other):
        if isinstance(other, Enum):
            return self.value == other.value
        else:
            return self.value == other

    def __ne__(self, other):
        if isinstance(other, Enum):
            return self.value != other.value
        else:
            return self.value != other

    def latex(self):
        return self.__class__.__latex__.get(self.value, '$%s$' % self.value)

    def label(self):
        return self.__class__.__label__.get(self.value, self.value)
