# coding: utf-8
import logging

from astropy import units
from astropy.coordinates import Angle
from astropy.io.votable.tree import Field
from astropy.time import Time
import html
from sqlalchemy import (
    Column,
    Unicode,
    Float,
    SmallInteger,
    ForeignKey,
    text, func
)
from sqlalchemy.orm import relationship, joinedload, contains_eager
from sqlalchemy.sql.expression import bindparam
from sqlalchemy_jsonfield import JSONField
from sqlalchemy_utc import UtcDateTime

from .meta import Base
from .utils import QueryUtil
from .utils.xmatch import XMatch

from .instrument import Instrument, InstrumentMode

logger = logging.getLogger(__name__)

str_nan = 'NaN'
float_nan = float(str_nan)


def escape(s):
    return html.escape(s, False)


class Exposure(Base):
    __tablename__ = 'exposure'
    __table_args__ = (
        {'schema': 'obsportal'}
    )
    id = Column(Unicode, nullable=False, primary_key=True)

    # OBSERVATION ######################################################################################################
    observation_id = Column(ForeignKey('obsportal.observation.id', onupdate='CASCADE', ondelete='RESTRICT'),
                            nullable=False, index=True)
    observation = relationship('Observation', back_populates='exposures')

    # HEADER ###########################################################################################################
    header_id = Column(ForeignKey('obsportal.header.id', onupdate='CASCADE', ondelete='RESTRICT'),
                       nullable=False, index=True)
    header = relationship('Header', back_populates='exposures')

    # ACQUISITION ######################################################################################################
    date_updated = Column(UtcDateTime)

    # Time ranges:
    mjd_start = Column(Float, index=True)
    mjd_end = Column(Float, index=True)

    @property
    def obs_time_start(self):
        if self.mjd_start is not None:
            observing_time = Time(self.mjd_start, format='mjd')
            return observing_time.iso
        return None

    @property
    def obs_time_end(self):
        if self.mjd_end is not None:
            observing_time = Time(self.mjd_end, format='mjd')
            return observing_time.iso
        return None

    @property
    def obs_exp_time(self):
        if self.mjd_end is not None:
            return (self.mjd_end - self.mjd_start) * 86400.0  # days to seconds
        return None

    @property
    def obs_exp_time_string(self):
        exp_time = self.obs_exp_time
        if exp_time is not None:
            return '%.1f' % exp_time
        return None

    # Baselines:
    projected_baselines = Column(JSONField)

    # WEATHER CONDITIONS ###############################################################################################
    tau0 = Column(Float)  # Coherence time [seconds]
    temperature = Column(Float)  # Observatory ambient temperature [C]
    seeing = Column(Float)  # Observatory seeing [arcsec]

    # VALIDATION #######################################################################################################
    validation_level = Column(SmallInteger, default=0, server_default=text('0'))
    invalid_fields = Column(JSONField)

    # Extra ######################################################################################################
    date_release = Column(UtcDateTime)

    # METHOD ###########################################################################################################
    def __repr__(self):
        return f'<Exposure {self.id}>'

    def add_invalid_field(self, value):
        if self.invalid_fields is None:
            self.invalid_fields = []
        self.invalid_fields.append(value)

    # VOTABLE EXPORT ###################################################################################################

    @staticmethod
    def votable_fields(votable, all_fields=True):
        if all_fields:
            return [
                # EXPOSURE
                Field(votable, name="exp_id", datatype="unicodeChar", arraysize="*", ucd="meta.id"),
                Field(votable, name="exp_header_id", datatype="unicodeChar", arraysize="*", ucd="meta.id"),
                Field(votable, name="exp_mjd_start", datatype="double", ucd="time.start;obs.exposure", unit="d"),
                Field(votable, name="exp_mjd_end", datatype="double", ucd="time.end;obs.exposure", unit="d"),
                Field(votable, name="exp_projected_baselines", datatype="unicodeChar", arraysize="*", ucd="instr.baseline"),
                Field(votable, name="exp_validation_level", datatype="int", ucd="meta.code.qual"),
                Field(votable, name="exp_validation_log", datatype="unicodeChar", arraysize="*", ucd=""),  # FIXME: UCD

                Field(votable, name="exp_tau0", datatype="double", ucd="time.processing", unit="s"),  # FIXME: UCD
                Field(votable, name="exp_temperature", datatype="double", ucd="phys.temperature", unit="deg"),
                Field(votable, name="exp_seeing", datatype="double", ucd="instr.obsty.seeing", unit="as"),

                Field(votable, name="exp_date_updated", datatype="unicodeChar", arraysize="*", ucd="time.processing"),
                Field(votable, name="exp_date_release", datatype="unicodeChar", arraysize="*", ucd="time.release;obs"),

                # OBSERVATION
                Field(votable, name="obs_id", datatype="int", ucd="meta.id"),
                Field(votable, name="obs_type", datatype="unicodeChar", arraysize="*", ucd="meta.id;class"),

                # PROGRAM
                Field(votable, name="obs_program", datatype="unicodeChar", arraysize="*", ucd="meta.id"),
                Field(votable, name="obs_program_title", datatype="unicodeChar", arraysize="*", ucd="meta.title"),
                Field(votable, name="obs_program_pi_coi", datatype="unicodeChar", arraysize="*", ucd="meta.curation;obs.observer"),

                # INTERFEROMETER
                Field(votable, name="interferometer_name", datatype="unicodeChar", arraysize="*", ucd="meta.id"),
                Field(votable, name="interferometer_stations", datatype="unicodeChar", arraysize="*"),

                # INSTRUMENT
                Field(votable, name="instrument_name", datatype="unicodeChar", arraysize="*", ucd="meta.id;instr"),
                Field(votable, name="instrument_mode", datatype="unicodeChar", arraysize="*", ucd="meta.id;instr.setup"),
                Field(votable, name="instrument_submode", datatype="unicodeChar", arraysize="*", ucd="meta.id;instr.setup"),

                # spectral axis:
                Field(votable, name="instrument_em_min", datatype="double", ucd="em.wl;stat.min", unit="m"),
                Field(votable, name="instrument_em_max", datatype="double", ucd="em.wl;stat.max", unit="m"),
                Field(votable, name="instrument_em_res_power", datatype="double", ucd="spect.resolution"),

                # TARGET
                Field(votable, name="target_id", datatype="int", ucd="meta.id"),
                Field(votable, name="target_name", datatype="unicodeChar", arraysize="*", ucd="meta.id;meta.main"),
                Field(votable, name="target_ra", datatype="double", ucd="pos.eq.ra;meta.main", unit="deg"),
                Field(votable, name="target_dec", datatype="double", ucd="pos.eq.dec;meta.main", unit="deg")
            ]
        # not all fields:
        return [
            # EXPOSURE
            Field(votable, name="exp_id", datatype="unicodeChar", arraysize="*", ucd="meta.id"),
            # exp_header_id
            Field(votable, name="exp_mjd_start", datatype="double", ucd="time.start;obs.exposure", unit="d"),
            Field(votable, name="exp_mjd_end", datatype="double", ucd="time.end;obs.exposure", unit="d"),
            Field(votable, name="exp_projected_baselines", datatype="unicodeChar", arraysize="*", ucd="instr.baseline"),
            # exp_validation_level
            # exp_validation_log

            Field(votable, name="exp_tau0", datatype="double", ucd="time.processing", unit="s"),  # FIXME: UCD
            Field(votable, name="exp_temperature", datatype="double", ucd="phys.temperature", unit="deg"),
            Field(votable, name="exp_seeing", datatype="double", ucd="instr.obsty.seeing", unit="as"),
            Field(votable, name="exp_date_updated", datatype="unicodeChar", arraysize="*", ucd="time.processing"),

            # OBSERVATION
            # obs_id
            Field(votable, name="obs_type", datatype="unicodeChar", arraysize="*", ucd="meta.id;class"),
            Field(votable, name="obs_program", datatype="unicodeChar", arraysize="*", ucd="meta.id"),

            # INTERFEROMETER
            Field(votable, name="interferometer_name", datatype="unicodeChar", arraysize="*", ucd="meta.id"),
            Field(votable, name="interferometer_stations", datatype="unicodeChar", arraysize="*"),

            # INSTRUMENT
            Field(votable, name="instrument_name", datatype="unicodeChar", arraysize="*", ucd="meta.id;instr"),
            Field(votable, name="instrument_mode", datatype="unicodeChar", arraysize="*", ucd="meta.id;instr.setup"),
            Field(votable, name="instrument_submode", datatype="unicodeChar", arraysize="*", ucd="meta.id;instr.setup"),

            # spectral axis:
            # instrument_em_min instrument_em_max instrument_em_res_power

            # TARGET
            # target_id
            Field(votable, name="target_name", datatype="unicodeChar", arraysize="*", ucd="meta.id;meta.main"),
            Field(votable, name="target_ra", datatype="double", ucd="pos.eq.ra;meta.main", unit="deg"),
            Field(votable, name="target_dec", datatype="double", ucd="pos.eq.dec;meta.main", unit="deg")
        ]

    def votable_array(self, all_fields=True):
        if all_fields:
            return (
                # EXPOSURE
                self.id,
                self.header_id,
                self.mjd_start if self.mjd_start else float_nan,
                self.mjd_end if self.mjd_end else float_nan,
                self.projected_baselines or '',
                self.validation_level or 0,
                self.header.validation_log or '',

                self.tau0 if self.tau0 else float_nan,
                self.temperature if self.temperature else float_nan,
                self.seeing if self.seeing else float_nan,

                str(self.date_updated),
                str(self.date_release) if self.date_release else '',

                # OBSERVATION
                self.observation_id,
                self.observation.category,

                # PROGRAM
                self.observation.program_id or '',
                self.observation.program.title or '',
                self.observation.program.pi_coi or '',

                # INTERFEROMETER
                self.observation.instrument.interferometer_id or '',
                self.observation.stations or '',

                # INSTRUMENT
                self.observation.instrument_id or '',
                self.observation.instrument_mode.name if self.observation.instrument_mode else '',
                self.observation.instrument_submode or '',

                # spectral axis:
                (self.observation.instrument_mode.wavelength_min * 1e-6) if self.observation.instrument_mode else float_nan,
                (self.observation.instrument_mode.wavelength_max * 1e-6) if self.observation.instrument_mode else float_nan,
                self.observation.instrument_mode.spectral_resolution if self.observation.instrument_mode else float_nan,

                # TARGET
                self.observation.target_id,
                self.observation.target.name or '',
                self.observation.target.ra if self.observation.target.ra else float_nan,
                self.observation.target.dec if self.observation.target.dec else float_nan
            )
        # not all fields:
        return (
            # EXPOSURE
            self.id,
            # self.header_id,
            self.mjd_start if self.mjd_start else float_nan,
            self.mjd_end if self.mjd_end else float_nan,
            self.projected_baselines or '',
            # self.validation_level or 0,
            # self.header.validation_log or '',
            self.tau0 if self.tau0 else float_nan,
            self.temperature if self.temperature else float_nan,
            self.seeing if self.seeing else float_nan,
            str(self.date_updated),

            # OBSERVATION
            # self.observation_id,
            self.observation.category,
            self.observation.program_id or '',

            # INTERFEROMETER
            self.observation.instrument.interferometer_id or '',
            self.observation.stations or '',

            # INSTRUMENT
            self.observation.instrument_id or '',
            self.observation.instrument_mode.name if self.observation.instrument_mode else '',
            self.observation.instrument_submode or '',

            # spectral axis:
            # wavelength_min, wavelength_max, spectral_resolution

            # TARGET
            # self.observation.target_id,
            self.observation.target.name or '',
            self.observation.target.ra if self.observation.target.ra else float_nan,
            self.observation.target.dec if self.observation.target.dec else float_nan
        )

    # Optimized VOTable ################################################################################################

    @staticmethod
    def write_votable_header(fd, status, last_mod_date, all_fields=True):
        if all_fields:
            fd.write(("""<?xml version="1.0" encoding="utf-8"?>
<VOTABLE version="1.4" xmlns="http://www.ivoa.net/xml/VOTable/v1.4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.ivoa.net/xml/VOTable/v1.4">
 <RESOURCE type="results">
  <PARAM ID="status" arraysize="*" datatype="unicodeChar" name="status" value="%s"/>
  <PARAM ID="last_mod_date" arraysize="*" datatype="unicodeChar" name="last_mod_date" value="%s"/>
  <TABLE>
   <FIELD ID="exp_id" arraysize="*" datatype="unicodeChar" name="exp_id" ucd="meta.id"/>
   <FIELD ID="exp_header_id" arraysize="*" datatype="unicodeChar" name="exp_header_id" ucd="meta.id"/>
   <FIELD ID="exp_mjd_start" datatype="double" name="exp_mjd_start" ucd="time.start;obs.exposure" unit="d"/>
   <FIELD ID="exp_mjd_end" datatype="double" name="exp_mjd_end" ucd="time.end;obs.exposure" unit="d"/>
   <FIELD ID="exp_projected_baselines" arraysize="*" datatype="unicodeChar" name="exp_projected_baselines" ucd="instr.baseline"/>
   <FIELD ID="exp_validation_level" datatype="int" name="exp_validation_level" ucd="meta.code.qual"/>
   <FIELD ID="exp_validation_log" arraysize="*" datatype="unicodeChar" name="exp_validation_log"/>
   <FIELD ID="exp_tau0" datatype="double" name="exp_tau0" ucd="time.processing" unit="s"/>
   <FIELD ID="exp_temperature" datatype="double" name="exp_temperature" ucd="phys.temperature" unit="deg"/>
   <FIELD ID="exp_seeing" datatype="double" name="exp_seeing" ucd="instr.obsty.seeing" unit="as"/>
   <FIELD ID="exp_date_updated" arraysize="*" datatype="unicodeChar" name="exp_date_updated" ucd="time.processing"/>
   <FIELD ID="exp_date_release" arraysize="*" datatype="unicodeChar" name="exp_date_release" ucd="time.release;obs"/>
   <FIELD ID="obs_id" datatype="int" name="obs_id" ucd="meta.id"/>
   <FIELD ID="obs_type" arraysize="*" datatype="unicodeChar" name="obs_type" ucd="meta.id;class"/>
   <FIELD ID="obs_program" arraysize="*" datatype="unicodeChar" name="obs_program" ucd="meta.id"/>
   <FIELD ID="obs_program_title" arraysize="*" datatype="unicodeChar" name="obs_program_title" ucd="meta.title"/>
   <FIELD ID="obs_program_pi_coi" arraysize="*" datatype="unicodeChar" name="obs_program_pi_coi" ucd="meta.curation;obs.observer"/>
   <FIELD ID="interferometer_name" arraysize="*" datatype="unicodeChar" name="interferometer_name" ucd="meta.id"/>
   <FIELD ID="interferometer_stations" arraysize="*" datatype="unicodeChar" name="interferometer_stations"/>
   <FIELD ID="instrument_name" arraysize="*" datatype="unicodeChar" name="instrument_name" ucd="meta.id;instr"/>
   <FIELD ID="instrument_mode" arraysize="*" datatype="unicodeChar" name="instrument_mode" ucd="meta.id;instr.setup"/>
   <FIELD ID="instrument_submode" arraysize="*" datatype="unicodeChar" name="instrument_submode" ucd="meta.id;instr.setup"/>
   <FIELD ID="instrument_em_min" datatype="double" name="instrument_em_min" ucd="em.wl;stat.min" unit="m"/>
   <FIELD ID="instrument_em_max" datatype="double" name="instrument_em_max" ucd="em.wl;stat.max" unit="m"/>
   <FIELD ID="instrument_em_res_power" datatype="double" name="instrument_em_res_power" ucd="spect.resolution"/>
   <FIELD ID="target_id" datatype="int" name="target_id" ucd="meta.id"/>
   <FIELD ID="target_name" arraysize="*" datatype="unicodeChar" name="target_name" ucd="meta.id;meta.main"/>
   <FIELD ID="target_ra" datatype="double" name="target_ra" ucd="pos.eq.ra;meta.main" unit="deg"/>
   <FIELD ID="target_dec" datatype="double" name="target_dec" ucd="pos.eq.dec;meta.main" unit="deg"/>
   <DATA>
    <TABLEDATA>""" % (status, last_mod_date)).encode())
        else:
            fd.write(("""<?xml version="1.0" encoding="utf-8"?>
<VOTABLE version="1.4" xmlns="http://www.ivoa.net/xml/VOTable/v1.4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.ivoa.net/xml/VOTable/v1.4">
 <RESOURCE type="results">
  <PARAM ID="status" arraysize="*" datatype="unicodeChar" name="status" value="%s"/>
  <PARAM ID="last_mod_date" arraysize="*" datatype="unicodeChar" name="last_mod_date" value="%s"/>
  <TABLE>
   <FIELD ID="exp_id" arraysize="*" datatype="unicodeChar" name="exp_id" ucd="meta.id"/>
   <FIELD ID="exp_mjd_start" datatype="double" name="exp_mjd_start" ucd="time.start;obs.exposure" unit="d"/>
   <FIELD ID="exp_mjd_end" datatype="double" name="exp_mjd_end" ucd="time.end;obs.exposure" unit="d"/>
   <FIELD ID="exp_projected_baselines" arraysize="*" datatype="unicodeChar" name="exp_projected_baselines" ucd="instr.baseline"/>
   <FIELD ID="exp_tau0" datatype="double" name="exp_tau0" ucd="time.processing" unit="s"/>
   <FIELD ID="exp_temperature" datatype="double" name="exp_temperature" ucd="phys.temperature" unit="deg"/>
   <FIELD ID="exp_seeing" datatype="double" name="exp_seeing" ucd="instr.obsty.seeing" unit="as"/>
   <FIELD ID="exp_date_updated" arraysize="*" datatype="unicodeChar" name="exp_date_updated" ucd="time.processing"/>
   <FIELD ID="obs_type" arraysize="*" datatype="unicodeChar" name="obs_type" ucd="meta.id;class"/>
   <FIELD ID="obs_program" arraysize="*" datatype="unicodeChar" name="obs_program" ucd="meta.id"/>
   <FIELD ID="interferometer_name" arraysize="*" datatype="unicodeChar" name="interferometer_name" ucd="meta.id"/>
   <FIELD ID="interferometer_stations" arraysize="*" datatype="unicodeChar" name="interferometer_stations"/>
   <FIELD ID="instrument_name" arraysize="*" datatype="unicodeChar" name="instrument_name" ucd="meta.id;instr"/>
   <FIELD ID="instrument_mode" arraysize="*" datatype="unicodeChar" name="instrument_mode" ucd="meta.id;instr.setup"/>
   <FIELD ID="instrument_submode" arraysize="*" datatype="unicodeChar" name="instrument_submode" ucd="meta.id;instr.setup"/>
   <FIELD ID="target_name" arraysize="*" datatype="unicodeChar" name="target_name" ucd="meta.id;meta.main"/>
   <FIELD ID="target_ra" datatype="double" name="target_ra" ucd="pos.eq.ra;meta.main" unit="deg"/>
   <FIELD ID="target_dec" datatype="double" name="target_dec" ucd="pos.eq.dec;meta.main" unit="deg"/>
   <DATA>
    <TABLEDATA>""" % (status, last_mod_date)).encode())

    @staticmethod
    def write_votable_footer(fd):
        fd.write("""
    </TABLEDATA>
   </DATA>
  </TABLE>
 </RESOURCE>
</VOTABLE>
""".encode())

    def write_votable_row(self, fd, all_fields=True):
        if all_fields:
            fd.write(("""
<TR>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%d</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%d</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%d</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
</TR>""" % (
            # EXPOSURE
            self.id,
            self.header_id,
            ('%.15g' % self.mjd_start) if self.mjd_start else str_nan,
            ('%.15g' % self.mjd_end) if self.mjd_end else str_nan,
            self.projected_baselines or '',
            self.validation_level or 0,
            escape(str(self.header.validation_log)) if self.header.validation_log else '',

            ('%.15g' % self.tau0) if self.tau0 else str_nan,
            ('%.15g' % self.temperature) if self.temperature else str_nan,
            ('%.15g' % self.seeing) if self.seeing else str_nan,

            str(self.date_updated),
            str(self.date_release) if self.date_release else '',

            # OBSERVATION
            self.observation_id,
            self.observation.category,

            # PROGRAM
            self.observation.program_id or '',
            escape(self.observation.program.title) if self.observation.program.title else '',
            escape(self.observation.program.pi_coi) if self.observation.program.pi_coi else '',

            # INTERFEROMETER
            self.observation.instrument.interferometer_id or '',
            self.observation.stations or '',

            # INSTRUMENT
            self.observation.instrument_id or '',
            self.observation.instrument_mode.name if self.observation.instrument_mode else str_nan,
            self.observation.instrument_submode or '',

            # spectral axis:
            ('%.15g' % (self.observation.instrument_mode.wavelength_min * 1e-6)) if self.observation.instrument_mode else str_nan,
            ('%.15g' % (self.observation.instrument_mode.wavelength_max * 1e-6)) if self.observation.instrument_mode else str_nan,
            ('%.15g' % self.observation.instrument_mode.spectral_resolution) if self.observation.instrument_mode else str_nan,

            # TARGET
            self.observation.target_id,
            self.observation.target.name or '',
            ('%.15g' % self.observation.target.ra) if self.observation.target.ra else str_nan,
            ('%.15g' % self.observation.target.dec) if self.observation.target.dec else str_nan
        )).encode())
        else:
            fd.write(("""
<TR>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
<TD>%s</TD>
</TR>""" % (
            # EXPOSURE
            self.id,
            # self.header_id,
            ('%.15g' % self.mjd_start) if self.mjd_start else str_nan,
            ('%.15g' % self.mjd_end) if self.mjd_end else str_nan,
            self.projected_baselines or '',
            # self.validation_level
            # self.header.validation_log
            ('%.15g' % self.tau0) if self.tau0 else str_nan,
            ('%.15g' % self.temperature) if self.temperature else str_nan,
            ('%.15g' % self.seeing) if self.seeing else str_nan,
            str(self.date_updated),

            # OBSERVATION
            # self.observation_id
            self.observation.category,

            # PROGRAM
            self.observation.program_id or '',
            # program.title, program.pi_coi

            # INTERFEROMETER
            self.observation.instrument.interferometer_id or '',
            self.observation.stations or '',

            # INSTRUMENT
            self.observation.instrument_id or '',
            self.observation.instrument_mode.name if self.observation.instrument_mode else str_nan,
            self.observation.instrument_submode or '',

            # spectral axis:
            # wavelength_min, wavelength_max, spectral_resolution

            # TARGET
            # self.observation.target_id
            self.observation.target.name or '',
            ('%.15g' % self.observation.target.ra) if self.observation.target.ra else str_nan,
            ('%.15g' % self.observation.target.dec) if self.observation.target.dec else str_nan
        )).encode())

    # SEARCH ###########################################################################################################

    @classmethod
    def get(cls, db_session, oid):
        return QueryUtil.queryGet(cls, db_session, oid) # No cache

    @classmethod
    def search(cls, db_session,
               ra=None, dec=None, radius=None,
               interferometer=None, instrument=None, instrument_mode=None,
               program_id=None,
               target_name=None,
               mjd_from=None, mjd_to=None,
               date_updated_from=None, date_updated_to=None,
               valid_level=None,
               order_by_field=None,
               order_asc=True,
               maxrec=1000,
               all_fields=False,
               as_query=False):
        # defer import to avoid cyclic import
        from . import Header,Program

        # Build query params
        statement_params = {}

        from obsportal.models import Observation, Target

        # Prepare query: eagerly load useful relationships to avoid extra sub-queries:
        query = db_session.query(Exposure).join(Observation).join(Target)
        if all_fields:
            query = query.options(
                joinedload(Exposure.header, innerjoin=True).load_only(Header.validation_log),  # do not load header now
                contains_eager(Exposure.observation)
                    .load_only(Observation.category, Observation.program_id, Observation.instrument_id, Observation.instrument_mode_id,
                               Observation.instrument_submode, Observation.stations, Observation.target_id)
                    .options(
                    contains_eager(Observation.target).load_only(Target.name, Target.ra, Target.dec),
                    joinedload(Observation.program).load_only(Program.title, Program.pi_coi),
                    joinedload(Observation.instrument).load_only(Instrument.interferometer_id),
                    joinedload(Observation.instrument_mode).load_only(InstrumentMode.name, InstrumentMode.wavelength_min, InstrumentMode.wavelength_max,
                                                                      InstrumentMode.spectral_resolution)
                )
            )
        else:
            query = query.options(contains_eager(Exposure.observation)
                    .load_only(Observation.category, Observation.program_id, Observation.instrument_id, Observation.instrument_mode_id,
                               Observation.instrument_submode, Observation.stations, Observation.target_id)
                    .options(
                    contains_eager(Observation.target).load_only(Target.name, Target.ra, Target.dec),
                    joinedload(Observation.instrument).load_only(Instrument.interferometer_id),
                    joinedload(Observation.instrument_mode).load_only(InstrumentMode.name)
                )
            )

        use_pos = False

        if ra is not None and ra != '' and dec is not None and dec != '':
            use_pos = True
            statement_params['ra'] = ra
            statement_params['dec'] = dec

            if radius is None or radius == '':
                radius = 10.0
            else:
                radius = float(radius)
            statement_params['radius'] = float( Angle(radius * units.arcsec).to_value(units.deg) )

            query = query.filter( XMatch(Target.ra, Target.dec) )

        if target_name:
            statement_params['target_name'] = f"%{target_name.lower()}%"
            query = query.filter(func.lower(Target.name).like(func.lower(bindparam('target_name'))))

        if interferometer:
            statement_params['interferometer'] = interferometer
            query = query.filter(Observation.interferometer_id == bindparam('interferometer'))

        if instrument:
            statement_params['instrument'] = instrument
            query = query.filter(Observation.instrument_id == bindparam('instrument'))

        if instrument_mode:
            statement_params['instrument_mode'] = instrument_mode
            query = query.filter(Observation.instrument_mode_id == bindparam('instrument_mode'))

        if program_id:
            statement_params['program_id'] = program_id
            query = query.filter(Observation.program_id == bindparam('program_id'))

        if mjd_from:
            statement_params['mjd_from'] = mjd_from
            query = query.filter(Exposure.mjd_start >= bindparam('mjd_from'))

        if mjd_to:
            statement_params['mjd_to'] = mjd_to
            query = query.filter(Exposure.mjd_end <= bindparam('mjd_to'))

        if date_updated_from:
            statement_params['date_updated_from'] = date_updated_from
            query = query.filter(Exposure.date_updated >= bindparam('date_updated_from'))

        if date_updated_to:
            statement_params['date_updated_to'] = date_updated_to
            query = query.filter(Exposure.date_updated <= bindparam('date_updated_to'))

        if valid_level is not None and valid_level != '':
            statement_params['valid_level'] = valid_level
            if valid_level == 0 or valid_level == '0':
                query = query.filter(Exposure.validation_level == bindparam('valid_level'))
            else:
                query = query.filter(Exposure.validation_level >= bindparam('valid_level'))

        logger.info(f"params: {statement_params} maxrec: [{maxrec}] all_fields: [{all_fields}]")

        # Finalize query
        if use_pos:
            query = query.order_by(Exposure.mjd_start.asc())
        else:
            if order_by_field:
                if logger.isEnabledFor(logging.DEBUG):
                    logger.debug(f"order_by_field: {order_by_field} asc: {order_asc}")
                if order_asc:
                    query = query.order_by(order_by_field.asc())
                else:
                    query = query.order_by(order_by_field.desc())
            else:
                query = query.order_by(Exposure.mjd_start.desc())
        # always sort by id to ensure stable sort:
        query = query.order_by(Exposure.id.asc())

        if maxrec:
            query = query.limit(maxrec)

        # Execute query
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query: {query}")
        if as_query:
            return query.params(statement_params).yield_per(100)  # process by chunks

        results = query.params(statement_params).all()

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"found {len(results)} results")
        return results
