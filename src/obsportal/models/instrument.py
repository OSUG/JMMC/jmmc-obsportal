# coding: utf-8
import logging

from sqlalchemy import (
    Column,
    Integer,
    Float,
    Unicode,
    UniqueConstraint,
    ForeignKey,
    text,
)
from sqlalchemy.orm import relationship

from .meta import Base
from .utils import QueryUtil

logger = logging.getLogger(__name__)


class Instrument(Base):
    __tablename__ = 'instrument'
    __table_args__ = (
        {'schema': 'obsportal'}
    )

    id = Column(Unicode, nullable=False, primary_key=True)

    interferometer_id = Column(ForeignKey('obsportal.interferometer.id', onupdate='CASCADE', ondelete='RESTRICT'),
                               nullable=False, index=True)
    interferometer = relationship('Interferometer', back_populates='instruments')

    name = Column(Unicode, nullable=False, unique=True)

    modes = relationship('InstrumentMode', back_populates='instrument')

    headers = relationship('Header', back_populates='instrument')

    observations = relationship('Observation', back_populates='instrument')

    # STATS ############################################################################################################

    headers_count = Column(Integer, default=0, server_default=text('0'))
    observations_count = Column(Integer, default=0, server_default=text('0'))
    exposures_count = Column(Integer, default=0, server_default=text('0'))

    @classmethod
    def update_stats(cls, db_session):
        logger.debug("update stats")
        cls._update_headers_count(db_session)
        cls._update_observations_count(db_session)
        cls._update_exposures_count(db_session)

    @classmethod
    def _update_headers_count(cls, db_session):
        logger.debug("update headers count")
        query = '''
        UPDATE obsportal.instrument AS i
        SET headers_count = (
            SELECT COUNT(h.id)
            FROM obsportal.header AS h
            WHERE i.id = h.instrument_id
        )
        '''
        return db_session.execute(text(query))

    @classmethod
    def _update_observations_count(cls, db_session):
        logger.debug("update observations count")
        query = '''
        UPDATE obsportal.instrument AS i
        SET observations_count = (
            SELECT SUM(im.observations_count)
            FROM obsportal.instrument_mode AS im
            WHERE i.id = im.instrument_id
        )
        '''
        return db_session.execute(text(query))

    @classmethod
    def _update_exposures_count(cls, db_session):
        logger.debug("update exposures count")
        query = '''
        UPDATE obsportal.instrument AS i
        SET exposures_count = (
            SELECT SUM(im.exposures_count)
            FROM obsportal.instrument_mode AS im
            WHERE i.id = im.instrument_id
        )
        '''
        return db_session.execute(text(query))

    # SEARCH ###########################################################################################################

    @classmethod
    def get(cls, db_session, oid):
        return QueryUtil.queryGetCached(cls, db_session, oid)


class InstrumentMode(Base):
    __tablename__ = 'instrument_mode'
    __table_args__ = (
        UniqueConstraint('instrument_id', 'name', name='uq_instrument_mode_instrument_id_name'),
        {'schema': 'obsportal'}
    )

    id = Column(Integer, nullable=False, primary_key=True)

    instrument_id = Column(ForeignKey('obsportal.instrument.id', onupdate='CASCADE', ondelete='RESTRICT'),
                           nullable=False, index=True)
    instrument = relationship('Instrument', back_populates='modes')

    name = Column(Unicode, nullable=False, index=True)
    wavelength_min = Column(Float)
    wavelength_max = Column(Float)
    spectral_resolution = Column(Float)

    observations = relationship('Observation', back_populates='instrument_mode')

    # STATS ############################################################################################################

    observations_count = Column(Integer, default=0, server_default=text('0'))
    exposures_count = Column(Integer, default=0, server_default=text('0'))

    @classmethod
    def update_stats(cls, db_session):
        logger.debug("update stats")
        cls._update_observations_count(db_session)
        cls._update_exposures_count(db_session)

    @classmethod
    def _update_observations_count(cls, db_session):
        logger.debug("update observations count")
        query = '''
        UPDATE obsportal.instrument_mode AS im
        SET observations_count = (
            SELECT COUNT(o.id)
            FROM obsportal.observation AS o
            WHERE im.id = o.instrument_mode_id
        )
        '''
        return db_session.execute(text(query))

    @classmethod
    def _update_exposures_count(cls, db_session):
        logger.debug("update exposures count")
        query = '''
        UPDATE obsportal.instrument_mode AS im
        SET exposures_count = (
            SELECT SUM(o.exposures_count)
            FROM obsportal.observation AS o
            WHERE im.id = o.instrument_mode_id
        )
        '''
        return db_session.execute(text(query))

    # SEARCH ###########################################################################################################

    @classmethod
    def find(cls, db_session, instrument_id, name):
        return QueryUtil.queryFilterCached(cls, db_session,
                                           instrument_id=instrument_id, name=name)
