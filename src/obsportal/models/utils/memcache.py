# coding: utf-8
import logging

logger = logging.getLogger(__name__)


# Straightforward implementation of the Singleton Pattern
class MemCache(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(MemCache, cls).__new__(cls)
            # Put any initialization here.
            logger.debug("init MemCache")
        return cls._instance

    # enable flag
    enabled = False

    def isEnabled(self):
        return self.enabled

    def setEnabled(self, state):
        self.enabled = state
        logger.info(f"setEnabled : {state}")

    # cache based on dictionary { cls: { key : object } }
    dict = {}
    # query cache based on dictionary { cls: { query_args : object } }
    dict_query_args = {}

    def clear(self):
        if not self.enabled:
            return
        logger.debug("clear MemCache")
        self.dict.clear()

    def dump(self):
        if not self.enabled:
            return
        if logger.isEnabledFor(logging.DEBUG):
            try:
                logger.debug(f"dict = {self.dict}")
                logger.debug(f"dict_query_args = {self.dict_query_args}")
            except Exception as e:
                logger.debug("MemCache dump failed", e)

    def get(self, cls, key):
        if not self.enabled:
            return
        if cls in self.dict:
            cache_dict = self.dict[cls]
            if key in cache_dict:
                item = cache_dict[key]

                if logger.isEnabledFor(logging.DEBUG):
                    logger.debug(f"MemCache.get({cls},{key}) = {item}")
                return item
        return None

    def get_query(self, cls, query_args):
        if not self.enabled:
            return
        if cls in self.dict_query_args:
            query_dict = self.dict_query_args[cls]
            if query_args in query_dict:
                item = query_dict[query_args]

                if logger.isEnabledFor(logging.DEBUG):
                    logger.debug(f"MemCache.get_query({cls},{query_args}) = {item}")
                return item
        return None

    def put(self, cls, key, item, query_args=None):
        if not self.enabled:
            return
        if cls not in self.dict:
            self.dict[cls] = {}

        if key:
            cache_dict = self.dict[cls]
            cache_dict[key] = item

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"MemCache.put({cls},{key}) = {item}")

        if query_args:
            if cls not in self.dict_query_args:
                self.dict_query_args[cls] = {}

            query_dict = self.dict_query_args[cls]
            query_dict[query_args] = item

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug(f"query({cls},{query_args}) = {item}")
