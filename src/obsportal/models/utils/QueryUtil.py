# coding: utf-8
import logging
from .memcache import MemCache

logger = logging.getLogger(__name__)
memcache = MemCache()


def flush(cls, db_session, item, **kwargs):
    if logger.isEnabledFor(logging.DEBUG):
        logger.debug(f"flush({cls}): {item} {kwargs}")

    if item:
        # Mark the object to be added to DB
        db_session.add(item)

        query_args = str(kwargs) if kwargs else None
        memcache.put(cls, item.id, item, query_args)

def queryGet(cls, db_session, oid):
    try:
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query({cls}).get({oid})")

        item = db_session.query(cls).get(oid)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query({cls}).get({oid}) QUERY: {item}")

        return item
    except:
        return None

def queryGetCached(cls, db_session, oid):
    item = memcache.get(cls, oid)
    if item:
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query({cls}).get({oid}) CACHE: {item}")

        return item

    item = queryGet(cls, db_session, oid)
    if item:
        memcache.put(cls, oid, item)
    return item

def queryFilterCached(cls, db_session, **kwargs):
    if not kwargs:
        return None
    query_args = str(kwargs)
    item = memcache.get_query(cls, query_args)
    if item:
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query({cls}).filter_by({query_args}) CACHE: {item}")

        return item
    try:
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query({cls}).filter_by({query_args})")

        item = db_session.query(cls).filter_by(**kwargs).one_or_none()

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query({cls}).filter_by({query_args}) QUERY: {item}")

        if item:
            memcache.put(cls, item.id, item, query_args)
        return item
    except Exception as e:
        return None