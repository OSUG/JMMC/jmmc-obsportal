# coding: utf-8
from sqlalchemy import bindparam
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.expression import ClauseElement
from sqlalchemy.sql import roles


class XMatch(ClauseElement,roles.WhereHavingRole):
    def __init__(self, col_ra, col_dec):
        self.col_ra = col_ra
        self.col_dec = col_dec


@compiles(XMatch)
def _match(element, compiler, **kw):
    return "spoint(radians(%s),radians(%s)) @ scircle(spoint(radians(%s),radians(%s)),radians(%s))" % (
        compiler.process(element.col_ra, **kw),
        compiler.process(element.col_dec, **kw),
        compiler.process(bindparam('ra'), **kw),
        compiler.process(bindparam('dec'), **kw),
        compiler.process(bindparam('radius'), **kw)
    )
