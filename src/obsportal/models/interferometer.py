# coding: utf-8
import logging

from sqlalchemy import (
    Column,
    Float,
    Integer,
    Unicode,
    text,
)
from sqlalchemy.orm import relationship

from .meta import Base
from .utils import QueryUtil

logger = logging.getLogger(__name__)

# See https://www.eso.org/observing/etc/doc/viscalc/vltistations.html

class Interferometer(Base):
    __tablename__ = 'interferometer'
    __table_args__ = (
        {'schema': 'obsportal'}
    )

    id = Column(Unicode, nullable=False, primary_key=True)
    name = Column(Unicode, nullable=False, unique=True)

    latitude = Column(Float)
    longitude = Column(Float)
    altitude = Column(Float)

    instruments = relationship('Instrument', back_populates='interferometer')

    headers = relationship('Header', back_populates='interferometer')

    observations = relationship('Observation', back_populates='interferometer')

    # STATS ############################################################################################################

    headers_count = Column(Integer, default=0, server_default=text('0'))
    observations_count = Column(Integer, default=0, server_default=text('0'))
    exposures_count = Column(Integer, default=0, server_default=text('0'))

    @classmethod
    def update_stats(cls, db_session):
        logger.debug("update stats")
        cls._update_headers_count(db_session)
        cls._update_observations_count(db_session)
        cls._update_exposures_count(db_session)

    @classmethod
    def _update_headers_count(cls, db_session):
        logger.debug("update headers count")
        query = '''
        UPDATE obsportal.interferometer AS i
        SET headers_count = (
            SELECT COUNT(h.id)
            FROM obsportal.header AS h
            WHERE i.id = h.interferometer_id
        )
        '''
        return db_session.execute(text(query))

    @classmethod
    def _update_observations_count(cls, db_session):
        logger.debug("update observations count")
        query = '''
        UPDATE obsportal.interferometer AS i
        SET observations_count = (
            SELECT SUM(inst.observations_count)
            FROM obsportal.instrument AS inst
            WHERE i.id = inst.interferometer_id
        )
        '''
        return db_session.execute(text(query))

    @classmethod
    def _update_exposures_count(cls, db_session):
        logger.debug("update exposures count")
        query = '''
        UPDATE obsportal.interferometer AS i
        SET exposures_count = (
            SELECT SUM(inst.exposures_count)
            FROM obsportal.instrument AS inst
            WHERE i.id = inst.interferometer_id
        )
        '''
        return db_session.execute(text(query))

    # SEARCH ###########################################################################################################

    @classmethod
    def get(cls, db_session, oid):
        return QueryUtil.queryGetCached(cls, db_session, oid)
