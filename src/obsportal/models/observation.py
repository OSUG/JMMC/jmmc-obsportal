# coding: utf-8
import logging

from astropy import units
from astropy.coordinates import Angle
from astropy.io.votable.tree import Field
from sqlalchemy import (
    Column,
    Unicode,
    Integer,
    ForeignKey,
    text, func
)
from sqlalchemy.orm import relationship, contains_eager, joinedload
from sqlalchemy.sql.expression import bindparam
from sqlalchemy_utils import ChoiceType

from obsportal.models.enums.tapfields import EnumEsoTapField
from .enums.observation import EnumObservationCategory
from .enums.program import findProgramType
from .meta import Base
from .program import Program
from .utils import QueryUtil
from .utils.xmatch import XMatch

logger = logging.getLogger(__name__)


class Observation(Base):
    __tablename__ = 'observation'
    __table_args__ = (
        # Index('ix_obsportal_observation_ra_dec_spatial', text("GIST(spoint(radians(target_ra),radians(target_dec)))")),
        {'schema': 'obsportal'}
    )

    # OBSERVATION ######################################################################################################
    id = Column(Integer, nullable=False, primary_key=True)
    category = Column(ChoiceType(EnumObservationCategory), nullable=False)

    program_id = Column(ForeignKey('obsportal.program.id', onupdate='CASCADE', ondelete='RESTRICT'),
                        nullable=False, index=True)
    program = relationship('Program', back_populates='observations')

    # INTERFEROMETER ###################################################################################################
    interferometer_id = Column(ForeignKey('obsportal.interferometer.id', onupdate='CASCADE', ondelete='RESTRICT'),
                               nullable=False, index=True)
    interferometer = relationship('Interferometer', back_populates='observations')

    # INTERFEROMETER SETUP #############################################################################################
    stations = Column('stations', Unicode)
    pops = Column('pops', Unicode)

    # INSTRUMENT #######################################################################################################
    instrument_id = Column(ForeignKey('obsportal.instrument.id', onupdate='CASCADE', ondelete='RESTRICT'),
                           nullable=False, index=True)
    instrument = relationship('Instrument', back_populates='observations')

    instrument_mode_id = Column(ForeignKey('obsportal.instrument_mode.id', onupdate='CASCADE', ondelete='RESTRICT'),
                                index=True)
    instrument_mode = relationship('InstrumentMode', back_populates='observations')

    instrument_submode = Column(Unicode)

    # TARGET ###########################################################################################################
    target_id = Column(ForeignKey('obsportal.target.id', onupdate='CASCADE', ondelete='RESTRICT'), index=True)
    target = relationship('Target', back_populates='observations')

    # EXPOSURES ########################################################################################################
    exposures = relationship('Exposure', back_populates='observation', cascade='all, delete-orphan',
                             passive_deletes=True)

    # STATS ############################################################################################################

    exposures_count = Column(Integer, default=0, server_default=text('0'))

    @classmethod
    def update_stats(cls, db_session):
        logger.debug("update stats")
        cls._update_exposures_count(db_session)

    @classmethod
    def _update_exposures_count(cls, db_session):
        logger.debug("update exposures count")
        query = '''
        UPDATE obsportal.observation AS o
        SET exposures_count = (
            SELECT COUNT(e.id)
            FROM obsportal.exposure AS e
            WHERE o.id = e.observation_id
        )
        '''
        return db_session.execute(text(query))

    # VOTABLE EXPORT ###################################################################################################

    @staticmethod
    def votable_fields(votable, all_fields=True):
        return [
            Field(votable, name="obs_id", datatype="int", ucd="meta.id"),
            Field(votable, name="obs_type", datatype="unicodeChar", arraysize="*", ucd="meta.id;class"),
            Field(votable, name="obs_program", datatype="unicodeChar", arraysize="*", ucd="meta.id"),

            # INTERFEROMETER
            Field(votable, name="interferometer_name", datatype="unicodeChar", arraysize="*", ucd="meta.id"),
            Field(votable, name="interferometer_stations", datatype="unicodeChar", arraysize="*"),

            # INSTRUMENT
            Field(votable, name="instrument_name", datatype="unicodeChar", arraysize="*", ucd="meta.id;instr"),
            Field(votable, name="instrument_mode", datatype="unicodeChar", arraysize="*", ucd="meta.id;instr.setup"),
            Field(votable, name="instrument_submode", datatype="unicodeChar", arraysize="*", ucd="meta.id;instr.setup"),

            # TARGET
            Field(votable, name="target_id", datatype="int", ucd="meta.id"),
            Field(votable, name="target_name", datatype="unicodeChar", arraysize="*", ucd="meta.id;meta.main"),
            Field(votable, name="target_ra", datatype="double", ucd="pos.eq.ra;meta.main", unit="deg"),
            Field(votable, name="target_dec", datatype="double", ucd="pos.eq.dec;meta.main", unit="deg"),
        ]

    def votable_array(self, all_fields=True):
        return (
            self.id,
            self.category or '',
            self.program_id or '',

            # INTERFEROMETER
            self.instrument.interferometer_id or '',
            self.stations or '',

            # INSTRUMENT
            self.instrument_id or '',
            self.instrument_mode.name if self.instrument_mode else '',
            self.instrument_submode or '',

            # TARGET
            self.target.id,
            self.target.name or '',
            self.target.ra,
            self.target.dec,
        )

    # SEARCH ###########################################################################################################

    @classmethod
    def get(cls, db_session, oid):
        return QueryUtil.queryGetCached(cls, db_session, oid)

    @classmethod
    def find(cls, db_session, interferometer=None, instrument=None, instrument_mode=None, instrument_submode=None,
             target=None, stations=None, pops=None, category=None, program_id=None):

        # Use most selective criteria first (target, program, instrument_mode):
        return QueryUtil.queryFilterCached(cls, db_session,
                                           target=target,
                                           program_id=program_id,
                                           instrument_mode=instrument_mode,
                                           instrument=instrument,
                                           instrument_submode=instrument_submode,
                                           stations=stations,
                                           pops=pops,
                                           category=category,
                                           interferometer=interferometer
                                           ) # see QueryUtil.flush

    @classmethod
    def factory(cls, db_session, attributes, category=None, program_id=None, target=None, interferometer=None,
                instrument=None, instrument_mode=None, instrument_submode=None, stations=None, pops=None):

        observation = cls.find(db_session,
                               category=category,
                               program_id=program_id,
                               target=target,
                               interferometer=interferometer,
                               instrument=instrument,
                               instrument_mode=instrument_mode,
                               instrument_submode=instrument_submode,
                               stations=stations,
                               pops=pops)
        program = None
        if observation:
            program = observation.program

        # Get existing (or create) program object
        program = Program.factory(db_session, program_id,
                                  attributes[EnumEsoTapField.PROG_TITLE],
                                  findProgramType(attributes[EnumEsoTapField.PROG_TYPE]),
                                  attributes[EnumEsoTapField.PI_COI],
                                  program
                                  )
        if observation is None:
            observation = cls()
            observation.category = category
            observation.program = program
            observation.interferometer = interferometer
            observation.stations = stations
            observation.pops = pops
            observation.instrument = instrument
            observation.instrument_mode = instrument_mode
            observation.instrument_submode = instrument_submode
            observation.target = target

            # cache new observation to be queryable (memory) by observation.find() method:
            QueryUtil.flush(cls, db_session, observation,
                            target=target,
                            program_id=program_id,
                            instrument_mode=instrument_mode,
                            instrument=instrument,
                            instrument_submode=instrument_submode,
                            stations=stations,
                            pops=pops,
                            category=category,
                            interferometer=interferometer) # see QueryUtil.queryFilterCached

        return observation

    @classmethod
    def search(cls, db_session,
               ra=None, dec=None, radius=None,
               instrument=None, instrument_mode=None,
               program_id=None,
               target_name=None,
               use_limit=True):

        # Build query params
        statement_params = {}

        from obsportal.models import Target, Instrument, InstrumentMode

        # Prepare query: eagerly load useful relationships to avoid extra sub-queries:
        query = db_session.query(Observation).join(Target).options(
            contains_eager(Observation.target).load_only(Target.name, Target.ra, Target.dec),
            joinedload(Observation.instrument).load_only(Instrument.interferometer_id),
            joinedload(Observation.instrument_mode).load_only(InstrumentMode.name)
        )

        use_pos = False

        if ra is not None and ra != '' and dec is not None and dec != '':
            use_pos = True
            statement_params['ra'] = ra
            statement_params['dec'] = dec

            if radius is None or radius == '':
                radius = 10.0
            else:
                radius = float(radius)
            statement_params['radius'] = Angle(radius * units.arcsec).to_value(units.deg)

            query = query.filter(XMatch(Target.ra, Target.dec))

        if target_name:
            statement_params['target_name'] = f"%{target_name.lower()}%"
            query = query.filter(func.lower(Target.name).like(func.lower(bindparam('target_name'))))

        if instrument:
            statement_params['instrument'] = instrument
            query = query.filter(Observation.instrument_id == bindparam('instrument'))

        if instrument_mode:
            statement_params['instrument_mode'] = instrument_mode
            query = query.filter(Observation.instrument_mode_id == bindparam('instrument_mode'))

        if program_id:
            statement_params['program_id'] = program_id
            query = query.filter(Observation.program_id == bindparam('program_id'))

        logger.info(f"statement: {statement_params}")

        # Finalize query
        # always sort by id to ensure stable sort:
        # sort by id (mimics by date)
        if use_pos:
            query = query.order_by(Observation.id.asc())
        else:
            query = query.order_by(Observation.id.desc())

        if use_limit:
            query = query.limit(1000)

        # Execute query
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query: {query}")

        results = query.params(statement_params).all()

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"found {len(results)} results")
        return results
