# coding: utf-8
from datetime import datetime
import logging

from sqlalchemy import (
    Column,
    Unicode,
)

from .meta import Base

logger = logging.getLogger(__name__)

# Possible tags:
DatabaseTagVersion = 'database_version'
SynchronizeStart = 'synchronize_start'
SynchronizeEnd = 'synchronize_end'

# Current last database model version
# Changing that value means re-processing of all headers is needed:
CurrentDatabaseTagVersion = 'obsportal_2020_06_05'


class DatabaseTag(Base):
    __tablename__ = 'database_tag'
    __table_args__ = (
        {'schema': 'obsportal'}
    )
    # key
    id = Column(Unicode, nullable=False, primary_key=True)

    # value
    value = Column(Unicode, index=True)

    # Get / Set db version #############################################################################################

    @classmethod
    def get_db_version(cls, db_session):
        return DatabaseTag.getValue(db_session, DatabaseTagVersion)

    @classmethod
    def set_current_db_version(cls, db_session):
        DatabaseTag.setValue(db_session, DatabaseTagVersion, CurrentDatabaseTagVersion)

    @classmethod
    def is_current_db_version(cls, db_session):
        version = DatabaseTag.get_db_version(db_session)

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"current_db_version: {version}")

        if not version:
            return False
        if version != CurrentDatabaseTagVersion:
            return False
        return True

    # Set synchronize dates ############################################################################################

    @classmethod
    def set_synchronize_start_now(cls, db_session):
        DatabaseTag.setValue(db_session, SynchronizeStart, str(datetime.now()))


    @classmethod
    def set_synchronize_end_now(cls, db_session):
        DatabaseTag.setValue(db_session, SynchronizeEnd, str(datetime.now()))

    # SEARCH ###########################################################################################################

    @classmethod
    def get(cls, db_session, oid):
        try:
            return db_session.query(cls).get(oid)
        except:
            return None

    @classmethod
    def list(cls, db_session):
        try:
            # always sort by id to ensure stable sort:
            return db_session.query(cls).order_by(DatabaseTag.id.asc()).all()
        except:
            return None

    # Get / Set value ##################################################################################################

    @classmethod
    def getValue(cls, db_session, oid):
        tag = DatabaseTag.get(db_session, oid)
        if tag:
            return tag.value
        return None

    @classmethod
    def setValue(cls, db_session, oid, value):
        tag = DatabaseTag.get(db_session, oid)
        if not tag:
            tag = cls()
            tag.id = oid
        if value != tag.value:
            # set value
            logger.info(f"DatabaseTag(id={oid}).setValue('{value}')")
            tag.value = value
            db_session.add(tag)
