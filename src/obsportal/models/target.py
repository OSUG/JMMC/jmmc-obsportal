# coding: utf-8
import logging
from astropy.io.votable.tree import Field
from astropy import units
from astropy.coordinates import Angle

from sqlalchemy import (
    Column,
    Integer,
    Float,
    Unicode,
    text, func,
    UniqueConstraint
)
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import bindparam

from .meta import Base
from .utils import QueryUtil
from .utils.xmatch import XMatch

logger = logging.getLogger(__name__)


class Target(Base):
    __tablename__ = 'target'
    __table_args__ = (
        #Index('ix_target_ra_dec_spatial', text("GIST(spoint(radians(ra),radians(dec)))")),
        UniqueConstraint('name', 'ra', 'dec', name='uq_target_name_ra_dec'),
        {'schema': 'obsportal'}
    )

    id = Column(Integer, nullable=False, primary_key=True)

    # TARGET ###########################################################################################################
    name = Column(Unicode, index=True)
    ra = Column(Float)
    dec = Column(Float, index=True)

    # OBSERVATION ######################################################################################################
    observations = relationship('Observation', back_populates='target')

    # STATS ############################################################################################################

    observations_count = Column(Integer, default=0, server_default=text('0'))
    exposures_count = Column(Integer, default=0, server_default=text('0'))

    @classmethod
    def update_stats(cls, db_session):
        logger.debug("update stats")
        cls._update_observations_count(db_session)
        cls._update_exposures_count(db_session)

    @classmethod
    def _update_observations_count(cls, db_session):
        logger.debug("update observations count")
        query = '''
        UPDATE obsportal.target AS t
        SET observations_count = (
            SELECT COUNT(o.id)
            FROM obsportal.observation AS o
            WHERE t.id = o.target_id
        )
        '''
        return db_session.execute(text(query))

    @classmethod
    def _update_exposures_count(cls, db_session):
        logger.debug("update exposures count")
        query = '''
        UPDATE obsportal.target AS t
        SET exposures_count = (
            SELECT SUM(o.exposures_count)
            FROM obsportal.observation AS o
            WHERE t.id = o.target_id
        )
        '''
        return db_session.execute(text(query))

    # METHODS ##########################################################################################################
    def __init__(self, name=None, ra=None, dec=None):
        self.name = name
        self.ra = ra
        self.dec = dec

    # VOTABLE EXPORT ###################################################################################################

    @staticmethod
    def votable_fields(votable, all_fields=True):
        return [
            Field(votable, name="target_id", datatype="int", ucd="meta.id"),

            Field(votable, name="target_name", datatype="unicodeChar", arraysize="*", ucd="meta.id;meta.main"),
            Field(votable, name="target_ra", datatype="double", ucd="pos.eq.ra;meta.main", unit="deg"),
            Field(votable, name="target_dec", datatype="double", ucd="pos.eq.dec;meta.main", unit="deg"),

            Field(votable, name="obs_count", datatype="int")
        ]

    def votable_array(self, all_fields=True):
        return (
            self.id,

            self.name or '',
            self.ra,
            self.dec,

            self.observations_count or 0
        )

    # SEARCH ###########################################################################################################

    @classmethod
    def get(cls, db_session, oid):
        return QueryUtil.queryGetCached(cls, db_session, oid)

    @classmethod
    def find(cls, db_session, name, ra, dec):
        # Use index on dec first:
        return QueryUtil.queryFilterCached(cls, db_session,
                                           dec=dec, ra=ra, name=name) # see QueryUtil.flush

    @classmethod
    def factory(cls, db_session, name, ra, dec):
        target = cls.find(db_session, name, ra, dec)

        if target is None:
            target = cls(name=name, ra=ra, dec=dec)
            # cache new target to be queryable (memory) by target.find() method:
            QueryUtil.flush(cls, db_session, target,
                            dec=dec, ra=ra, name=name) # see QueryUtil.queryFilterCached

        return target

    @classmethod
    def search(cls, db_session,
               name=None,
               ra=None, dec=None, radius=None,
               use_limit=True):

        # Build query params
        statement_params = {}

        # Prepare query:
        query = db_session.query(Target)

        if ra is not None and ra != '' and dec is not None and dec != '':
            statement_params['ra'] = ra
            statement_params['dec'] = dec

            if radius is None or radius == '':
                radius = 10.0
            else:
                radius = float(radius)
            statement_params['radius'] = Angle(radius * units.arcsec).to_value(units.deg)

            query = query.filter(XMatch(Target.ra, Target.dec))

        if name:
            statement_params['target_name'] = f"%{name.lower()}%"
            query = query.filter(func.lower(Target.name).like(func.lower(bindparam('target_name'))))

        logger.info(f"statement: {statement_params}")

        # Finalize query
        query = query.order_by(Target.dec.desc())
        # always sort by id to ensure stable sort:
        query = query.order_by(Target.id.asc())

        if use_limit:
            query = query.limit(1000)

        # Execute query
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"query: {query}")

        results = query.params(statement_params).all()

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"found {len(results)} results")
        return results
