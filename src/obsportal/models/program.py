# coding: utf-8
import logging

from sqlalchemy import (
    Column,
    Integer,
    Text,
    Unicode,
    PrimaryKeyConstraint,
    ForeignKey,
)
from sqlalchemy.orm import relationship
from sqlalchemy_utils import UUIDType, ChoiceType

from .enums.program import EnumProgramType
from .meta import Base


# See https://archive.eso.org/cms/eso-data/eso-programme-identification-code.html
from .utils import QueryUtil

logger = logging.getLogger(__name__)


class Program(Base):
    __tablename__ = 'program'
    __table_args__ = (
        {'schema': 'obsportal'}
    )

    id = Column(Unicode, nullable=False, primary_key=True)

    # PROGRAM ##########################################################################################################

    title = Column('title', Unicode)
    type = Column(ChoiceType(EnumProgramType, impl=Integer()), nullable=False)

    pi_coi = Column('pi_coi', Unicode)

    # OBSERVATION ######################################################################################################
    observations = relationship('Observation', back_populates='program')

    # SEARCH ###########################################################################################################

    @classmethod
    def get(cls, db_session, oid):
        return QueryUtil.queryGetCached(cls, db_session, oid)

    @classmethod
    def factory(cls, db_session, program_id, program_title, program_type, pi_coi, program=None):
        is_new = False

        if not program:
            program = cls.get(db_session, program_id)

        if program is None:
            is_new = True
            program = cls()
            program.id = program_id

        if program_title != program.title or pi_coi != program.pi_coi:
            logger.info(f"'{program_id}' change detected on title ('{program_title}' != '{program.title}') or pi-coi ('{pi_coi}' != '{program.pi_coi}')")
            # update fields anyway:
            program.title = program_title
            program.type = program_type
            program.pi_coi = pi_coi

        if is_new:
            # cache new program to be queryable (memory) by program.get() method:
            QueryUtil.flush(cls, db_session, program)

        return program
