import logging
import os
from pyramid.settings import asbool
from pyramid.paster import get_appsettings

logger = logging.getLogger(__name__)


def load_settings(config_uri, section_name=None, env_prefix='obsportal'):
    if logger.isEnabledFor(logging.DEBUG):
        logger.debug(f"load settings from '{config_uri}'")

    # Get settings from INI file
    settings = get_appsettings(config_uri, name=section_name)

    # Merge global conf. with application conf.
    merged_settings = settings.global_conf.copy()
    merged_settings.update(settings)

    # Merge Environment variables
    merged_settings = update_settings_with_envvars(merged_settings, env_prefix)

    return merged_settings


# Inspired by pyramid_auto_env:
# https://pypi.org/project/pyramid-auto-env/
# https://raw.githubusercontent.com/marcelomoraes28/pyramid-auto-env/master/pyramid_auto_env/__init__.py
def update_settings_with_envvars(settings, prefix='obsportal'):
    for k, v in settings.items():

        # STEP 1: Format the key
        #########################

        # If key is starting by prefix
        if k.startswith(prefix):
            transformed_key = "{}".format(k.upper().replace('.', '_').replace('-', '_'))

        # Else, try to add prefix
        else:
            transformed_key = "{}_{}".format(prefix.upper(), k.upper().replace('.', '_').replace('-', '_'))

        # STEP 2: Find the envvar
        #########################

        envvar_value = os.environ.get(transformed_key)
        if not envvar_value:
            continue

        settings[k] = asbool(envvar_value) if envvar_value.lower() in ['true', 'false'] else envvar_value

        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(f"replace INI '{k}' with ENV '{transformed_key}'")

    return settings
