# coding=utf-8


# From https://tdhopper.com/blog/testing-whether-a-python-string-contains-an-integer/
def is_integer(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


# From https://www.pythoncentral.io/how-to-check-if-a-string-is-a-number-in-python-including-unicode/
def is_number(value):
    try:
        float(value)
        return True
    except ValueError:
        pass
    # ignore unicode part
    return False
