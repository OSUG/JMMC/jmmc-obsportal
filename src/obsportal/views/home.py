# coding=utf-8
from pyramid.view import view_config

from .view_abstract import ViewAbstract

@view_config(route_name='home',
             renderer='../templates/views/home.jinja2')
class ViewHome(ViewAbstract):

    def __call__(self):
        return {'updating': self.synchronize_running}
