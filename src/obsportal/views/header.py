# coding=utf-8
import os
import logging

from pyramid.httpexceptions import HTTPNotFound
from pyramid.response import FileResponse
from pyramid.view import view_config

from .view_abstract import ViewAbstract
from .view_votable_abstract import ViewVOTableAbstract
from ..models.header import Header

logger = logging.getLogger(__name__)

@view_config(route_name='detail_raw',
             match_param='type=header')
class ViewHeaderFile(ViewAbstract):

    def __call__(self):

        # Get object
        header = Header.get(self.db_session, self.request.matchdict.get('id'))

        if header is not None:
            file_path = os.path.join(self.settings.get('obsportal.paths.data'), header.relative_file_path)

            if os.path.exists(file_path):
                return FileResponse(file_path, self.request)
            else:
#                if logger.isEnabledFor(logging.DEBUG):
#                    logger.debug(f"Can't find '{file_path}' header file:\n{header}")
                return HTTPNotFound('Resource file not found!')
        else:
            return HTTPNotFound(f"Header {self.request.matchdict.get('id')} not found!")


@view_config(route_name='detail',
             match_param='type=header',
             renderer='../templates/views/header/detail.jinja2')
class ViewHeaderDetail(ViewAbstract):

    def __call__(self):
        # Get object
        header = Header.get(self.db_session, self.request.matchdict.get('id'))

        # Render
        if header is not None:
            return {'header': header, 'updating': self.synchronize_running}
        else:
            return HTTPNotFound(f"Header {self.request.matchdict.get('id')} not found!")


@view_config(route_name='detail_votable',
             match_param='type=header')
class ViewHeaderDetailVotable(ViewVOTableAbstract):

    def __call__(self):
        # Get object
        header = Header.get(self.db_session, self.request.matchdict.get('id'))

        # Build VOTable
        votable = self._build_votable(Header, header)

        # Render
        return self._render_votable(votable)


@view_config(route_name='list',
             match_param='type=header',
             renderer='../templates/views/header/list.jinja2')
class ViewHeaderList(ViewAbstract):

    def __call__(self):
        # Get list
        headers = Header.search(self.db_session, load_exposure_ids=True)  # TODO: define instrument

        # Render
        return {'headers': headers, 'updating': self.synchronize_running}


@view_config(route_name='list_votable',
             match_param='type=header')
class ViewHeaderListVotable(ViewVOTableAbstract):

    def __call__(self):
        # Get list
        # NOTE: use limit to not exceed server memory (find another solution for huge tables : streaming / fs ?)
        headers = Header.search(self.db_session, self.request.params.get('instrument'), use_limit=True)

        # Build VOTable
        votable = self._build_votable(Header, headers)

        # Render
        return self._render_votable(votable)
