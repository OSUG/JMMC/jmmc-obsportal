# coding=utf-8

from abc import abstractmethod
import logging
import uuid
import os
from gzip import GzipFile
from io import BytesIO
from tempfile import NamedTemporaryFile

from pyramid.request import Request
from pyramid.response import FileResponse, Response
from sqlalchemy.orm import Session
from astropy.io.votable.tree import VOTableFile, Resource, Table, Param

from ..models.instrument import Instrument
from ..models.interferometer import Interferometer
from ..models.header import Header

logger = logging.getLogger(__name__)


class ViewAbstract(object):

    @abstractmethod
    def __init__(self, request, dbsession=None):
        """Init ViewAbstract.

        :param Request request: The request object.
        """
        self.request = request
        self._dbsession = dbsession

    @property
    def db_session(self):
        """:rtype: Session"""
        return self._dbsession or self.request.dbsession

    @property
    def settings(self):
        return self.request.registry.settings

    @property
    def instruments(self):
        return self.db_session.query(Instrument).all()

    @property
    def interferometers(self):
        return self.db_session.query(Interferometer).all()

    @property
    def synchronize_running(self):
        lock_file = self.settings.get('obsportal.locks.synchronize') or '/tmp/synchronize.lock'
        return os.path.exists(lock_file)