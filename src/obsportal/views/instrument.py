# coding=utf-8
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPNotFound

from .view_abstract import ViewAbstract
from ..models.instrument import Instrument


@view_config(route_name='list',
             match_param='type=instrument',
             renderer='../templates/views/instrument/list.jinja2')
class ViewInstrumentList(ViewAbstract):

    def __call__(self):
        return {'instruments': self.instruments, 'updating': self.synchronize_running}


@view_config(route_name='detail',
             match_param='type=instrument',
             renderer='../templates/views/instrument/detail.jinja2')
class ViewInstrumentDetail(ViewAbstract):

    def __call__(self):
        instrument = Instrument.get(self.db_session, self.request.matchdict.get('id'))

        # Render
        if instrument is not None:
            return {'instrument': instrument, 'updating': self.synchronize_running}
        else:
            return HTTPNotFound(f"Instrument {self.request.matchdict.get('id')} not found!")
