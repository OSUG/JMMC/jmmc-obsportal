# coding=utf-8
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPNotFound

from .view_abstract import ViewAbstract
from .view_votable_abstract import ViewVOTableAbstract
from ..models.target import Target


@view_config(route_name='detail',
             match_param='type=target',
             renderer='../templates/views/target/detail.jinja2')
class ViewTargetDetail(ViewAbstract):

    def __call__(self):
        # Get object
        target = Target.get(self.db_session, self.request.matchdict.get('id'))

        # Render
        if target is not None:
            return {'target': target, 'updating': self.synchronize_running}
        else:
            return HTTPNotFound(f"Target {self.request.matchdict.get('id')} not found!")


@view_config(route_name='list',
             match_param='type=target',
             renderer='../templates/views/target/list.jinja2')
class ViewTargetList(ViewAbstract):

    def __call__(self):
        # Get list
        targets = Target.search(self.db_session,
                                self.request.params.get('name'),
                                self.request.params.get('ra'),
                                self.request.params.get('dec'),
                                self.request.params.get('radius'))

        return {'targets': targets, 'updating': self.synchronize_running}


@view_config(route_name='list_votable',
             match_param='type=target')
class ViewTargetListVotable(ViewVOTableAbstract):

    def __call__(self):
        # Get list
        targets = Target.search(self.db_session,
                                self.request.params.get('name'),
                                self.request.params.get('ra'),
                                self.request.params.get('dec'),
                                self.request.params.get('radius'),
                                use_limit=False)  # no limit

        # Build VOTable
        votable = self._build_votable(Target, targets)

        # Render
        return self._render_votable(votable)
