# coding=utf-8
import logging
import os
import uuid
from gzip import GzipFile
from io import BytesIO
from tempfile import NamedTemporaryFile

from astropy.io.votable.tree import Param, Resource, Table, VOTableFile
from pyramid.response import FileResponse, Response

from .view_abstract import ViewAbstract
from ..models.header import Header

logger = logging.getLogger(__name__)


class ViewVOTableAbstract(ViewAbstract):

    def _build_votable(self, table_class, table_items, all_fields=True):
        """
        Build a VOTable
        :param table_class: str
        :param table_items: list|object
        :return: VOTableFile
        """

        # Create a new VOTable file...
        votable = VOTableFile()

        # ...with one resource...
        resource = Resource()
        votable.resources.append(resource)

        # ... with one table
        table = Table(votable)
        resource.tables.append(table)

        # ... with parameters
        status = 'SYNC' if self.synchronize_running else 'OK'
        param_status = Param(votable, name='status', datatype="unicodeChar", arraysize="*", value=status)
        resource.params.append(param_status)

        last_mod_date = Header.last_modification_date(self.db_session)
        param_last_mod_date = Param(votable, datatype="unicodeChar", arraysize="*", name='last_mod_date',
                                    value=str(last_mod_date))
        resource.params.append(param_last_mod_date)

        # Define some fields
        table.fields.extend(table_class.votable_fields(votable, all_fields))

        # Multiple items
        if isinstance(table_items, (list, tuple)):

            # Now, use those field definitions to create the numpy record arrays, with the given number of rows
            table.create_arrays(len(table_items))

            # Now table.array can be filled with data
            item_index = 0
            for table_item in table_items:
                table.array[item_index] = table_item.votable_array(all_fields)
                item_index += 1

        # Single item
        elif table_items is not None:
            table.create_arrays(1)
            table.array[0] = table_items.votable_array(all_fields)

        return votable

    def _render_votable(self, votable, compressed=True):
        #  return self._render(votable, compressed)
        return self._render_stream(votable, compressed)

    # deprecated
    def _render(self, votable, compressed):
        """
        Render a VOTable as FileResponse
        :param votable: VOTableFile
        :param compressed: bool
        :return: FileResponse
        """
        logger.info('RENDER')

        # Define Content Type
        content_type = 'text/xml' #'application/x-votable+xml'

        # Define content encoding
        if compressed:
            content_encoding = 'gzip'
        else:
            content_encoding = None

        # Generate a temporary filename
        tmp_file_path = os.path.join(self.settings.get('obsportal.paths.tmp', '/tmp'), "%s" % uuid.uuid4())

        with NamedTemporaryFile(mode='wb', prefix=tmp_file_path,
                                suffix='.votable.gz', delete=True) as tmp:

            # Now write the whole thing to a file.
            votable.to_xml(tmp, compressed)

            tmp.flush()

            # Build response
            response = FileResponse(os.path.abspath(tmp.name), self.request, cache_max_age=0,
                                    content_type=content_type, content_encoding=content_encoding)

            logger.info('VOTABLE: done')

            # Return response
            return response

    @staticmethod
    def _render_stream(votable, compressed):
        """
        Render a VOTable as FileResponse
        :param votable: VOTableFile
        :param compressed: bool
        :return: Response
        """
        logger.info('RENDER')

        with BytesIO() as buffer:
            if compressed:
                content_encoding = 'gzip'
                with GzipFile(fileobj=buffer, mode='wb') as buffer_gzip:
                    votable.to_xml(buffer_gzip)
            else:
                content_encoding = None
                votable.to_xml(buffer)

            buffer.seek(0)

            response = Response(body=buffer.getvalue(), content_type='text/xml', content_encoding=content_encoding)

        logger.info('VOTABLE: done')

        # Return response
        return response
