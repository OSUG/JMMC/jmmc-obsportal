# coding=utf-8
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPNotFound

from .view_abstract import ViewAbstract
from ..models.interferometer import Interferometer


@view_config(route_name='list',
             match_param='type=interferometer',
             renderer='../templates/views/interferometer/list.jinja2')
class ViewInterferometerList(ViewAbstract):

    def __call__(self):
        interferometers = self.db_session.query(Interferometer).all()
        return {'interferometers': interferometers, 'updating': self.synchronize_running}


@view_config(route_name='detail',
             match_param='type=interferometer',
             renderer='../templates/views/interferometer/detail.jinja2')
class ViewInterferometerDetail(ViewAbstract):

    def __call__(self):
        interferometer = Interferometer.get(self.db_session, self.request.matchdict.get('id'))

        # Render
        if interferometer is not None:
            return {'interferometer': interferometer, 'updating': self.synchronize_running}
        else:
            return HTTPNotFound(f"Interferometer {self.request.matchdict.get('id')} not found!")
