# coding=utf-8
import logging
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPNotFound
from .view_abstract import ViewAbstract
from .view_votable_abstract import ViewVOTableAbstract
from ..models.observation import Observation
from ..models.instrument import Instrument
from ..forms.instrument import FormInstrumentNameList

logger = logging.getLogger(__name__)


@view_config(route_name='detail',
             match_param='type=observation',
             renderer='../templates/views/observation/detail.jinja2')
class ViewObservationDetail(ViewAbstract):

    def __call__(self):
        # Get object
        observation = Observation.get(self.db_session, self.request.matchdict.get('id'))

        # Render
        if observation is not None:
            return {'observation': observation, 'updating': self.synchronize_running}
        else:
            return HTTPNotFound(f"Observation {self.request.matchdict.get('id')} not found!")


@view_config(route_name='detail_votable',
             match_param='type=observation')
class ViewObservationDetailVotable(ViewVOTableAbstract):

    def __call__(self):
        # Get object
        observation = Observation.get(self.db_session, self.request.matchdict.get('id'))

        # Build VOTable
        votable = self._build_votable(Observation, observation)

        # Render
        return self._render_votable(votable)


@view_config(route_name='list',
             match_param='type=observation',
             renderer='../templates/views/observation/list.jinja2')
class ViewObservationList(ViewAbstract):

    def __call__(self):
        # Initialize the form
        form = FormInstrumentNameList(self.request.params, instruments=self.instruments)

        # Submitted form?
        if self.request.params and form.validate():
            observations = Observation.search(self.db_session, instrument=form.data.get('instrument'))
        else:
            observations = Observation.search(self.db_session)

        # Render
        return {'form': form, 'observations': observations, 'updating': self.synchronize_running}


@view_config(route_name='list_votable',
             match_param='type=observation')
class ViewObservationListVotable(ViewVOTableAbstract):

    def __call__(self):
        # Get list
        observations = Observation.search(self.db_session,
                                          instrument=self.request.params.get('instrument'),
                                          use_limit=False)  # no limit

        # Build VOTable
        votable = self._build_votable(Observation, observations)

        # Render
        return self._render_votable(votable)
