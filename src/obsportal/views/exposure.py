# coding=utf-8
import logging
import re
from datetime import datetime
from gzip import GzipFile
from io import BytesIO

from pyramid.response import Response
from pyramid.view import view_config
from pyramid.httpexceptions import HTTPNotFound

from .view_abstract import ViewAbstract
from .view_votable_abstract import ViewVOTableAbstract
from ..models import Header
from ..models.exposure import Exposure
from ..forms.search import FormSearch

logger = logging.getLogger(__name__)

use_votable_fast = True

if use_votable_fast:
    logger.info("use_votable_fast: enabled.")


class ViewVOTableExposureAbstract(ViewVOTableAbstract):

    def render_votable(self, exposures, all_fields=True):
        if use_votable_fast:
            return self.render_votable_fast(exposures, all_fields)

        logger.info(f'BUILD VOTABLE: {len(exposures)}')

        votable = self._build_votable(Exposure, exposures, all_fields)

        # Render
        return self._render_votable(votable)

    def render_votable_fast(self, exposures, all_fields=True):
        logger.info('RENDER')

        count = 0  # size not known (query streaming)

        with BytesIO() as buffer:
            content_encoding = 'gzip'
            with GzipFile(fileobj=buffer, mode='wb') as buffer_gzip:
                # ... with parameters
                status = 'SYNC' if self.synchronize_running else 'OK'
                last_mod_date = Header.last_modification_date(self.db_session)

                Exposure.write_votable_header(buffer_gzip, status, str(last_mod_date), all_fields)
                for exp in exposures:
                    exp.write_votable_row(buffer_gzip, all_fields)
                    count += 1
                Exposure.write_votable_footer(buffer_gzip)

            buffer.seek(0)

            response = Response(body=buffer.getvalue(), content_type='text/xml', content_encoding=content_encoding)

        logger.info(f'VOTABLE: done ({count} exposures)')

        # Return response
        return response


@view_config(route_name='detail',
             match_param='type=exposure',
             renderer='../templates/views/exposure/detail.jinja2')
class ViewExposureDetail(ViewAbstract):

    def __call__(self):
        # Get object
        exposure = Exposure.get(self.db_session, self.request.matchdict.get('id'))

        # Render
        if exposure is not None:
            return {'exposure': exposure, 'updating': self.synchronize_running}
        else:
            return HTTPNotFound(f"Exposure {self.request.matchdict.get('id')} not found!")


@view_config(route_name='list',
             match_param='type=exposure',
             renderer='../templates/views/exposure/list.jinja2')
class ViewExposureList(ViewAbstract):

    def __call__(self):
        # Get list anyway
        # NOTE: use default limit:
        exposures = Exposure.search(self.db_session, order_by_field=Exposure.date_updated, order_asc=False)

        # Render
        return {'exposures': exposures, 'updating': self.synchronize_running}


@view_config(route_name='list_votable',
             match_param='type=exposure')
class ViewExposureListVotable(ViewVOTableExposureAbstract):

    def __call__(self):
        # return all fields by default:
        all_fields = True

        # Get list anyway
        # NOTE: use default limit:
        exposures = Exposure.search(self.db_session, order_by_field=Exposure.date_updated,
                                    all_fields=all_fields, as_query=use_votable_fast)

        # Build VOTable
        return self.render_votable(exposures, all_fields)


@view_config(route_name='search', renderer='../templates/views/exposure/search.jinja2')
class ViewExposureSearch(ViewAbstract):

    def __call__(self):
        # Initialize the form
        form = FormSearch(self.request.params, instruments=self.instruments, interferometers=self.interferometers)

        # Execute the search
        if self.request.params and form.validate():
            # Search
            exposures = Exposure.search(self.db_session,
                                        form.data.get('ra'),
                                        form.data.get('dec'),
                                        form.data.get('radius'),
                                        form.data.get('interferometer'),
                                        form.data.get('instrument'),
                                        form.data.get('instrument_mode'),
                                        form.data.get('program_id'),
                                        form.data.get('target_name'),
                                        form.data.get('mjd_from'),
                                        form.data.get('mjd_to'),
                                        valid_level=form.data.get('valid_level'))

            # Render
            return {'form': form, 'exposures': exposures, 'updating': self.synchronize_running}

        # No search, just display the form
        return {'form': form, 'updating': self.synchronize_running}


@view_config(route_name='search', request_param="format=votable")
@view_config(route_name='search_votable')
class ViewExposureSearchVotable(ViewVOTableExposureAbstract):

    def __call__(self):
        # Search
        logger.info('SEARCH')

        valid_level = self.request.params.get('valid_level')
        if valid_level is None:
            # only valid results for ASPRO2 by default:
            valid_level = 1

        all_fields = self.request.params.get('all_fields')
        if all_fields in (False, 'false'):
            all_fields = False
        else:
            # return all fields by default:
            all_fields = True

        maxrec = self.request.params.get('maxrec')
        if maxrec:
            try:
                maxrec = int(maxrec)
                if maxrec <= 0:
                    maxrec = None
            except(ValueError|TypeError):
                maxrec = None  # no limit by default

        # handle parameters for date_updated
        date_updated_from = self.request.params.get('date_updated_from')
        if date_updated_from:
            date_updated_from = datetime.fromisoformat(date_updated_from)

        date_updated_to = self.request.params.get('date_updated_to')
        if date_updated_to:
            date_updated_to = datetime.fromisoformat(date_updated_to)

        exposures = Exposure.search(self.db_session,
                                    self.request.params.get('ra'),
                                    self.request.params.get('dec'),
                                    self.request.params.get('radius'),
                                    self.request.params.get('interferometer'),
                                    self.request.params.get('instrument'),
                                    self.request.params.get('instrument_mode'),
                                    self.request.params.get('program_id'),
                                    self.request.params.get('target_name'),
                                    self.request.params.get('mjd_from'),
                                    self.request.params.get('mjd_to'),
                                    date_updated_from,
                                    date_updated_to,
                                    valid_level=valid_level,
                                    maxrec=maxrec,
                                    all_fields=all_fields,
                                    as_query=use_votable_fast)

        return self.render_votable(exposures, all_fields)
