# coding=utf-8
import logging
import os
from astropy.time import Time
from sqlalchemy import func, text
from sqlalchemy.exc import DBAPIError, OperationalError, StatementError
from pyramid.view import view_config

from .view_abstract import ViewAbstract
from ..models import Observation, Header, Target, DatabaseTag
from ..models.exposure import Exposure

logger = logging.getLogger(__name__)


@view_config(route_name='health',
             renderer='../templates/views/health.jinja2')
class ViewHealth(ViewAbstract):

    def __call__(self):

        # Initialize status metrics
        services = {
            'database': False,
            'database structure': False,
        }
        hdr_count = 0
        tgt_count = 0
        obs_count = 0
        exp_count = 0
        last_mod_date = None
        exp_valid_count = 0
        exp_valid_ratio = 0
        exp_min_date = None
        exp_max_date = None
        db_host = None
        db_name = None
        db_user = None
        db_pool_status = None
        pg_stats = None
        db_tags = None

        state = True
        try:
            # 1. Database availability
            self.db_session.execute(text("SELECT 1"))
            services['database'] = True

            # 1b: DB connection pool status
            engine = self.db_session.get_bind()
            db_host = engine.url.host
            db_name = engine.url.database
            db_user = engine.url.username

            db_pool_status = engine.pool.status()

            # 1c: PG stats
            pg_stats_query = """
            SELECT count(*) AS number, state
            FROM pg_stat_activity
            WHERE datname = :datname
            GROUP BY 2;
            """
            pg_stats_query_params = {'datname': engine.url.database}
            pg_stats = self.db_session.execute(text(pg_stats_query), pg_stats_query_params)

            # 1d: Database tags
            db_tags = DatabaseTag.list(self.db_session)

            # 2. Database structure availability + stats
            hdr_count = self.db_session.query(Header).count()
            tgt_count = self.db_session.query(Target).count()
            obs_count = self.db_session.query(Observation).count()
            exp_count = self.db_session.query(Exposure).count()
            services['database structure'] = True

            # Valid exposures
            exp_valid_count = self.db_session.query(Exposure).filter(Exposure.validation_level >= 1).count()
            if exp_count > 0:
                exp_valid_ratio = exp_valid_count / exp_count * 100

            # Min/Max exposure dates (use mjd then convert to timestamp)
            mjd_min = self.db_session.query(func.min(Exposure.mjd_start)).scalar()
            if mjd_min:
                exp_min_date = Time(mjd_min, format='mjd').iso

            mjd_max = self.db_session.query(func.max(Exposure.mjd_start)).scalar()
            if mjd_max:
                exp_max_date = Time(mjd_max, format='mjd').iso

            last_mod_date = Header.last_modification_date(self.db_session)

        except DBAPIError:
            state = False
        except OperationalError:
            state = False
        except StatementError:
            state = False

        # 3: Data path
        services['data directory'] = check_path(self.settings.get('obsportal.paths.data'))

        # 4: Logs path
        services['logs directory'] = check_path(self.settings.get('obsportal.paths.logs'))

        # 5: Tmp path
        services['tmp directory'] = check_path(self.settings.get('obsportal.paths.tmp'))

        if state:
            # update final status on all services:
            for status in services.values():
                if not status:
                    state = False
                    break

        if not state:
            # Set HTTP 500 to let kubernetes liveliness fail
            self.request.response.status = 500
            try:
                logger.warning(f"Health check failed: {services}")
            except Exception as e:
                print(f"Health check failed: {services}")

        return {
            'settings': self.settings,
            'services': services,
            'updating': self.synchronize_running,

            'hdr_count': hdr_count,
            'tgt_count': tgt_count,
            'obs_count': obs_count,
            'exp_count': exp_count,
            'exp_valid_count': exp_valid_count,
            'exp_valid_ratio': exp_valid_ratio,
            'exp_min_date': exp_min_date,
            'exp_max_date': exp_max_date,
            'last_mod_date': last_mod_date,

            'db_host': db_host,
            'db_name': db_name,
            'db_user': db_user,
            'db_pool_status': db_pool_status,
            'pg_stats': pg_stats,
            'db_tags': db_tags
        }


def check_path(path, test_write=True):
    state = False
    try:
        if os.path.exists(path):
            if test_write:
                # check writing in path:
                test_file_path = os.path.join(path, "test-fs.log")

                with open(test_file_path, 'w') as output_file:
                    output_file.write("TEST WRITE")

                os.remove(test_file_path)

            state=True
    except Exception as e:
        logger.error(f"check_path('{path}'): failed", e)

    return state

