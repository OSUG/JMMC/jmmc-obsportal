#!/bin/bash

echo "run download-night for all GRAVITY nights"

cat "db/tap_eso-dbo_raw-all_GRAVITY_nights.txt" | while read line 
do
    CMD="obsportal-cli eso download-night $line --debug --settings=client.ini"
    echo "Running: $CMD"
    $CMD
done

echo "That's all, Folks !!!"

