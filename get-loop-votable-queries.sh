#!/bin/bash

N=10
TH=$1

export LANG=C


for i in $(seq $N)
do
    echo "Loop: ${i}/$N ..."
    echo "-------------------"

    time ./get-votable-queries.sh $TH

    echo "-------------------"
    sleep 1
done

echo "Loops done"

