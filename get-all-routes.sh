#!/bin/bash

export LANG=C

# production
#HOST="http://obs.jmmc.fr"
# pre-production
#HOST="http://obs-preprod.jmmc.fr"
# dev
HOST="http://localhost:6543"

DIR="tests"

echo "HOST: $HOST"

mkdir $DIR
cd $DIR
rm *

cat "../http_routes.txt" | grep -v "#" | while read line 
do
    URL="${HOST}${line}"
    echo "Get '$URL'"
    time wget -q $URL
done

echo "That's all, Folks !!!"

