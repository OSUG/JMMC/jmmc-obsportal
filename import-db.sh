#!/bin/bash

SETTINGSFILE=${1:-client.ini}
test -z "$SETTINGSFILE"   && echo "Missing argument, please provide a settings file."            && exit 1
test ! -r "$SETTINGSFILE" && echo "Can't read '$SETTINGSFILE', please provide a settings file."  && exit 1

processId="obsportal-cli"
logFile=/logs/client.log

# Check if service is running:
PID=`ps -ef | grep "${processId}" | grep -v "grep" | awk '{print $2}'`
#echo "PID: '${PID}'"

N_PID=`echo "${PID}" | wc -w`

if [ "${N_PID}" -ne "0" ]
then
    echo "Process '${processId}' (PID = ${PID}) is already running ..."
    exit 1
fi

# Move log before starting new process
mv $logFile ${logFile}.$(date +"%F_%T")

# Upgrade the database structure
CMD="obsportal-cli database upgrade --settings=$SETTINGSFILE"
echo "Running: $CMD"
$CMD

# Launch the synchronization
CMD="obsportal-cli synchronize --settings=$SETTINGSFILE"
echo "Running: $CMD"
$CMD

echo "That's all, Folks !!!"
