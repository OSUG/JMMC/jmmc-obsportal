#!/bin/bash

# Use US Locale:
export LANG=C

if [ -z "$1" ]
  then
    echo "No argument 1 (CMD) supplied"
    exit 1
fi

DIR="$1/eso"

echo "Fixing folders in '$DIR' ..."

cd $DIR

# Scan folder YEAR
for YDIR in */; do
    echo "Processing '${YDIR}'"
	cd ${YDIR}

	# fix folder MONTH
	for i in {1..9}; do
		if [ -d "${i}" ]; then
			mv -v ${i} 0${i}
		fi
	done

	for MDIR in */; do
		echo "Processing '${YDIR}${MDIR}'"
		cd ${MDIR}

		# fix folder DAY
		for i in {1..9}; do
			if [ -d "${i}" ]; then
				mv -v ${i} 0${i}
			fi
		done

		cd ..
	done

	cd ..
done

echo "That's all, Folks !!!"

