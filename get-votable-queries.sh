#!/bin/bash

TH=$1

export LANG=C

# production
#HOST="http://obs.jmmc.fr"
# pre-production
#HOST="http://obs-preprod.jmmc.fr"
# dev
HOST="http://localhost:6543"

echo "HOST: $HOST"

DIR="./tests"

mkdir $DIR
cd $DIR

i=0

cat "../http_votable_queries.txt" | grep -v "#" | while read line 
do
    URL="${HOST}/${line}"
    echo "Get '$URL'"
    wget -q -O "votable_${TH}_${line}.vot.gz" $URL

    i=$((i + 1))
done

echo "End: $i requests"

