#!/bin/bash

docker build -t "obsportal:latest" --build-arg SHAREDCONFFILE=shared.ini --build-arg APPCONFFILE=development.ini --build-arg CLICONFFILE=client.ini .
docker run -p 6543:6543 -v /data:/data -v /data/obsportal/logs:/logs --rm --name d_obsportal "obsportal:latest"

